-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2019 at 10:30 AM
-- Server version: 5.6.44
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer_familynetwork`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us_core_features`
--

CREATE TABLE `about_us_core_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us_core_features`
--

INSERT INTO `about_us_core_features` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Personal Profile Page', '2018-11-27 01:02:54', '2018-11-27 01:02:54'),
(2, 'Family Tree Builder', '2018-11-27 01:03:05', '2018-11-27 01:03:05'),
(3, 'Weekly Inspiration Question', '2018-11-27 01:03:14', '2018-11-27 01:03:14'),
(4, 'Upload and store video, photos and audio', '2018-11-27 01:03:24', '2018-11-27 01:03:24'),
(5, 'Forums', '2018-11-27 01:03:34', '2018-11-27 01:03:34'),
(6, 'Universal Content Search', '2018-11-27 01:03:44', '2018-11-27 01:03:44'),
(7, 'Featured Family Member', '2018-11-27 01:03:54', '2018-11-27 01:03:54'),
(8, 'Privacy and Security', '2018-11-27 01:04:03', '2018-11-27 01:04:03'),
(9, 'You Own Your Own Data', '2018-11-27 01:04:12', '2018-11-27 01:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `published_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(2, NULL, 1, 'Category 2', 'category-2', '2018-11-19 01:08:16', '2018-11-19 01:08:16');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(49, 6, 'body', 'hidden', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(57, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(58, 11, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(59, 11, 'content', 'rich_text_box', 'Content', 1, 1, 1, 1, 1, 1, '{}', 3),
(61, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(62, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(63, 11, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(64, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 15, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(66, 15, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(67, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(68, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(69, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(70, 17, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(71, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(72, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(73, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(74, 19, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(75, 19, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(76, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(77, 21, 'user_id', 'hidden', 'User Id', 0, 0, 1, 1, 1, 1, '{}', 2),
(78, 21, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(79, 21, 'excerpt', 'text_area', 'Excerpt', 0, 1, 1, 1, 1, 1, '{}', 4),
(80, 21, 'description', 'rich_text_box', 'Description', 0, 0, 1, 1, 1, 1, '{}', 5),
(81, 21, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 6),
(82, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(83, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(84, 22, 'question', 'text_area', 'Question', 0, 1, 1, 1, 1, 1, '{}', 2),
(85, 22, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 3),
(86, 22, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(87, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(88, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(89, 25, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 2),
(90, 25, 'category_name', 'text', 'Category Name', 0, 1, 1, 1, 1, 1, '{}', 3),
(91, 25, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 4),
(92, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(93, 29, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(94, 29, 'plan', 'text', 'Plan', 0, 1, 1, 1, 1, 1, '{}', 2),
(95, 29, 'price', 'text', 'Price', 0, 1, 1, 1, 1, 1, '{}', 3),
(96, 29, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(97, 29, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(99, 29, 'choose_plan_options', 'select_multiple', 'Choose Plan Options', 0, 1, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"my-profile\":\"Profile Page\",\"my-tree\":\"Family Tree Builder\",\"family-members\":\"View family member profiles and stories\",\"stories\":\"Write stories, upload and store video, photos and audio\",\"questions\":\"Weekly Inspiration Question\",\"my-forums\":\"Forums\"}}', 4),
(100, 29, 'added_by', 'text', 'Added By', 0, 0, 0, 0, 0, 0, '{}', 5);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-11-19 01:08:09', '2018-11-19 01:08:09'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-11-19 01:08:09', '2018-11-19 01:08:09'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-11-19 01:08:09', '2018-11-19 01:08:09'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-11-19 01:08:15', '2018-11-19 01:08:15'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'Pvtl\\VoyagerPageBlocks\\Page', NULL, '\\Pvtl\\VoyagerPageBlocks\\Http\\Controllers\\PageController', '', 1, 0, NULL, '2018-11-19 01:08:17', '2018-11-20 05:23:09'),
(7, 'page_blocks', 'page-blocks', 'Page Block', 'Page Blocks', 'voyager-file-text', 'Pvtl\\VoyagerPageBlocks\\PageBlock', NULL, '\\Pvtl\\VoyagerPageBlocks\\Http\\Controllers\\PageBlockController', NULL, 1, 0, NULL, '2018-11-20 05:23:09', '2018-11-20 05:23:09'),
(11, 'testimonials', 'testimonials', 'Testimonial', 'Testimonials', NULL, 'App\\Testimonial', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-26 05:49:28', '2018-11-26 06:25:39'),
(14, 'yearloom', 'yearloom', 'Yearloom', 'Yearlooms', NULL, 'App\\Yearloom', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-27 00:37:58', '2018-11-27 00:37:58'),
(15, 'yearlooms', 'yearlooms', 'Yearloom', 'Yearlooms', NULL, 'App\\Yearloom', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-27 00:40:31', '2018-11-27 00:40:31'),
(17, 'about_us_core_features', 'about-us-core-features', 'About Us Core Feature', 'About Us Core Features', NULL, 'App\\AboutUsCoreFeature', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-27 01:01:38', '2018-11-27 01:01:38'),
(19, 'faqs', 'faqs', 'Faq', 'Faqs', NULL, 'App\\Faq', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-12-03 04:14:17', '2018-12-03 04:14:17'),
(21, 'stories', 'stories', 'Story', 'Stories', NULL, 'App\\Story', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-12-17 23:51:56', '2018-12-17 23:54:06'),
(22, 'user_questions', 'user-questions', 'User Question', 'User Questions', NULL, 'App\\Models\\UserQuestion', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-02-25 23:32:55', '2019-02-25 23:34:49'),
(25, 'forum_categories', 'forum-categories', 'Forum Category', 'Forum Categories', NULL, 'App\\Models\\ForumCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-02-26 01:08:26', '2019-02-26 01:08:26'),
(29, 'user_subscriptions', 'user-subscriptions', 'User Subscription', 'User Subscriptions', NULL, 'App\\Models\\UserSubscription', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-03-19 05:42:19', '2019-03-25 07:54:20');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ?', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim,&nbsp;</p>\r\n<p>elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue.</p>', '2018-12-03 04:15:46', '2018-12-03 04:15:46'),
(2, 'printing and typesetting industry?', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim,&nbsp;</p>\r\n<p>elementum ut interdum ut, tempor ac lacus. Duis nec hendrerit felis. Ut gravida congue nisi sed egestas. Nunc nec orci sed odio condimentum vestibulum sit amet et ex. Etiam vel purus diam. Morbi tincidunt vel justo ut congue.</p>', '2018-12-03 04:16:25', '2018-12-03 04:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `forum_categories`
--

CREATE TABLE `forum_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_name` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `user_id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Category1', '2019-02-26 01:08:40', '2019-02-26 01:08:40'),
(2, NULL, 'Category2', '2019-02-26 01:08:49', '2019-02-26 01:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `forum_comments`
--

CREATE TABLE `forum_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `forum_comments`
--

INSERT INTO `forum_comments` (`id`, `user_id`, `forum_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 38, 1, 'fdgdfg', '2019-04-18 12:53:32', '2019-04-18 12:53:32'),
(2, 38, 1, 'gfgdfg', '2019-04-18 12:53:35', '2019-04-18 12:53:35'),
(3, 38, 1, 'gfdgdfgdf', '2019-04-18 12:53:37', '2019-04-18 12:53:37'),
(4, 38, 1, 'hello', '2019-04-18 12:54:04', '2019-04-18 12:54:04'),
(5, 41, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2019-05-20 03:20:54', '2019-05-20 03:20:54'),
(6, 41, 2, 'yada yadayada yada yada', '2019-05-20 03:24:14', '2019-05-20 03:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `group_conversations`
--

CREATE TABLE `group_conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_conversations`
--

INSERT INTO `group_conversations` (`id`, `group_id`, `user_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, 38, 'hye', '2019-04-18 12:55:08', '2019-04-18 12:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE `group_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `notify-status` int(11) NOT NULL DEFAULT '0',
  `status` enum('Invited','Accepted','Rejected','Removed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `group_members`
--

INSERT INTO `group_members` (`id`, `group_id`, `user_id`, `member_id`, `notify-status`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 38, 39, 0, 'Accepted', NULL, '2019-04-18 12:54:18', '2019-04-18 12:54:18');

-- --------------------------------------------------------

--
-- Table structure for table `invite_members`
--

CREATE TABLE `invite_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` int(11) NOT NULL,
  `status` enum('Pending','Accepted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invite_members`
--

INSERT INTO `invite_members` (`id`, `user_id`, `unique_id`, `member_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 38, 'Eyd9QPBl', 39, 'Accepted', '2019-04-18 12:48:50', '2019-04-18 12:49:43'),
(2, 41, 'DwJTQVHM', 42, 'Accepted', '2019-05-20 03:31:52', '2019-05-20 03:33:21');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(2, 'Top Menu', '2018-11-20 05:41:02', '2018-11-20 05:41:02'),
(3, 'Footer Menu', '2018-11-26 23:26:50', '2018-11-26 23:26:50');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-11-19 01:08:10', '2018-11-19 01:08:10', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2018-11-19 01:08:10', '2018-11-26 23:36:09', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-11-19 01:08:10', '2018-11-26 23:36:09', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-11-19 01:08:10', '2018-11-26 23:36:09', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2018-11-19 01:08:10', '2018-11-27 00:41:50', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-11-19 01:08:10', '2018-11-26 06:57:30', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-11-19 01:08:10', '2018-11-27 00:41:49', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-11-19 01:08:10', '2018-11-27 01:02:20', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-11-19 01:08:10', '2018-11-27 01:02:20', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2018-11-19 01:08:10', '2018-11-27 00:41:50', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 10, '2018-11-19 01:08:15', '2018-11-27 00:41:50', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2018-11-19 01:08:16', '2018-11-26 23:36:09', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2018-11-19 01:08:17', '2018-11-26 23:36:09', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-11-19 01:08:19', '2018-11-27 01:02:20', 'voyager.hooks', NULL),
(16, 1, 'Testimonials', '', '_self', 'icon voyager-news', '#000000', 22, 1, '2018-11-26 05:49:29', '2018-11-26 23:36:30', 'voyager.testimonials.index', 'null'),
(18, 2, 'How it works', '/how-it-works', '_self', NULL, '#000000', NULL, 1, '2018-11-26 07:06:27', '2018-11-26 07:20:51', NULL, ''),
(19, 2, 'About Us', '/about-us', '_self', NULL, '#000000', NULL, 11, '2018-11-26 23:25:40', '2018-11-26 23:25:40', NULL, ''),
(20, 3, 'How It works', '/how-it-works', '_self', NULL, '#000000', NULL, 12, '2018-11-26 23:27:10', '2018-11-26 23:27:10', NULL, ''),
(21, 3, 'About Us', '/about-us', '_self', NULL, '#000000', NULL, 13, '2018-11-26 23:27:21', '2018-11-26 23:27:21', NULL, ''),
(22, 1, 'Page Content', '', '_self', 'icon voyager-file-text', '#000000', NULL, 7, '2018-11-26 23:36:01', '2018-11-26 23:37:21', NULL, ''),
(25, 1, 'Yearlooms', '', '_self', 'icon icon voyager-news', '#000000', 22, 2, '2018-11-27 00:40:32', '2018-11-27 00:56:13', 'voyager.yearlooms.index', 'null'),
(27, 1, 'About Us Core Features', '', '_self', 'icon icon icon voyager-news', '#000000', 22, 3, '2018-11-27 01:01:38', '2018-11-27 01:22:08', 'voyager.about-us-core-features.index', 'null'),
(28, 2, 'Faq', '/faq', '_self', NULL, '#000000', NULL, 14, '2018-11-28 01:32:56', '2018-11-28 01:32:56', NULL, ''),
(29, 2, 'Pricing', '/pricing', '_self', NULL, '#000000', NULL, 15, '2018-11-28 01:33:16', '2018-11-28 01:33:16', NULL, ''),
(30, 3, 'Pricing', '/pricing', '_self', NULL, '#000000', NULL, 16, '2018-11-30 12:28:04', '2018-11-30 12:28:04', NULL, ''),
(31, 3, 'FAQ', '/faq', '_self', NULL, '#000000', NULL, 17, '2018-11-30 12:28:22', '2018-11-30 12:28:22', NULL, ''),
(33, 1, 'Faqs', '', '_self', NULL, NULL, NULL, 18, '2018-12-03 04:14:17', '2018-12-03 04:14:17', 'voyager.faqs.index', NULL),
(35, 1, 'Stories', '', '_self', NULL, NULL, NULL, 20, '2018-12-17 23:51:56', '2018-12-17 23:51:56', 'voyager.stories.index', NULL),
(36, 1, 'User Questions', '', '_self', NULL, NULL, NULL, 21, '2019-02-25 23:32:56', '2019-02-25 23:32:56', 'voyager.user-questions.index', NULL),
(37, 1, 'Forum Categories', '', '_self', NULL, NULL, NULL, 22, '2019-02-26 01:08:27', '2019-02-26 01:08:27', 'voyager.forum-categories.index', NULL),
(38, 1, 'User Subscriptions', '', '_self', NULL, NULL, NULL, 23, '2019-03-19 05:42:19', '2019-03-19 05:42:19', 'voyager.user-subscriptions.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(27, '2018_02_11_224531_create_page_blocks_table', 3),
(28, '2018_04_18_000000_create_blog_posts_table', 3),
(29, '2018_04_18_000000_create_pages_table', 3),
(30, '2018_04_19_224316_add_fields_to_pages_table', 3),
(31, '2018_05_09_000000_create_categories_table', 3),
(32, '2018_05_11_000000_remove_blog_keywords_field', 3),
(33, '2018_05_11_000001_remove_pages_keywords_field', 3),
(34, '2018_11_27_092121_alter_users_table', 4),
(35, '2018_11_27_110109_alter_users_table', 5),
(36, '2018_11_27_120402_create_user_relation_table', 6),
(37, '2018_12_05_112244_create_user_relation_parents_table', 7),
(38, '2018_12_05_112532_create_user_relation_partners_table', 7),
(39, '2018_12_05_115159_alter_user_relation_table', 8),
(40, '2018_12_06_055811_alter_users_table', 9),
(41, '2019_01_03_041235_alter_users_table', 10),
(42, '2019_02_08_072045_create_user_photos', 11),
(43, '2019_02_08_072618_create_user_videos', 12),
(46, '2019_02_14_071021_create_user_groups', 13),
(47, '2019_02_14_100929_create_group_members', 14),
(48, '2019_02_22_102118_create_story_members', 15),
(49, '2019_02_25_044015_create_user_forums', 16),
(50, '2019_02_25_062558_create_forum_comments', 17),
(51, '2019_02_25_101421_create_user_invite_family', 18),
(52, '2019_02_25_101806_create_invite_members', 19),
(53, '2019_03_05_054237_create_story_comments', 20),
(54, '2019_03_05_065901_create_story_likes', 21),
(55, '2019_03_11_063923_create_user_conversation', 22),
(56, '2019_03_11_064259_create_user_conversation_message', 23),
(57, '2019_03_12_121628_create_group_conversations', 24),
(58, '2019_03_15_073332_create_user_notifications', 25),
(59, '2019_03_15_073453_create_user_notification', 25),
(60, '2019_03_27_124611_create_user_payment_details', 26);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `layout` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `status`, `created_at`, `updated_at`, `layout`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'ACTIVE', '2018-11-19 01:08:18', '2018-11-19 01:08:18', NULL),
(3, 1, 'Home', 'Home', NULL, NULL, 'home', 'Home', 'ACTIVE', '2018-11-20 05:26:20', '2019-04-01 07:33:39', NULL),
(4, 1, 'How It works', 'How It works', NULL, NULL, 'how-it-works', 'How It works', 'ACTIVE', '2018-11-26 02:29:02', '2018-11-28 00:44:20', NULL),
(5, 1, 'About Us', 'About Us', NULL, NULL, 'about-us', 'About Us', 'ACTIVE', '2018-11-26 23:11:27', '2018-11-26 23:20:41', NULL),
(6, 1, 'Faq', 'Faq', NULL, NULL, 'faq', 'Faq', 'ACTIVE', '2018-11-28 01:31:58', '2018-11-28 01:32:25', NULL),
(7, 1, 'Pricing', 'Pricing', NULL, NULL, 'pricing', 'Pricing', 'ACTIVE', '2018-11-28 01:32:17', '2018-11-28 01:32:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_blocks`
--

CREATE TABLE `page_blocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(11) NOT NULL,
  `type` enum('template','include') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'include',
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `is_minimized` tinyint(1) NOT NULL DEFAULT '0',
  `is_delete_denied` tinyint(1) NOT NULL DEFAULT '0',
  `cache_ttl` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '10000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_blocks`
--

INSERT INTO `page_blocks` (`id`, `page_id`, `type`, `path`, `data`, `is_hidden`, `is_minimized`, `is_delete_denied`, `cache_ttl`, `order`, `created_at`, `updated_at`) VALUES
(4, 3, 'template', 'home_banner', '{\"html_content\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.<\\/p>\",\"image\":\"blocks\\/Q6TpIoQvKbm7hBVE4OQvTAAnUVazHCvS6qWE4yCP.png\",\"br_1\":null,\"title\":\"Share your Stories with the Ones who matter Most.\",\"br_2\":null,\"button_text\":\"Get Started\",\"link\":\"\\/how-it-works\"}', 0, 1, 0, 0, 1, '2018-11-20 07:56:33', '2019-03-28 02:07:30'),
(5, 3, 'template', 'home_story_1', '{\"html_content\":\"<p>Yearloom lets you record and share the best stories of your life with your family. The childhood pranks, teenage adventures, moments of tragedy and all the life changing experiences that make you one of a kind. Tell your stories so that present and future generations can get to know the real you.<\\/p>\",\"image\":\"blocks\\/nq7HuCXsQmeLiKpUHNCzYoVTdsv0h7hjuISH6TJS.png\",\"image_position\":\"1\",\"title\":\"Everyone loves    a Good Story\",\"background_color\":\"#f9f9f9\",\"button_text\":\"Learn More\",\"link\":null}', 0, 1, 0, 0, 2, '2018-11-23 00:07:25', '2019-03-28 02:07:33'),
(6, 3, 'template', 'home_story_1', '{\"html_content\":\"<p>Connect deeply with your very first social network: Your family. Build an integrated family tree with our easy-to-use Tree Builder. Access both sides of your family tree and easily locate and learn about any family member.<\\/p>\",\"image\":\"blocks\\/DIfqUVZd59lLbPZPAJauX3NGuoBBNKrBBBeuQBm9.png\",\"image_position\":\"0\",\"title\":\"Build your Family Tree\",\"background_color\":\"#ffffff\",\"button_text\":\"Start building your Family Tree\",\"link\":null}', 0, 1, 0, 0, 3, '2018-11-23 00:09:14', '2019-03-28 02:07:35'),
(9, 3, 'template', 'home_story_1', '{\"html_content\":\"<p>It&rsquo;s totally normal to not know which story to tell. So every week we&rsquo;ll send you a question about your life that you can use as inspiration to get started.<\\/p>\",\"image\":\"blocks\\/ep6nimC2nI4pmNeGucRdqyvvwjDyeMEUTGepSnOX.png\",\"image_position\":\"1\",\"title\":\"Lost for Words?\",\"background_color\":\"#ffffff\",\"button_text\":\"Learn More\",\"link\":\"\\/how-it-works\"}', 0, 1, 0, 0, 5, '2018-11-26 01:34:45', '2018-11-26 02:04:09'),
(10, 3, 'template', 'home_story_1', '{\"html_content\":\"<p>Record and upload your stories in text, video, photos or audio. Yearloom is built to safely store your memories for the future, no matter the format.&nbsp;<\\/p>\",\"image\":\"blocks\\/x6PGWdiCIngjj40ZBJusevkJKWmO33P0UvQzgegr.png\",\"image_position\":\"0\",\"title\":\"Store Photos, Video and  Audio Safely\",\"background_color\":\"#f9f9f9\",\"button_text\":null,\"link\":null}', 0, 1, 0, 0, 6, '2018-11-26 01:42:17', '2018-11-26 02:04:09'),
(11, 3, 'template', 'home_story_1', '{\"html_content\":\"<p>Your treasures are well guarded. Everything you store with us is 100% yours and we will never share it with anyone. You choose who to share your content with and you can retrieve or delete any of your content at any time.<\\/p>\",\"image\":\"blocks\\/Sjn4Z0AqKKgZSujqZEo4g7OqCd6ytEBJhLb8DOGO.png\",\"image_position\":\"1\",\"title\":\"Private, secure and all yours\",\"background_color\":\"#ffffff\",\"button_text\":null,\"link\":null}', 0, 1, 0, 0, 7, '2018-11-26 01:51:51', '2018-11-26 02:04:09'),
(12, 3, 'template', 'home_story_1', '{\"html_content\":\"<p>Sign up now for free and start telling the stories that made you, you.<\\/p>\",\"image\":\"blocks\\/HagKCK0L5NIDQV5sHNzgjV4wNHnBFKgbodoTInaY.png\",\"image_position\":\"0\",\"title\":\"Welcome to the family\",\"background_color\":\"#f9f9f9\",\"button_text\":\"Sign Up\",\"button_class\":\"user_sign_up\",\"link\":\"javascript:void(0);\"}', 0, 1, 0, 0, 9, '2018-11-26 01:53:29', '2019-04-01 07:32:28'),
(13, 3, 'template', 'testimonial_no_background_image', '[]', 0, 0, 0, 0, 4, '2018-11-26 02:03:37', '2019-03-28 02:07:39'),
(14, 3, 'template', 'testimonial_with_background_image', '[]', 0, 1, 0, 0, 8, '2018-11-26 02:06:46', '2019-04-01 07:33:39'),
(15, 4, 'template', 'inner_pages_banner', '{\"html_content\":\"<p>Yearloom was created for families to share life stories, to connect deeply with one another and to leave these stories behind for the benefit of future generations. To achieve this we&rsquo;ve created a private platform that makes connecting with family easy and powerful.<\\/p>\",\"image\":\"blocks\\/mKSAaiepXKEEfZn0PAW6PVOTxYKWkoXrFEe8Wg31.png\",\"image_position\":\"1\",\"title\":\"How it works\",\"background_color\":\"#20c4f4\",\"button_text\":\"Learn More\",\"link\":null}', 0, 1, 0, 0, 10000, '2018-11-26 02:42:59', '2018-11-26 04:36:58'),
(19, 4, 'template', 'how-it-works', '{\"html_content\":\"<div class=\\\"feature_bx wow fadeIn\\\" style=\\\"visibility: visible; animation-duration: 1000ms; animation-delay: 0.1s; animation-name: fadeIn;\\\" data-wow-delay=\\\"0.1s\\\" data-wow-duration=\\\"1000ms\\\">\\r\\n<h2>Sign Up<\\/h2>\\r\\n<p>Get started by signing up, completing your profile and selecting a membership option.<\\/p>\\r\\n<\\/div>\\r\\n<div class=\\\"feature_bx wow fadeIn\\\" style=\\\"visibility: visible; animation-duration: 1000ms; animation-delay: 0.2s; animation-name: fadeIn;\\\" data-wow-delay=\\\"0.2s\\\" data-wow-duration=\\\"1000ms\\\">\\r\\n<h2>Grow your Family Tree<\\/h2>\\r\\n<p>Start growing your family tree by inviting family members to join your tree.<\\/p>\\r\\n<\\/div>\\r\\n<div class=\\\"feature_bx wow fadeIn\\\" style=\\\"visibility: visible; animation-duration: 1000ms; animation-delay: 0.3s; animation-name: fadeIn;\\\" data-wow-delay=\\\"0.3s\\\" data-wow-duration=\\\"1000ms\\\">\\r\\n<h2>Tell your stories<\\/h2>\\r\\n<p>Once a week, Yearloom will email you a question about your life that you&rsquo;ve probably never been asked before. You can answer in any way you choose. You can write out your story in full descriptive texts and add photos, or if you&rsquo;re more of an orator, you can record video or audio of yourself telling the story and upload it.<\\/p>\\r\\n<p>It doesn&rsquo;t matter how you tell your story. The goal is to recapture these moments in time so that you, your children and their children can look back and read, see and hear what your life was really like, straight from the source, you!<\\/p>\\r\\n<\\/div>\\r\\n<div class=\\\"feature_bx wow fadeIn\\\" style=\\\"visibility: hidden; animation-duration: 1000ms; animation-delay: 0.4s; animation-name: none;\\\" data-wow-delay=\\\"0.4s\\\" data-wow-duration=\\\"1000ms\\\">\\r\\n<h2>Explore your Family Tree<\\/h2>\\r\\n<p>The more stories you add, the more valuable the tree becomes. We encourage our users to share stories freely. Yearloom is completely private and no one outside your family will ever see what you&rsquo;ve shared. We believe that everything has a ripple effect and some people look to their family for inspiration and validation. Sharing an experience or an achievement that you might have had in your life could have a major positive effect on someone else on your tree.<\\/p>\\r\\n<\\/div>\\r\\n<div class=\\\"feature_bx wow fadeIn\\\" style=\\\"visibility: hidden; animation-duration: 1000ms; animation-delay: 0.5s; animation-name: none;\\\" data-wow-delay=\\\"0.5s\\\" data-wow-duration=\\\"1000ms\\\">\\r\\n<h2>Visit the Forum Area<\\/h2>\\r\\n<p>The forum is where family members can get together, pool their wisdom or discuss topics of interest. For example, families can have a Business Forum where successful family members can give advice to the younger ambitions ones, or a Philosophy forum where some members can discuss moral and ethical ideas. Or even a TV forum where members talk about and recommend their best shows on Netflix. The discussion topics are limitless in the forum area.<\\/p>\\r\\n<\\/div>\"}', 0, 0, 0, 0, 10000, '2018-11-26 04:21:06', '2018-11-28 00:44:20'),
(20, 5, 'template', 'page-content', '{\"html_content\":\"<p>We started this project by asking, what binds a family together? Why does one family&rsquo;s bonds remain strong over generations, while others become estranged to each other?<\\/p>\\r\\n<p>What we realized was that family members who share personal stories with each other develop a closer bond over time than those who do not. Families that share their business and professional knowledge with each other seem to find more individual successes than those that do not, and families that are able to discuss and share their ideas and opinions openly, find more overall happiness and well-being.<\\/p>\\r\\n<p>The significance of family bonds emerged again when we looked at the greatest regrets that people frequently cite when close to the end of their lives. Chief among these is that they would easily trade their material possessions for just a bit more time to spend with their family.<\\/p>\\r\\n<p>We were left to conclude that the most valuable thing you can give to someone is your time. It&rsquo;s the one thing we can&rsquo;t get back, but we can make a record of how it was spent. If you could leave behind stories of the most important moments of your life, that would be priceless to your family.<\\/p>\\r\\n<p>The years of your life re-counted by you in person, would be the most valuable heirloom you could hand down.<\\/p>\\r\\n<p>That&rsquo;s why we created Yearloom.<\\/p>\"}', 0, 0, 0, 0, 10000, '2018-11-26 23:19:51', '2018-11-26 23:20:41'),
(23, 4, 'template', 'year-loom', '{\"title\":\"Why use Yearloom?\",\"background_color\":\"#20c4f4\"}', 0, 1, 0, 0, 10000, '2018-11-27 00:03:53', '2018-11-28 00:43:07');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(2, 'browse_bread', NULL, '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(3, 'browse_database', NULL, '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(4, 'browse_media', NULL, '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(5, 'browse_compass', NULL, '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(6, 'browse_menus', 'menus', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(7, 'read_menus', 'menus', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(8, 'edit_menus', 'menus', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(9, 'add_menus', 'menus', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(10, 'delete_menus', 'menus', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(11, 'browse_roles', 'roles', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(12, 'read_roles', 'roles', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(13, 'edit_roles', 'roles', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(14, 'add_roles', 'roles', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(15, 'delete_roles', 'roles', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(16, 'browse_users', 'users', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(17, 'read_users', 'users', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(18, 'edit_users', 'users', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(19, 'add_users', 'users', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(20, 'delete_users', 'users', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(21, 'browse_settings', 'settings', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(22, 'read_settings', 'settings', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(23, 'edit_settings', 'settings', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(24, 'add_settings', 'settings', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(25, 'delete_settings', 'settings', '2018-11-19 01:08:11', '2018-11-19 01:08:11'),
(26, 'browse_categories', 'categories', '2018-11-19 01:08:15', '2018-11-19 01:08:15'),
(27, 'read_categories', 'categories', '2018-11-19 01:08:15', '2018-11-19 01:08:15'),
(28, 'edit_categories', 'categories', '2018-11-19 01:08:15', '2018-11-19 01:08:15'),
(29, 'add_categories', 'categories', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(30, 'delete_categories', 'categories', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(31, 'browse_posts', 'posts', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(32, 'read_posts', 'posts', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(33, 'edit_posts', 'posts', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(34, 'add_posts', 'posts', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(35, 'delete_posts', 'posts', '2018-11-19 01:08:16', '2018-11-19 01:08:16'),
(36, 'browse_pages', 'pages', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(37, 'read_pages', 'pages', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(38, 'edit_pages', 'pages', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(39, 'add_pages', 'pages', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(40, 'delete_pages', 'pages', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(41, 'browse_hooks', NULL, '2018-11-19 01:08:19', '2018-11-19 01:08:19'),
(42, 'browse_page_blocks', 'page_blocks', '2018-11-20 05:23:09', '2018-11-20 05:23:09'),
(43, 'read_page_blocks', 'page_blocks', '2018-11-20 05:23:09', '2018-11-20 05:23:09'),
(44, 'edit_page_blocks', 'page_blocks', '2018-11-20 05:23:09', '2018-11-20 05:23:09'),
(45, 'add_page_blocks', 'page_blocks', '2018-11-20 05:23:09', '2018-11-20 05:23:09'),
(46, 'delete_page_blocks', 'page_blocks', '2018-11-20 05:23:09', '2018-11-20 05:23:09'),
(47, 'browse_testimonials', 'testimonials', '2018-11-26 05:49:29', '2018-11-26 05:49:29'),
(48, 'read_testimonials', 'testimonials', '2018-11-26 05:49:29', '2018-11-26 05:49:29'),
(49, 'edit_testimonials', 'testimonials', '2018-11-26 05:49:29', '2018-11-26 05:49:29'),
(50, 'add_testimonials', 'testimonials', '2018-11-26 05:49:29', '2018-11-26 05:49:29'),
(51, 'delete_testimonials', 'testimonials', '2018-11-26 05:49:29', '2018-11-26 05:49:29'),
(57, 'browse_yearloom', 'yearloom', '2018-11-27 00:37:58', '2018-11-27 00:37:58'),
(58, 'read_yearloom', 'yearloom', '2018-11-27 00:37:58', '2018-11-27 00:37:58'),
(59, 'edit_yearloom', 'yearloom', '2018-11-27 00:37:58', '2018-11-27 00:37:58'),
(60, 'add_yearloom', 'yearloom', '2018-11-27 00:37:58', '2018-11-27 00:37:58'),
(61, 'delete_yearloom', 'yearloom', '2018-11-27 00:37:58', '2018-11-27 00:37:58'),
(62, 'browse_yearlooms', 'yearlooms', '2018-11-27 00:40:32', '2018-11-27 00:40:32'),
(63, 'read_yearlooms', 'yearlooms', '2018-11-27 00:40:32', '2018-11-27 00:40:32'),
(64, 'edit_yearlooms', 'yearlooms', '2018-11-27 00:40:32', '2018-11-27 00:40:32'),
(65, 'add_yearlooms', 'yearlooms', '2018-11-27 00:40:32', '2018-11-27 00:40:32'),
(66, 'delete_yearlooms', 'yearlooms', '2018-11-27 00:40:32', '2018-11-27 00:40:32'),
(72, 'browse_about_us_core_features', 'about_us_core_features', '2018-11-27 01:01:38', '2018-11-27 01:01:38'),
(73, 'read_about_us_core_features', 'about_us_core_features', '2018-11-27 01:01:38', '2018-11-27 01:01:38'),
(74, 'edit_about_us_core_features', 'about_us_core_features', '2018-11-27 01:01:38', '2018-11-27 01:01:38'),
(75, 'add_about_us_core_features', 'about_us_core_features', '2018-11-27 01:01:38', '2018-11-27 01:01:38'),
(76, 'delete_about_us_core_features', 'about_us_core_features', '2018-11-27 01:01:38', '2018-11-27 01:01:38'),
(82, 'browse_faqs', 'faqs', '2018-12-03 04:14:17', '2018-12-03 04:14:17'),
(83, 'read_faqs', 'faqs', '2018-12-03 04:14:17', '2018-12-03 04:14:17'),
(84, 'edit_faqs', 'faqs', '2018-12-03 04:14:17', '2018-12-03 04:14:17'),
(85, 'add_faqs', 'faqs', '2018-12-03 04:14:17', '2018-12-03 04:14:17'),
(86, 'delete_faqs', 'faqs', '2018-12-03 04:14:17', '2018-12-03 04:14:17'),
(92, 'browse_stories', 'stories', '2018-12-17 23:51:56', '2018-12-17 23:51:56'),
(93, 'read_stories', 'stories', '2018-12-17 23:51:56', '2018-12-17 23:51:56'),
(94, 'edit_stories', 'stories', '2018-12-17 23:51:56', '2018-12-17 23:51:56'),
(95, 'add_stories', 'stories', '2018-12-17 23:51:56', '2018-12-17 23:51:56'),
(96, 'delete_stories', 'stories', '2018-12-17 23:51:56', '2018-12-17 23:51:56'),
(97, 'browse_user_questions', 'user_questions', '2019-02-25 23:32:56', '2019-02-25 23:32:56'),
(98, 'read_user_questions', 'user_questions', '2019-02-25 23:32:56', '2019-02-25 23:32:56'),
(99, 'edit_user_questions', 'user_questions', '2019-02-25 23:32:56', '2019-02-25 23:32:56'),
(100, 'add_user_questions', 'user_questions', '2019-02-25 23:32:56', '2019-02-25 23:32:56'),
(101, 'delete_user_questions', 'user_questions', '2019-02-25 23:32:56', '2019-02-25 23:32:56'),
(102, 'browse_forum_categories', 'forum_categories', '2019-02-26 01:08:27', '2019-02-26 01:08:27'),
(103, 'read_forum_categories', 'forum_categories', '2019-02-26 01:08:27', '2019-02-26 01:08:27'),
(104, 'edit_forum_categories', 'forum_categories', '2019-02-26 01:08:27', '2019-02-26 01:08:27'),
(105, 'add_forum_categories', 'forum_categories', '2019-02-26 01:08:27', '2019-02-26 01:08:27'),
(106, 'delete_forum_categories', 'forum_categories', '2019-02-26 01:08:27', '2019-02-26 01:08:27'),
(107, 'browse_user_subscriptions', 'user_subscriptions', '2019-03-19 05:42:19', '2019-03-19 05:42:19'),
(108, 'read_user_subscriptions', 'user_subscriptions', '2019-03-19 05:42:19', '2019-03-19 05:42:19'),
(109, 'edit_user_subscriptions', 'user_subscriptions', '2019-03-19 05:42:19', '2019-03-19 05:42:19'),
(110, 'add_user_subscriptions', 'user_subscriptions', '2019-03-19 05:42:19', '2019-03-19 05:42:19'),
(111, 'delete_user_subscriptions', 'user_subscriptions', '2019-03-19 05:42:19', '2019-03-19 05:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-19 01:08:17', '2018-11-19 01:08:17'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-19 01:08:17', '2018-11-19 01:08:17'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-19 01:08:17', '2018-11-19 01:08:17'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-19 01:08:17', '2018-11-19 01:08:17');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-11-19 01:08:10', '2018-11-19 01:08:10'),
(2, 'user', 'Normal User', '2018-11-19 01:08:10', '2018-11-19 01:08:10');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Year Loom', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\November2018\\BUgHLKPj6hVMh3fO01WQ.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'site.facebook_link', 'Facebook Link', 'https://www.facebook.com/', NULL, 'text', 7, 'Site'),
(12, 'site.twitter_link', 'Twitter Link', 'https://twitter.com/login', NULL, 'text', 6, 'Site'),
(13, 'site.linkdin_link', 'Linkdin Link', 'https://in.linkedin.com/', NULL, 'text', 8, 'Site');

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_photo_id` int(11) DEFAULT NULL,
  `user_video_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `group_id`, `question_id`, `user_id`, `user_photo_id`, `user_video_id`, `title`, `excerpt`, `description`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '38', 1, 1, 'test', 'Hgfhgf', '<p>hgfhgfh</p>', '2019-04-18 12:50:10', '2019-04-18 12:51:43'),
(2, NULL, 1, '38', 3, NULL, 'What is your name?', 'Test', 'Test', '2019-04-18 12:55:26', '2019-04-18 12:55:26'),
(3, 1, NULL, '38', 4, NULL, 'fdsfdsf', 'Fsdfdf', '<p>dsfdsf</p>', '2019-04-18 13:03:00', '2019-04-18 13:03:00'),
(4, NULL, NULL, '41', NULL, NULL, 'My First Story', 'The first story on Yearloom', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', '2019-05-20 03:10:22', '2019-05-20 03:10:22'),
(5, NULL, NULL, '41', NULL, NULL, '2nd Story', '2nd story', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', '2019-05-20 03:16:21', '2019-05-20 03:16:21'),
(6, NULL, NULL, '42', NULL, NULL, 'Dads 1st Story', 'Dads 1st Story', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', '2019-05-20 03:44:45', '2019-05-20 03:44:45');

-- --------------------------------------------------------

--
-- Table structure for table `story_comments`
--

CREATE TABLE `story_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `story_id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `story_comments`
--

INSERT INTO `story_comments` (`id`, `user_id`, `story_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 38, 1, 'Hye', '2019-04-18 12:50:24', '2019-04-18 12:50:24'),
(2, 38, 1, 'gfdgdf', '2019-04-18 13:01:14', '2019-04-18 13:01:14'),
(3, 38, 1, 'hye', '2019-04-18 13:02:32', '2019-04-18 13:02:32'),
(5, 38, 3, 'f', '2019-04-18 13:03:13', '2019-04-18 13:03:13');

-- --------------------------------------------------------

--
-- Table structure for table `story_likes`
--

CREATE TABLE `story_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `story_id` int(11) NOT NULL,
  `like` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `story_likes`
--

INSERT INTO `story_likes` (`id`, `user_id`, `story_id`, `like`, `created_at`, `updated_at`) VALUES
(1, 38, 1, 1, '2019-04-18 12:50:17', '2019-04-18 12:50:17');

-- --------------------------------------------------------

--
-- Table structure for table `story_members`
--

CREATE TABLE `story_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `story_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `story_members`
--

INSERT INTO `story_members` (`id`, `story_id`, `member_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 39, 0, '2019-04-18 12:55:26', '2019-04-18 12:55:26'),
(2, 3, 39, 0, '2019-04-18 13:03:00', '2019-04-18 13:03:00'),
(3, 6, 41, 0, '2019-05-20 03:44:45', '2019-05-20 03:44:45');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `title`, `content`, `created_at`, `updated_at`, `image`) VALUES
(1, 'John Smith', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.</p>', '2018-11-26 06:23:25', '2018-11-26 06:26:11', 'testimonials\\November2018\\28lE7RNK31H2oNTP5yIU.jpg'),
(2, 'John Smith 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.</p>', '2018-11-26 06:26:46', '2018-11-26 06:26:46', 'testimonials\\November2018\\PCwm8oK5HtPzK8Nh89pN.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-11-19 01:08:18', '2018-11-19 01:08:18'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-11-19 01:08:18', '2018-11-19 01:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `unique_id` text COLLATE utf8mb4_unicode_ci,
  `invited_by_id` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'bydefault.png',
  `cover_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'cover-img.jpg',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `bio_title` text COLLATE utf8mb4_unicode_ci,
  `bio_short_description` text COLLATE utf8mb4_unicode_ci,
  `bio_description` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `register_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `unique_id`, `invited_by_id`, `name`, `email`, `avatar`, `cover_img`, `email_verified_at`, `bio_title`, `bio_short_description`, `bio_description`, `password`, `provider`, `provider_id`, `remember_token`, `settings`, `created_at`, `updated_at`, `dob`, `register_from`, `login_from`, `gender`) VALUES
(1, 1, '', NULL, 'Admin', 'admin@admin.com', 'users/default.png', NULL, NULL, '', '', '', '$2y$10$dFDKHU8nxk4fqN3xXFXg/.rnJpZsSB33TCsBT1HXNHbNMX95h1eFq', NULL, NULL, 'huLHMbwg6tAMImvUfMQDjkemMvQNKxbjPQYl6iwqkGfNbUmkYnA9F1bTumNB', NULL, '2018-11-19 01:08:16', '2018-11-19 01:08:16', NULL, NULL, NULL, NULL),
(37, 2, 'tvjw8kB4', NULL, 'Test User', 'gurjeevan@gmail.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$y239eInditMDpCFc4IjGq.2WH0oL/vCoNDcjWvehAe5j2sMdzkRXW', NULL, NULL, 'SLklGksXgV2rkvHNHDEqo0VEF0GqMLki8kzS9nZ9BfFY1sl6BBZY8NIiraDN', NULL, '2019-04-18 12:47:15', '2019-04-18 12:47:15', '2019-04-02', NULL, NULL, 'female'),
(38, 2, '8OAHYKWD', '', 'Gurjeevan Kaur', 'gurjeevan.kaur@imarkinfotech.com', '1555591926.jpg', '1555591699.jpg', NULL, 'Hye', NULL, NULL, '$2y$10$8RJPQCd5lGKLmZFIlI4g7ebM.Pqpag8kk77TGhk4YnQMHQRvGadOq', 'google', '115269186546572795983', '5e4piz99eY7brOx10CQjHtneGaMFEgnAodQi8rsD6qx9CQhXv6n15vtpu6Bj', NULL, '2019-04-18 12:47:38', '2019-05-21 09:49:38', '2019-05-16', NULL, NULL, NULL),
(39, 2, 'Eyd9QPBl', '38', 'Shivam Virli', 'shivam.virli@imarkinfotech.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$dFDKHU8nxk4fqN3xXFXg/.rnJpZsSB33TCsBT1HXNHbNMX95h1eFq', 'google', '113262664377709363270', '4oY3XfB43RSbg5OzCD8NuaHWZQKagdqmsuY8jfKKQB1Ur9Xy9CcYeufKJ8Sh', NULL, '2019-04-18 12:49:43', '2019-04-18 12:49:43', NULL, NULL, NULL, NULL),
(40, 2, 'hi13cpLM', '', 'Harwinder Singh', 'harwinder@imarkinfotech.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$tETlZoBX6IPwmqXjSd5hv.24rsOhENFg9edBDjCAjwjPF6z.1nyL6', 'google', '115365403144911974139', 'OT3ypkcLc0zQxm95kBmaMwOtkUXxgY0PPiZmtdadHLl2tk58HQNmuq065DsA', NULL, '2019-04-19 04:43:32', '2019-04-19 04:43:32', NULL, NULL, NULL, NULL),
(41, 2, 'o2Qjf15X', NULL, 'Shawn CA', 'adventcorpltd@gmail.com', '1558321849.jpg', '1558321934.png', NULL, NULL, NULL, NULL, '$2y$10$LOrbDTQxCnY53JdwcEtya.R3.StV6N1YjV6MOUq52owLArml8J3Xi', NULL, NULL, NULL, NULL, '2019-05-20 03:07:52', '2019-05-20 03:15:33', '1970-01-01', NULL, NULL, 'male'),
(42, 2, 'DwJTQVHM', '41', 'Dad CA', 's.chongashing@gmail.com', '1558323416.jpg', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$9CBWzjce9YCJ3xZw6dGszOHdgE5Wz3gRAl8VXSDgfyZVwnb45Apvu', NULL, NULL, NULL, NULL, '2019-05-20 03:33:21', '2019-05-20 03:36:56', '1970-01-01', NULL, NULL, 'male'),
(43, 2, 'rK4FRG2H', NULL, 'Gurjeevan Kaur', 'gurjeevan.kaur@imajkjkrkinfotech.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$WzpssF5cdouGdTeYnlQuLOSr1v3Xdd9hTmN8.y74Kmhhibt3ImK4y', NULL, NULL, 'UnxY96iLXCzyM69XRrzzZLZwubFdsa5SykteOfS5iFKLEtydHs2aziU5dAPX', NULL, '2019-05-21 09:52:54', '2019-05-21 09:52:55', '1970-01-01', NULL, NULL, 'male'),
(44, 2, 'sfLv1Eh2', NULL, 'Gurjeevan Kaur', 'gurjeevan.kaur@imarkijghnfotech.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$8Gv2jY0q1M6DzelceN1Roe/pu0TWuklnyKufALSeBBZNR9tWq8lMa', NULL, NULL, 'w17Dn1pH1DTBonmpqZo5aVJ8AUDjcJTjmlNDtA4wX4LqkSGh2mrBbS7kk6AP', NULL, '2019-05-21 09:59:07', '2019-05-21 09:59:07', '1970-01-01', NULL, NULL, 'female'),
(45, 2, '8P43biz6', NULL, 'Gurjeevan Kaur', 'gurjeevan.kaur@imafdsfrkinfotech.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$TYuZ8tttVkyZWgwBDlX9kOPEU1k1JoOo12mntAIQK3GLWKWqkzaMu', NULL, NULL, 'jqUYrUMPNLAqy16nZjHEGpA5dgzxtiGpQUiUfHi3GnoemmM5GzpMWwXKOGin', NULL, '2019-05-21 10:00:34', '2019-05-21 10:00:34', '1970-01-01', NULL, NULL, 'female'),
(46, 2, 'krX2ubv5', NULL, 'Gurjeevan Kaur', 'gurjeevan.kaur@imarkinfdgfotech.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$7cN5iEM12axZCIbT8d8kgetUkto90zqzMdoqtaePNTK7EylO28dHq', NULL, NULL, 'Lqh2UxzpI22W3AsLIr04wQsRz0Gz9KnlyVx9bdo5V7zPCMDQd652amfCSLAB', NULL, '2019-05-21 10:04:26', '2019-05-21 10:04:26', '2019-05-21', NULL, NULL, 'female'),
(47, 2, 'mcQAv5PX', NULL, 'Gurjeevan Kaur', 'gurjeevan.kaur@imardsfkinfotech.com', 'bydefault.png', 'cover-img.jpg', NULL, NULL, NULL, NULL, '$2y$10$zAAXTik8Rt/PsufuKwJgtuoZtRlm0svXRgP6ZW8o5L0z7e3R3.bie', NULL, NULL, 'j8mQfgdGsRPPmaTVkcHffSjavWOCD8jtKNZxa6WN5TwUWugSJGdXM9jQ0mix', NULL, '2019-05-21 10:06:37', '2019-05-21 10:06:37', NULL, NULL, NULL, 'female');

-- --------------------------------------------------------

--
-- Table structure for table `user_conversations`
--

CREATE TABLE `user_conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_conversations`
--

INSERT INTO `user_conversations` (`id`, `from`, `to`, `created_at`, `updated_at`) VALUES
(1, 38, 39, '2019-04-18 13:08:43', '2019-04-18 13:08:43');

-- --------------------------------------------------------

--
-- Table structure for table `user_conversation_messages`
--

CREATE TABLE `user_conversation_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `chat_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_conversation_messages`
--

INSERT INTO `user_conversation_messages` (`id`, `chat_id`, `sender_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, 38, 'hye', '2019-04-18 13:08:43', '2019-04-18 13:08:43'),
(2, 1, 39, 'fdfds', '2019-04-18 13:11:07', '2019-04-18 13:11:07'),
(3, 1, 39, 'hello', '2019-05-21 05:32:26', '2019-05-21 05:32:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_forums`
--

CREATE TABLE `user_forums` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_forums`
--

INSERT INTO `user_forums` (`id`, `user_id`, `title`, `cat_id`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 38, 'ggdfggf', 1, 'Gfdgdfgdf', '1555592003.jpg', '2019-04-18 12:53:23', '2019-04-18 12:53:23'),
(2, 41, 'New Forum', 1, 'New Forum', '1558322365.jpg', '2019-05-20 03:19:25', '2019-05-20 03:19:25'),
(3, 47, 'ghfhgh', 2, 'Ggdf', '1558433486.jpg', '2019-05-21 10:11:26', '2019-05-21 10:11:26'),
(4, 47, 'fdsf', 2, 'Fdsfdsf', NULL, '2019-05-21 10:16:51', '2019-05-21 10:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'logo.png',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `user_id`, `group_name`, `profile_image`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 38, 'ghfhgh', '1555592058.jpg', 'Hghgfgh', NULL, '2019-04-18 12:54:18', '2019-04-18 12:54:18'),
(2, 41, 'New group', 'logo.png', 'Yada Yada Yada Yada Yada Yada Yada Yada', NULL, '2019-05-20 03:25:21', '2019-05-20 03:25:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_invite_family`
--

CREATE TABLE `user_invite_family` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `status` enum('Pending','Accepted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `notification` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `sender_id`, `notification`, `model_name`, `table_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 39, 38, 'New forum ggdfggf added byGurjeevan Kaur', 'Forum', 1, 0, '2019-04-18 12:53:23', '2019-04-18 12:53:23'),
(2, 39, 38, 'New comment added in forum', 'Forum', 1, 0, '2019-04-18 12:53:32', '2019-04-18 12:53:32'),
(3, 39, 38, 'New comment added in forum', 'Forum', 1, 0, '2019-04-18 12:53:35', '2019-04-18 12:53:35'),
(4, 39, 38, 'New comment added in forum', 'Forum', 1, 0, '2019-04-18 12:53:37', '2019-04-18 12:53:37'),
(5, 39, 38, 'New comment added in forum', 'Forum', 1, 0, '2019-04-18 12:54:05', '2019-04-18 12:54:05'),
(6, 39, 38, 'New group ghfhgh created byGurjeevan Kaur', 'UserGroup', 1, 0, '2019-04-18 12:54:18', '2019-04-18 12:54:18'),
(7, 39, 38, 'New answer for question What is your name? added by Gurjeevan Kaur', 'Story', 2, 0, '2019-04-18 12:55:26', '2019-04-18 12:55:26'),
(8, 39, 38, 'New story fdsfdsf added by Gurjeevan Kaur under group ghfhgh', 'Story', 3, 0, '2019-04-18 13:03:00', '2019-04-18 13:03:00'),
(9, 39, 38, 'New comment added by Gurjeevan Kaur under group story fdsfdsf', 'Comment', 3, 0, '2019-04-18 13:03:11', '2019-04-18 13:03:11'),
(10, 39, 38, 'New comment added by Gurjeevan Kaur under group story fdsfdsf', 'Comment', 3, 0, '2019-04-18 13:03:13', '2019-04-18 13:03:13'),
(11, 38, 39, 'New conversation message', 'Chat', 1, 0, '2019-04-18 13:08:43', '2019-04-18 13:08:43'),
(12, 41, 42, 'New Story Dads 1st Story added by Dad CA', 'Story', 6, 0, '2019-05-20 03:44:45', '2019-05-20 03:44:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_payment_details`
--

CREATE TABLE `user_payment_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `txn_id` text COLLATE utf8mb4_unicode_ci,
  `subscriber_id` int(11) DEFAULT NULL,
  `subscription_status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan_price` float NOT NULL,
  `payer_email` text COLLATE utf8mb4_unicode_ci,
  `transaction_date` timestamp NULL DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_payment_details`
--

INSERT INTO `user_payment_details` (`id`, `user_id`, `txn_id`, `subscriber_id`, `subscription_status`, `plan`, `plan_price`, `payer_email`, `transaction_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 17, 'Free', NULL, 'Active', 'Basic', 0, NULL, '2019-04-18 04:53:35', 'Completed', '2019-04-18 04:53:35', '2019-04-18 04:53:35'),
(2, 39, 'Free', NULL, 'Active', 'Basic', 0, NULL, '2019-05-21 05:08:39', 'Completed', '2019-05-21 05:08:39', '2019-05-21 05:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_photos`
--

CREATE TABLE `user_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_photos`
--

INSERT INTO `user_photos` (`id`, `user_id`, `user_photo`, `created_at`, `updated_at`) VALUES
(1, 38, '1555591903.jpg', '2019-04-18 12:50:10', '2019-04-18 12:51:43'),
(2, 38, '1555591862_0.jpg', '2019-04-18 12:51:02', '2019-04-18 12:51:02'),
(3, 38, '1555592126.jpg', '2019-04-18 12:55:26', '2019-04-18 12:55:26'),
(4, 38, '1555592580.jpg', '2019-04-18 13:03:00', '2019-04-18 13:03:00'),
(5, 41, '1558322245_0.png', '2019-05-20 03:17:25', '2019-05-20 03:17:25'),
(6, 41, '1558322245_1.jpg', '2019-05-20 03:17:25', '2019-05-20 03:17:25'),
(7, 41, '1558322245_2.jpg', '2019-05-20 03:17:25', '2019-05-20 03:17:25');

-- --------------------------------------------------------

--
-- Table structure for table `user_questions`
--

CREATE TABLE `user_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_questions`
--

INSERT INTO `user_questions` (`id`, `question`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'What is your name?', NULL, 0, '2019-04-10 02:33:44', '2019-04-10 02:33:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_relations`
--

CREATE TABLE `user_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_id` text COLLATE utf8mb4_unicode_ci,
  `gender` text COLLATE utf8mb4_unicode_ci,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maiden_name` text COLLATE utf8mb4_unicode_ci,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation_with` int(11) DEFAULT NULL,
  `parent` text COLLATE utf8mb4_unicode_ci,
  `profile_image` text COLLATE utf8mb4_unicode_ci,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `borndate` date DEFAULT NULL,
  `bornplace` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `live_dead` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_relations`
--

INSERT INTO `user_relations` (`id`, `user_id`, `unique_id`, `gender`, `first_name`, `maiden_name`, `last_name`, `relation`, `relation_with`, `parent`, `profile_image`, `email_address`, `borndate`, `bornplace`, `live_dead`, `created_at`, `updated_at`) VALUES
(1, 22, NULL, NULL, 'Gurjeevan Kaur', '', 'Gurjeevan Kaur', '0', NULL, '2,3', NULL, 'gurjeevan.kaur@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 05:14:56', '2019-04-18 05:14:56'),
(2, 22, 'k82sypHR', 'male', 'Unknown', '', 'Father', 'Father', 1, NULL, NULL, 'shivan.virli@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 05:14:56', '2019-04-18 05:16:01'),
(3, 22, 'LBlj2mgz', 'female', 'My', '', 'Mother', 'Mother', 1, NULL, NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 05:14:56', '2019-04-18 05:20:51'),
(4, 23, NULL, NULL, 'Harwinder Singh', '', 'Harwinder Singh', '0', NULL, '5,6', NULL, 'harwinder@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 05:15:43', '2019-04-18 05:15:43'),
(5, 23, 'lBAcuhq2', 'male', 'Unknown', '', 'Father', 'Father', 4, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 05:15:43', '2019-04-18 05:15:43'),
(6, 23, 'ZrPeGYln', 'female', 'Unknown', '', 'Mother', 'Mother', 4, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 05:15:43', '2019-04-18 05:15:43'),
(7, 24, 'k82sypHR', 'male', 'Shivam', '', 'Virli', '0', 0, '8,9', NULL, 'shivan.virli@imarkinfotech.com', '2019-04-02', NULL, 'Living', '2019-04-18 05:18:33', '2019-04-18 05:18:33'),
(8, 24, 'quCN0B4h', 'male', 'Test', '', 'Father', 'Father', 7, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 05:18:33', '2019-04-18 05:18:33'),
(9, 24, 'MqK4wBec', 'female', 'Test', '', 'Mother', 'Mother', 7, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 05:18:33', '2019-04-18 05:18:33'),
(10, 25, NULL, NULL, 'Pankaj Dhiman', '', 'Pankaj Dhiman', '0', NULL, '11,12', NULL, 'pankaj.dhiman@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 06:05:41', '2019-04-18 06:05:41'),
(11, 25, 'BTVr4cUt', 'male', 'Unknown', '', 'Father', 'Father', 10, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 06:05:41', '2019-04-18 06:05:41'),
(12, 25, 'jHhT2CzZ', 'female', 'Unknown', '', 'Mother', 'Mother', 10, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 06:05:41', '2019-04-18 06:05:41'),
(13, 26, 'k82sypHR', NULL, 'gur jeevan', '', 'gur jeevan', '0', NULL, '14,15', NULL, 'shivan.virli@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 06:54:07', '2019-04-18 06:54:07'),
(14, 26, '5IN1gDpJ', 'male', 'Unknown', '', 'Father', 'Father', 13, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 06:54:07', '2019-04-18 06:54:07'),
(15, 26, 'qjX9Jy6p', 'female', 'Unknown', '', 'Mother', 'Mother', 13, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 06:54:07', '2019-04-18 06:54:07'),
(16, 27, 'k82sypHR', NULL, 'Unknown Father', '', 'Unknown Father', '0', NULL, '17,18', NULL, 'shivan.virli@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 06:57:44', '2019-04-18 06:57:44'),
(17, 27, 'O9jzw6hF', 'male', 'Unknown', '', 'Father', 'Father', 16, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 06:57:44', '2019-04-18 06:57:44'),
(18, 27, 'pu7cXJha', 'female', 'Unknown', '', 'Mother', 'Mother', 16, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 06:57:44', '2019-04-18 06:57:44'),
(19, 28, 'k82sypHR', NULL, 'Unknown Father', '', 'Unknown Father', '0', NULL, '20,21', NULL, 'shivan.virli@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 07:03:28', '2019-04-18 07:03:28'),
(20, 28, 'w2Wr51hE', 'male', 'Unknown', '', 'Father', 'Father', 19, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:03:28', '2019-04-18 07:03:28'),
(21, 28, 'FPQC4aUD', 'female', 'Unknown', '', 'Mother', 'Mother', 19, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:03:28', '2019-04-18 07:03:28'),
(22, 29, 'ykUFSwgL', NULL, 'Sahil Gaba', '', 'Sahil Gaba', '0', NULL, '23,24', NULL, 'sahil.gaba@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 07:11:16', '2019-04-18 07:11:16'),
(23, 29, 'cmgYPFbH', 'male', 'Ashok', '', 'Gaba', 'Father', 22, NULL, NULL, 'sahil.t@imarkinfotech.com', '1970-01-01', NULL, 'Living', '2019-04-18 07:11:16', '2019-04-18 07:13:52'),
(24, 29, 'DGk1hWpa', 'female', 'Kiran', '', 'Gaba', 'Mother', 22, NULL, NULL, 'dheryyavier@imarkinfotech.com', '1970-01-01', NULL, 'Living', '2019-04-18 07:11:16', '2019-04-18 07:13:37'),
(25, 29, 'G2TxtEd5', 'male', 'Mohit', '', 'Gaba', 'Brother', 22, '23,24', NULL, 'sahil.gaba@imarkinfotech.com', '1970-01-01', NULL, 'Living', '2019-04-18 07:14:23', '2019-04-18 07:14:23'),
(26, 30, 'iKHVbs95', NULL, 'Dheryyavier Singh', '', 'Dheryyavier Singh', '0', NULL, '27,28', NULL, 'dheryyavier@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 07:22:41', '2019-04-18 07:22:41'),
(27, 30, 'fJrcyGzn', 'male', 'Unknown', '', 'Father', 'Father', 26, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:22:41', '2019-04-18 07:22:41'),
(28, 30, 'fAmveHFU', 'female', 'Unknown', '', 'Mother', 'Mother', 26, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:22:41', '2019-04-18 07:22:41'),
(29, 31, 'LbBTf1O8', NULL, 'Gurjeevan Kaur', '', 'Gurjeevan Kaur', '0', NULL, '30,31', NULL, 'gurjeevan.kaur@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 07:27:06', '2019-04-18 07:27:06'),
(30, 31, 'IlNC5bnq', 'male', 'Unknown', '', 'Father', 'Father', 29, NULL, NULL, 'dfdsf@gmail.com', NULL, NULL, NULL, '2019-04-18 07:27:06', '2019-04-18 12:11:51'),
(31, 31, 'PMzf9gWQ', 'female', 'Unknown', '', 'Mother', 'Mother', 29, NULL, NULL, 'sahil.gabafdsf@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 07:27:06', '2019-04-18 12:08:52'),
(32, 32, 'PMzf9gWQ', NULL, 'Unknown Mother', '', 'Unknown Mother', '0', NULL, '33,34', NULL, 'sahil.gaba@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 07:29:00', '2019-04-18 07:29:00'),
(33, 32, 'RHyVcPfA', 'male', 'Unknown', '', 'Father', 'Father', 32, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:29:00', '2019-04-18 07:29:00'),
(34, 32, 'iItKofAl', 'female', 'Unknown', '', 'Mother', 'Mother', 32, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:29:00', '2019-04-18 07:29:00'),
(35, 33, 'PMzf9gWQ', NULL, 'Unknown Mother', '', 'Unknown Mother', '0', NULL, '36,37', NULL, 'sahil.gaba@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 07:32:40', '2019-04-18 07:32:40'),
(36, 33, '7X3D50li', 'male', 'Unknown', '', 'Father', 'Father', 35, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:32:40', '2019-04-18 07:32:40'),
(37, 33, 'ux8G0PsW', 'female', 'Unknown', '', 'Mother', 'Mother', 35, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 07:32:40', '2019-04-18 07:32:40'),
(38, 35, 'C2OxcJi6', 'female', 'kjkjhk', NULL, 'jkjk', '0', 0, '39,40', NULL, 'khjkhj@gmail.ovm', '2019-04-17', NULL, 'Living', '2019-04-18 10:13:23', '2019-04-18 10:13:23'),
(39, 35, 'BcVjh28q', 'male', 'hjhgjgh', NULL, 'jghjghj', 'Father', 38, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 10:13:23', '2019-04-18 10:13:23'),
(40, 35, 'DFLSJ9Ga', 'female', 'hey', 'jhgh', 'name', 'Mother', 38, NULL, NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 10:13:23', '2019-04-18 10:38:46'),
(41, 36, 'NDe28fv4', 'female', 'tertfdfd', NULL, 'fdfdfdf', '0', 0, '42,43', NULL, 'fdsfdsfds@gmail.com', '2019-04-02', NULL, 'Living', '2019-04-18 10:22:06', '2019-04-18 10:22:06'),
(42, 36, 'NIBKzAiL', 'male', 'fdsfds', NULL, 'fdfdsfds', 'Father', 41, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 10:22:06', '2019-04-18 10:22:06'),
(43, 36, 'tCz6gbXa', 'female', 'my', 'maiden', 'name', 'Mother', 41, '44,45', NULL, NULL, NULL, NULL, NULL, '2019-04-18 10:22:06', '2019-04-18 10:31:40'),
(44, 36, '1KXHrqBv', 'female', 'hye', 'maiden', 'name', 'Mother', 43, NULL, NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 10:31:40', '2019-04-18 10:40:40'),
(45, 36, 'gbfeJBow', 'male', 'My', NULL, 'Father', 'Father', 44, '46,47', NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 10:31:40', '2019-04-18 10:41:47'),
(46, 36, 'GBRLuhc8', 'female', 'fdsfsdfd', 'fdsfdsf', 'dfdfdsfdsfdf', 'Mother', 45, '50,51', NULL, NULL, '2019-03-21', NULL, 'Living', '2019-04-18 10:41:47', '2019-04-18 10:42:21'),
(47, 36, 'ZCQtwRaT', 'male', 'Unknown', NULL, 'Father', 'Father', 46, '48,49', NULL, NULL, NULL, NULL, NULL, '2019-04-18 10:41:47', '2019-04-18 10:42:02'),
(48, 36, 'EcHb8Y9F', NULL, 'fdsfdsf', NULL, 'dsfdsfdsf', 'Father', 47, NULL, NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 10:42:02', '2019-04-18 10:42:02'),
(49, 36, 'VLEm61PH', 'female', 'Unknown', NULL, 'Mother', 'Mother', 48, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 10:42:02', '2019-04-18 10:42:02'),
(50, 36, 'uU7kAwcR', 'male', 'fdsfdsf', NULL, 'dsfdsfdf', 'Father', 46, NULL, NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 10:42:21', '2019-04-18 10:42:21'),
(51, 36, 'nLDlTHFB', 'female', 'Unknown', 'fdsfdsfd', 'Mother', 'Mother', 50, NULL, NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 10:42:21', '2019-04-18 10:42:35'),
(52, 37, 'tvjw8kB4', 'female', 'Test', NULL, 'User', '0', 0, '53,54', NULL, 'gurjeevan@gmail.com', '2019-04-02', NULL, 'Living', '2019-04-18 12:47:15', '2019-04-18 12:47:15'),
(53, 37, 'z4WDngc9', 'male', 'hghf', NULL, 'hgfh', 'Father', 52, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 12:47:15', '2019-04-18 12:47:15'),
(54, 37, 'giMKW2G6', 'female', 'ggfhgfh', 'hgh', 'ghgfh', 'Mother', 52, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 12:47:15', '2019-04-18 12:47:15'),
(55, 38, '8OAHYKWD', NULL, 'Gurjeevan Kaur', NULL, 'Gurjeevan Kaur', '0', NULL, '56,57', NULL, 'gurjeevan.kaur@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 12:47:38', '2019-04-18 12:47:38'),
(56, 38, 'Eyd9QPBl', 'male', 'MY', NULL, 'Father', 'Father', 55, NULL, NULL, 'shivam.virli@imarkinfotech.com', '1970-01-01', NULL, 'Living', '2019-04-18 12:47:38', '2019-04-18 12:49:11'),
(57, 38, 'T3Zxihl4', 'female', 'My', NULL, 'Mother', 'Mother', 55, NULL, NULL, NULL, '1970-01-01', NULL, 'Living', '2019-04-18 12:47:38', '2019-04-18 12:49:22'),
(58, 39, 'Eyd9QPBl', NULL, 'Shivam Virli', NULL, 'Shivam Virli', '0', NULL, '59,60', NULL, 'shivam.virli@imarkinfotech.com', NULL, NULL, NULL, '2019-04-18 12:49:43', '2019-04-18 12:49:43'),
(59, 39, 'HuPUkh1W', 'male', 'Unknown', NULL, 'Father', 'Father', 58, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 12:49:43', '2019-04-18 12:49:43'),
(60, 39, '07q1GsOW', 'female', 'Unknown', NULL, 'Mother', 'Mother', 58, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-18 12:49:43', '2019-04-18 12:49:43'),
(61, 40, 'hi13cpLM', NULL, 'Harwinder Singh', NULL, 'Harwinder Singh', '0', NULL, '62,63', NULL, 'harwinder@imarkinfotech.com', NULL, NULL, NULL, '2019-04-19 04:43:32', '2019-04-19 04:43:32'),
(62, 40, '6d85tVvr', 'male', 'Unknown', NULL, 'Father', 'Father', 61, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-19 04:43:32', '2019-04-19 04:43:32'),
(63, 40, 'J9Z2NKCn', 'female', 'Unknown', NULL, 'Mother', 'Mother', 61, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-19 04:43:32', '2019-04-19 04:43:32'),
(64, 41, 'o2Qjf15X', 'male', 'Shawn', NULL, 'CA', '0', 0, '65,66', NULL, 'adventcorpltd@gmail.com', '1970-01-01', NULL, 'Living', '2019-05-20 03:07:52', '2019-05-20 03:07:52'),
(65, 41, 'DwJTQVHM', 'male', 'Dad', NULL, 'CA', 'Father', 64, NULL, NULL, 's.chongashing@gmail.com', NULL, NULL, NULL, '2019-05-20 03:07:52', '2019-05-20 03:31:52'),
(66, 41, 'qPWf3MIb', 'female', 'Mom', 'Maiden', 'CA', 'Mother', 64, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 03:07:52', '2019-05-20 03:07:52'),
(67, 42, 'DwJTQVHM', 'male', 'Dad', NULL, 'CA', '0', 0, '68,69', NULL, 's.chongashing@gmail.com', '1970-01-01', NULL, 'Living', '2019-05-20 03:33:21', '2019-05-20 03:33:21'),
(68, 42, 'OEWMfwc8', 'male', 'Grampa', NULL, 'CA', 'Father', 67, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 03:33:21', '2019-05-20 03:33:21'),
(69, 42, 'h5W239S4', 'female', 'Grandma', 'Maiden Two', 'CA', 'Mother', 67, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 03:33:21', '2019-05-20 03:33:21'),
(70, 42, 'EsLiomWN', NULL, 'Shawn', NULL, 'CA', 'Son', 67, '67', NULL, 'adventcorpltd@gmail.com', '1970-01-01', NULL, 'Living', '2019-05-20 03:34:01', '2019-05-20 03:34:01'),
(71, 43, 'rK4FRG2H', 'male', 'Gurjeevan', NULL, 'Kaur', '0', 0, '72,73', NULL, 'gurjeevan.kaur@imajkjkrkinfotech.com', '1970-01-01', NULL, 'Living', '2019-05-21 09:52:55', '2019-05-21 09:52:55'),
(72, 43, 'vZwMXpIW', 'male', 'Gurjeevan', NULL, 'Kaur', 'Father', 71, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 09:52:55', '2019-05-21 09:52:55'),
(73, 43, 'kBPbgG1V', 'female', 'Gurjeevan', 'test', 'Kaur', 'Mother', 71, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 09:52:55', '2019-05-21 09:52:55'),
(74, 45, '8P43biz6', 'female', 'Gurjeevan', NULL, 'Kaur', '0', 0, '75,76', NULL, 'gurjeevan.kaur@imafdsfrkinfotech.com', '2019-05-21', NULL, 'Living', '2019-05-21 10:00:34', '2019-05-21 10:00:34'),
(75, 45, 'd8YGaEgV', 'male', 'Gurjeevan', NULL, 'Kaur', 'Father', 74, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 10:00:34', '2019-05-21 10:00:34'),
(76, 45, '7OZBrIfQ', 'female', 'Gurjeevan', 'fdsf', 'Kaur', 'Mother', 74, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 10:00:34', '2019-05-21 10:00:34'),
(77, 46, 'krX2ubv5', 'female', 'Gurjeevan', NULL, 'Kaur', '0', 0, '78,79', NULL, 'gurjeevan.kaur@imarkinfdgfotech.com', '2019-05-21', NULL, 'Living', '2019-05-21 10:04:26', '2019-05-21 10:04:26'),
(78, 46, '2OMKQpAY', 'male', 'Gurjeevan', NULL, 'Kaur', 'Father', 77, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 10:04:26', '2019-05-21 10:04:26'),
(79, 46, 'eN9dQvMV', 'female', 'Gurjeevan', NULL, 'Kaur', 'Mother', 77, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 10:04:26', '2019-05-21 10:04:26'),
(80, 47, 'mcQAv5PX', 'female', 'Gurjeevan', NULL, 'Kaur', '0', 0, '81,82', NULL, 'gurjeevan.kaur@imardsfkinfotech.com', NULL, NULL, 'Living', '2019-05-21 10:06:37', '2019-05-21 10:06:37'),
(81, 47, 'HVagsxIb', 'male', 'Gurjeevan', NULL, 'Kaur', 'Father', 80, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 10:06:37', '2019-05-21 10:06:37'),
(82, 47, '1TEIVlBC', 'female', 'Gurjeevan', '345345', 'Kaur', 'Mother', 80, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 10:06:37', '2019-05-21 10:06:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_relation_parents`
--

CREATE TABLE `user_relation_parents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rel_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_relation_partners`
--

CREATE TABLE `user_relation_partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rel_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_relation_partners`
--

INSERT INTO `user_relation_partners` (`id`, `user_id`, `rel_id`, `created_at`, `updated_at`) VALUES
(1, '19', '20', NULL, NULL),
(2, '20', '19', NULL, NULL),
(3, '39', '18', NULL, NULL),
(4, '18', '39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_subscriptions`
--

CREATE TABLE `user_subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan` text COLLATE utf8mb4_unicode_ci,
  `price` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `choose_plan_options` longtext COLLATE utf8mb4_unicode_ci,
  `added_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_subscriptions`
--

INSERT INTO `user_subscriptions` (`id`, `plan`, `price`, `created_at`, `updated_at`, `choose_plan_options`, `added_by`) VALUES
(1, 'Basic', 0, '2019-03-19 07:23:00', '2019-03-28 01:30:31', 'my-profile,my-tree', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_videos`
--

CREATE TABLE `user_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_videos`
--

INSERT INTO `user_videos` (`id`, `user_id`, `user_video`, `created_at`, `updated_at`) VALUES
(1, 38, '1555591810.mp4', '2019-04-18 12:50:10', '2019-04-18 12:50:10'),
(2, 38, '1555594125_0.mp4', '2019-04-18 13:28:45', '2019-04-18 13:28:45'),
(3, 40, '1555650346_0.mp4', '2019-04-19 05:05:46', '2019-04-19 05:05:46');

-- --------------------------------------------------------

--
-- Table structure for table `yearlooms`
--

CREATE TABLE `yearlooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `yearlooms`
--

INSERT INTO `yearlooms` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Connect with Family', '<p>Your family grows continuously and will spread across the world so stay connected</p>', '2018-11-27 00:41:14', '2018-11-27 00:41:14'),
(2, 'Inspire generations', '<p>Your stories can be the key that will open the doors of their imagination</p>', '2018-11-27 00:42:24', '2018-11-27 00:42:24'),
(3, 'Share life lessons', '<p>Share succeses and failures so that family members can learn from each other</p>', '2018-11-27 00:42:44', '2018-11-27 00:42:44'),
(4, 'Write a memoir', '<p>Your own story told in your own voice</p>', '2018-11-27 00:43:12', '2018-11-27 00:43:12'),
(5, 'Preserve culture and traditions', '<p>Ensure that your culture and family traditions are not lost to time or distance</p>', '2018-11-27 00:43:32', '2018-11-27 00:43:32'),
(6, 'Tell it like it is', '<p>The stories of your life straight from the source.</p>', '2018-11-27 00:43:49', '2018-11-27 00:43:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us_core_features`
--
ALTER TABLE `about_us_core_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_posts_slug_unique` (`slug`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_comments`
--
ALTER TABLE `forum_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_conversations`
--
ALTER TABLE `group_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_members`
--
ALTER TABLE `group_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invite_members`
--
ALTER TABLE `invite_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `page_blocks`
--
ALTER TABLE `page_blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_blocks_page_id_index` (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `story_comments`
--
ALTER TABLE `story_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `story_likes`
--
ALTER TABLE `story_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `story_members`
--
ALTER TABLE `story_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_conversations`
--
ALTER TABLE `user_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_conversation_messages`
--
ALTER TABLE `user_conversation_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_forums`
--
ALTER TABLE `user_forums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_invite_family`
--
ALTER TABLE `user_invite_family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payment_details`
--
ALTER TABLE `user_payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_photos`
--
ALTER TABLE `user_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_questions`
--
ALTER TABLE `user_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_relations`
--
ALTER TABLE `user_relations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_relation_parents`
--
ALTER TABLE `user_relation_parents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_relation_partners`
--
ALTER TABLE `user_relation_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_videos`
--
ALTER TABLE `user_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yearlooms`
--
ALTER TABLE `yearlooms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us_core_features`
--
ALTER TABLE `about_us_core_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `forum_categories`
--
ALTER TABLE `forum_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `forum_comments`
--
ALTER TABLE `forum_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `group_conversations`
--
ALTER TABLE `group_conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `group_members`
--
ALTER TABLE `group_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invite_members`
--
ALTER TABLE `invite_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `page_blocks`
--
ALTER TABLE `page_blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `story_comments`
--
ALTER TABLE `story_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `story_likes`
--
ALTER TABLE `story_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `story_members`
--
ALTER TABLE `story_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `user_conversations`
--
ALTER TABLE `user_conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_conversation_messages`
--
ALTER TABLE `user_conversation_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_forums`
--
ALTER TABLE `user_forums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_invite_family`
--
ALTER TABLE `user_invite_family`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_payment_details`
--
ALTER TABLE `user_payment_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_photos`
--
ALTER TABLE `user_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_questions`
--
ALTER TABLE `user_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_relations`
--
ALTER TABLE `user_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `user_relation_parents`
--
ALTER TABLE `user_relation_parents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_relation_partners`
--
ALTER TABLE `user_relation_partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_videos`
--
ALTER TABLE `user_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `yearlooms`
--
ALTER TABLE `yearlooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
