<!-- story_sec end -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 ftr_main">
					<p>Copyright &copy; <?php echo date('Y'); ?> Yearloom. Powered By - <a href="http://imarkinfotech.com" target="_blank">iMark Infotech</a></p>
					{!! menu('Footer Menu','footer_menu') !!} 
					<div class="ftr_social">
						<ul>
							<li><a href="{{setting('site.facebook_link')}}" title="" target="_blank"><i class="fa fa-facebook" aria-hidden="true" target="_blank"></i></a></li>
							<li><a href="{{setting('site.twitter_link')}}" title="" target="_blank"><i class="fa fa-twitter" aria-hidden="true" target="_blank"></i></a></li>
							<li><a href="{{setting('site.linkdin_link')}}" title="" target="_blank"><i class="fa fa-linkedin" aria-hidden="true" target="_blank"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div> 

<!----- Subscribe Modal ------->

  	<div class="modal fade" id="subscribe_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h2 class="modal-title" id="exampleModalLabel1">Subscribe Family network</h2>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
						<span aria-hidden="true">&times;</span> 
					</button>

				</div>

				<div class="modal-body">

					<form method="post" action="{{url('/paypal-subscription')}}">
						@csrf
						<p>
							<label>Plan</label>
							<input type="text" name="plan" class="form-control user_plan" readonly>
						</p>
						<p>
							<label>Plan Type</label>
							<input type="text" name="plan_type" class="form-control user_plan_type" readonly>
						</p>
						<p>
							<label>Plan Price</label>
							<input type="text" name="plan_price" class="form-control user_plan_price" readonly>
						</p>
						<input type="submit" value="Subscribe Now" class="post-btn">
					</form>

				</div>
			</div>
		</div>
	</div>

<!----- Forgot Password Modal ------->

  	<div class="modal fade " id="forgot_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h2 class="modal-title" id="exampleModalLabel1">Forgot Password</h2>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
						<span aria-hidden="true">&times;</span> 
					</button>

				</div>

				<div class="modal-body">

					<form id="user_forgot_password" method="post" action="{{url('/forgot-password')}}">
						@csrf
						<div class="form-group">
							<!--- <label>Email</label> --->
							<input type="text" name="email" placeholder="Email" class="form_control user_plan">
						</div>
						
						<div class="form-group button-outr login_sbmt eff-5">
						   <div class="login_sbmt eff-5">
							    <input type="submit" value="Submit" class="login post-btn">
					      </div>
						</div>
						
						
					</form>

				</div>
			</div>
		</div>
	</div>

<!----- Reset Password Modal ------->

  	<div class="modal fade" id="user_reset_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					

					<h2 class="modal-title" id="exampleModalLabel1">Reset Password</h2>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
						<span aria-hidden="true">&times;</span> 
					</button>

				</div> 
 
				<div class="modal-body">

					<form id="user_reset_password_form" method="post" action="{{url('/reset-password')}}">
						@csrf
						<input type="hidden" name="userId" class="forgot_user_id">
						<p>
							<label>New Password</label>
							<input type="password" name="new_password" id="new_password" placeholder="New Password" class="form-control ">
						</p>
						<p>
							<label>Confirm Password</label>
							<input type="password" name="confirm_password" placeholder="Confirm Password" class="form-control">
						</p>
						<input type="submit" value="Submit" class="post-btn">
					</form>
 
				</div>
			</div>
		</div>
	</div>
	
	
	
<!---	login popup  -->

<div class="modal fade" id="login_pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					

					<h2 class="modal-title" id="exampleModalLabel1">Login</h2>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
						<span aria-hidden="true">&times;</span> 
					</button>

				</div> 
 
				<div class="modal-body">
					<div class="login_form">
						@if (!Auth::check())
						<form id="user_login" method="POST" action="{{ route('login') }}">
						@csrf
						
						<div class="row">
						  <div class="col-md-12">
						     <div class="form-group">
							    	<input id="email" type="email" class="form_control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required>
							@if ($errors->has('email'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
							 </div>
							 
							  <div class="form-group">
							    <input id="password" placeholder="Password" type="password" class="form_control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
								<a href="javascript:void();" class="forgot_password">Forgot Password</a>
							@if ($errors->has('password'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
							 </div>
						  </div>
						 
							 <div class="col-md-12 button-utr flex-dir-col">
							 <div class="login_sbmt eff-5">
							    <input type="submit" value="login" class="login">
						     </div>
							 </div>
						</div>
						
						  
						</form>
						
						@else
						<div class="user_log_in">
							<p>Welcome, <span>{{ Auth::user()->name }}</span></p>
							<a href="{{url('/logout')}}" class="open_source_btn">Logout</a> 
						</div> 
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	</footer>

	@if(!Auth::user())
	
		<div class="gmail-links">
		   <a href="{{url('/auth/facebook')}}" class="fixed-btn facebook-btn back" title="" target="_blank">
		   <span> <i class="fa fa-facebook" aria-hidden="true"></i></span>Login with Facebook</a>
		   <a href="{{url('/auth/google')}}" class=" fixed-btn gmail-btn back" title="" target="_blank"><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>Login with Gmail</a>
		</div>
		
	@endif
	
	
	<!-- footer end -->
	@include('includes.modal')
	<!-- jQuery (necessary for JavaScript plugins) -->
	
	<script type="text/javascript" src="{{ URL::asset('js/jquery3.3.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/popper.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/wow.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/my-custom.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/dashboard-custom.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/toastr.js') }}"></script>
	 
	
</body>

</html>