<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ setting('site.title') }} - @yield('page_title')</title>
	<link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/x-icon">
	<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/jquery-ui.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/animate.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/toastr.css') }}" rel="stylesheet" type="text/css">
		
</head>
<body>
<div id="preloader"> 
	<div class="wrapper">
		<div class="cssload-loader"></div>
	</div>
</div>
<header>  

<nav class="navbar navbar-expand-lg">
	<div class="container">
			<a class="navbar-brand" href="{{ url('/') }}"><img src="{{ imageUrl(setting('site.logo'), 256, null, ['crop' => false]) }}" alt="logo"></a>
			
			<div class="nav-right">
			<div class="nav-right-top">
				 <!-- <div class="account-wrap">
					<div class="account-item clearfix js-item-menu">
						<div class="content">
							<a class="js-acc-btn" href="#"></a>
						</div>
						<div class="account-dropdown js-dropdown">

							<div class="account-dropdown__body">
								<div class="account-dropdown__item">
									<a href="{{url('/auth/facebook')}}" class="facebook-btn" title="" target="_blank><span><i class="fa fa-facebook" aria-hidden="true" target="_blank"></i></span>Login with Facebook</a>
								</div>
								<div class="account-dropdown__item">
									<a href="{{url('/auth/google')}}" class=" gmail-btn" title="" target="_blank><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>Login with Gmail</a>
								</div>

							</div>
						</div>
					</div> -->
				</div>
				</div>
			
			<div class="nav-right-collapse">
			    <div class="collapse navbar-collapse ">
					<ul class="navbar-nav">
						<li class="nav-item {{ (Request::path()=='photos' ? 'active' : '') }}">
							<a class="nav-link" href="{{url('/how-it-works')}}" target="_self" style="">
								<span>How it works</span>
							</a>
								</li>
								
						<li class="nav-item {{ (Request::path()=='about-us' ? 'active' : '') }}">
							<a class="nav-link" href="{{url('/about-us')}}" target="_self" style="">
								<span>About Us</span>
							</a>
								</li>
						
						<li class="nav-item {{ (Request::path()=='faq' ? 'active' : '') }}">
							<a class="nav-link" href="{{url('/faq')}}" target="_self" style="">
								<span>Faq</span>
							</a>
								</li>

						<li class="nav-item {{ (Request::path()=='pricing' ? 'active' : '') }}">
							<a class="nav-link" href="{{url('/pricing')}}" target="_self" style="">
								<span>Pricing</span>
							</a>
						</li>
						
						
					</ul>
					
					<!-- @include('includes.loginform') -->
				</div>
				
				@if(!Auth::user())
				
					<div class="login-right">
					   <ul>
						 <li class="login_sbmt eff-5"><a href="javascript:void(0);" id="open_login_popup" class="login"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
					   </ul>
					</div>

				@endif

				@if(Auth::user())
					
					<div class="account-wrap">
						<div class="account-item clearfix js-item-menu">
							<div class="image"> 
								<a href="{{url('/dashboard')}}"> 
									<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="usr_img">
								</a> 
								
								<a href="javacript:void();" class="notify">{{Helper::getUserNotification()}}</a>
							</div>
							<div class="content">
								<a class="js-acc-btn" href="#"></a>
							</div>
							<div class="account-dropdown js-dropdown">
								<div class="info clearfix">
									<div class="image">
										<a href="{{url('/dashboard')}}"> 
											<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="usr_img"> 
										</a>
										</div>
									<div class="content">
										<h5 class="name">
											<a href="javascript:void(0)">{{Auth::user()->name}}</a>
										</h5>
										<span class="email">{{Auth::user()->email}}</span> 
									</div>
								</div> 
								<div class="account-dropdown__body">
									<div class="account-dropdown__item">
										<a href="{{url('user-notification')}}"> 
											
											<i class="fa fa-bell" aria-hidden="true"></i><span>{{Helper::getUserNotification()}}</span>Notification
										</a>
									</div>
									<div class="account-dropdown__item">
										<a href="{{url('change-password')}}"> 
											<i class="fa fa-cog" aria-hidden="true"></i>Setting
										</a>
									</div>
								</div>
								<div class="account-dropdown__footer">
									<a href="{{url('logout')}}"> 
										<i class="fa fa-power-off" aria-hidden="true"></i>Logout
									</a>
								</div>
							</div>
						</div> 
					</div>
				@endif
				<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> 
			<svg viewBox="2 6 28 20" enable-background="new 2 6 28 20" xml:space="preserve">
				<path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2
				c0,1.104,0.896,2,2,2h24c1.104,0,2-0.896,2-2C30,14.896,29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24
				c1.104,0,2-0.896,2-2S29.104,22,28,22z"></path>
		   </svg>
		</button>
			</div>
		</div>
	</div>
</nav>
</header>