<div class="login_form">
    @if (!Auth::check())
	<form id="user_login" method="POST" action="{{ route('login') }}">
	@csrf
	
      <p>
        <input id="email" type="email" class="form_control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required>
		@if ($errors->has('email'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('email') }}</strong>
			</span>
		@endif
       
		<input id="password" placeholder="Password" type="password" class="form_control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
		@if ($errors->has('password'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('password') }}</strong>
			</span>
		@endif
      </p> 
	  
      <div class="login_sbmt eff-5">
         <input type="submit" value="login" class="login">
      </div>
       <div class="login_sbmt eff-5">
         <input type="button" value="Forgot Password" class="login forgot_password">
      </div>
    </form>
	
	@else
	<div class="user_log_in">
		<p>Welcome, <span>{{ Auth::user()->name }}</span></p>
		<a href="{{url('/logout')}}" class="open_source_btn">Logout</a> 
	</div> 
	@endif
</div>
