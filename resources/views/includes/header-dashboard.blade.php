<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ setting('site.title') }} - @yield('page_title')</title>
	<link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/x-icon">
	
	<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/jquery-ui.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/primitives.latest.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/animate.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/dashboard.css') }}" rel="stylesheet"> 
	<link href="{{ URL::asset('css/toastr.css') }}" rel="stylesheet"> 
	<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" rel="stylesheet"> 
	@yield('custom_css')
</head>
	
	
<body class="dashboard-home"> 
<div id="preloader">
	<div class="wrapper">
		<div class="cssload-loader"></div>
	</div>
</div>
<div id="main">
	<header>
		<div class="container-fluid">
			<div class="head-wrap">
				<div class="head-wrap-lft">
					<a href="{{url('/')}}"><img src="{{ URL::asset('images/dashboard-logo.png') }}" alt="dashboard-logo"></a>
				</div> 
				@include('includes.dashboard-top-menu')  
			</div>
			<!-- head-wrap end -->
		</div>
	</header>
	<div class="dashboard_content" id="main1">
			@include('includes.sidebar-dashboard')
		