 
<div class="head-wrap-rght">
	<form action="{{url('/my-story')}}" method="GET">
		<div class="form-group-main">
			<input type="text" class="form_control" name="keyword" placeholder="Search family members or stories">
			<button type="submit">
				<span class="fa fa-search form-control-feedback"></span> 
			</button>
		</div>
	</form>
	<div class="head-button"> 
		<a href="javascript:void(0)" title="" id="invite-family" class="usr-svg btn-white">Invite Family
			<svg>
				<use xlink:href="#add_icn"></use>
			</svg>
		</a> 
		<a href="{{url('/create-story')}}" title="" class="usr-edit usr-svg btn-blue">New Story
			<svg>
				<use xlink:href="#edt_icn"></use>
			</svg>
		</a> 
	</div>
	<div class="account-wrap">
		<div class="account-item clearfix js-item-menu">
			<div class="image"> 
				<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="usr_img"> 
				<a href="javacript:void();" class="notify">0</a>
			</div>
			<div class="content">
				<a class="js-acc-btn" href="#"></a>
			</div>
			<div class="account-dropdown js-dropdown">
				<div class="info clearfix">
					<div class="image">
						<a href="#"> 
							<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="usr_img"> 
						</a>
					</div>
					<div class="content">
						<h5 class="name">
							<a href="#">{{Auth::user()->name}}</a>
						</h5>
						<span class="email">{{Auth::user()->email}}</span> 
					</div>
				</div> 
				<div class="account-dropdown__body">
					<div class="account-dropdown__item">
						<a href="{{url('user-notification')}}"> 
							
							<i class="fa fa-bell" aria-hidden="true"></i><span>{{Helper::getUserNotification()}}</span>Notification
						</a>
					</div>
					<div class="account-dropdown__item">
						<a href="{{url('/change-password')}}"> 
							<i class="fa fa-cog" aria-hidden="true"></i>Setting
						</a>
					</div>
				</div>
				<div class="account-dropdown__footer">
					<a href="{{url('/logout')}}"> 
						<i class="fa fa-power-off" aria-hidden="true"></i>Logout
					</a>
				</div>
			</div>
		</div> 
	</div>
</div>