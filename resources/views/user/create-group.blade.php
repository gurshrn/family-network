@extends('layouts.dashboard')
@section('page_title')
Create Group
@endsection
@section('content')
	<div class="sidebar-rght">

		<div class="dashboard-wrapper tp_89 pd_none">

			<div class="group-internal">

				<div class="create-post">

					<h2>Create Group</h2>

					<form id="create-group" method="post" class="post-create" action="{{url('/create-group')}}" enctype="multipart/form-data">

						@csrf

						<p>
							<input type="text" placeholder="Name" name="group_name" class="cptl form_control" maxlength="50">
						</p>

						<p>
							<select class="cptl chosen form_control" name="member_list[]" data-placeholder="Tag Members..." multiple>

								@if(isset($userRel))
								@foreach($userRel as $val)

									<option value="{{$val[0]['id']}}">{{$val[0]['name']}}</option>
								@endforeach
								@endif
							</select>
						</p>

						<div class="input-group form_control"> 
							<span class="input-group-btn">
								<div class="btn1 custom-file-uploader">

				                	<input type="file" name="profile_image" onchange="this.form.filename.value = this.files.length ? this.files[0].name : ''" />

				                		Upload Image

				              	</div>
							</span>

							<input type="text" name="filename" class="txtsctn" readonly>
							<img src="images/attach.svg" alt="attach" class="attch-file"> 
						</div>

						<p>

							<textarea class="form_control" name="description" placeholder="Description"></textarea>

						</p>
						<p>
							<input type="submit" value="Create" class="post-btn add-submit-btn">
						</p>

					</form>
				</div>

			</div>

		</div>

	</div>
@endsection
