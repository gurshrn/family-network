@extends('layouts.dashboard')
@section('page_title')
Chat
@endsection
@section('content')


	<div class="sidebar-rght story-page">
		<div class="dashboard-wrapper dashboard-full">
			<div class="chat-page">
				<div class="messaging">
					<div class="inbox_msg">
						<div class="inbox_people">
							<div class="heading_srch">
								<div class="srch_bar">
									<div class="stylish-input-group">
										<input type="text" class="search-bar search-member" placeholder="Search Members"> 
										<span class="input-group-addon">
											<button type="button"> 
												<i class="fa fa-search" aria-hidden="true"></i> 
											</button>
										</span> 
									</div>
								</div>
							</div>

							<div class="inbox_chat chat_members">
								@if(count($memberList->getmembers) > 0)
									@foreach($memberList->getmembers as $val)
										
									 	<div class="chat_list active_chat">
											<div class="chat_people">
												<a href="javscript:void(0)" data-attr="{{$val->user['id']}}" title="" id="get_conversation_detail">
													<div class="chat_img"> 
														<img src="{{url('upload/profileimages/'.$val->user['avatar'])}}" alt="john"> 
													</div>
													<div class="chat_ib">
														<h5>{{$val->user['name']}}</h5> 
													</div>
												</a>
											</div>
										</div>

									@endforeach
								@else 
									<div class="no-found">
									<div class="alert alert-danger">No Member Found</div>
									</div>
								@endif 
								@if(count($invitedUser)>0)
									@foreach($invitedUser as $invUser)
										@if(!empty($invUser[0]))
											<div class="chat_list active_chat">
												<div class="chat_people">
													<a href="javscript:void(0)" data-attr="{{$invUser[0]['id']}}" title="" id="get_conversation_detail">
														<div class="chat_img"> 
															<img src="{{url('upload/profileimages/'.$invUser[0]['avatar'])}}" alt="john"> 
														</div>
														<div class="chat_ib">
															<h5>{{$invUser[0]['name']}}</h5> 
														</div>
													</a>
												</div>
											</div>
										@endif

									@endforeach
								@endif

								@if(count($memberList->parentUser) > 0)
									<div class="chat_list active_chat">
										<div class="chat_people">
											<a href="javscript:void(0)" data-attr="{{$memberList->parentUser->id}}" title="" id="get_conversation_detail">
												<div class="chat_img"> 
													<img src="{{url('upload/profileimages/'.$memberList->parentUser->avatar)}}" alt="john"> 
												</div>
												<div class="chat_ib">
													<h5>{{$memberList->parentUser->name}}</h5> 
												</div>
											</a>
										</div>
									</div>
								@endif
							</div>
						</div>

						

						<div class="mesgs-main">


							<div class="mesgs">

								@if(count($getConversation) > 0)

								<div class="usr-name">

									@if($getConversation->from ==  Auth::id())
											
										@php $userId = $getConversation->to @endphp
										
									@else
										
										@php  $userId = $getConversation->from @endphp
									
									@endif

									@php $getUserDetail = Helper::getUserDetail($userId);  @endphp

								 	<h3>{{$getUserDetail->name}}</h3>

								</div>

								<div class="msg_history">

									
									@foreach($getConversation->conversationmsg as $msg)



										@if($msg->sender_id == Auth::id())

											<div class="outgoing_msg">
												<div class="sent_msg">
													<p>{{$msg->message}}</p>
												</div>	
												<div class="outgoing_msg_img"> 
													<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="john"> 
												</div>
											</div>
										
										@else

											@if($msg->sender_id ==  $getConversation->from)
											
												@php $userId = $getConversation->from @endphp
												
											@else
												
												@php  $userId = $getConversation->to @endphp
											
											@endif

											@php $getUserDetail = Helper::getUserDetail($userId);  @endphp
										
											<div class="incoming_msg">

												<div class="incoming_msg_img"> 
													<img src="{{url('/upload/profileimages/'.$getUserDetail->avatar)}}" alt="vicky"> 
												</div>
												<div class="received_msg">
													<div class="received_withd_msg">
														<p>{{$msg->message}}</p>
													</div>
												</div>
											</div>
										@endif

									@endforeach
									

									<form id="user_conversation" action="{{url('/chat')}}" method="post">

										<div class="type_msg">

											@csrf

											<input type="hidden" class="form_control" name="reciever_id" value="{{$getUserDetail->id}}">
											<input type="hidden" class="form_control" id="chat_id" name="chat_id" value="{{$getConversation->id}}">
											<input type="text" class="form_control" name="message" placeholder="Reply" />
											<input type="submit" value="Send" class="post-btn">
										</div>

									</form>
								</div>
								@else 
									<div class="alert alert-danger">No Chat Found</div>
								@endif 
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
@endsection

