@extends('layouts.dashboard')
@section('page_title')
Group Chat
@endsection
@section('content')

	<div class="sidebar-rght story-page">

		<div class="dashboard-wrapper dashboard-full">

			<div class="chat-page group-chat">

				<div class="messaging">

					<div class="inbox_msg">

						<div class="inbox_people">

							<div class="heading_srch">

								<div class="srch_bar">

									<div class="stylish-input-group">

										<input type="text" class="search-bar" placeholder="Search Members"> <span class="input-group-addon">

   			 								<button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>

                						</span> </div>

										</div>

									</div>

									<div class="inbox_chat">

										@if(count($userGroup) > 0)
											@foreach($userGroup as $val)

												<div class="chat_list active_chat">

													<div class="chat_people">

														<a href="javascript:void(0)" data-attr="{{$val->id}}" id="group_chat" title="">

															<div class="chat_img"> <img src="{{url('upload/groupphotos/'.$val->profile_image)}}" alt="john"> </div>

															<div class="chat_ib">

																<h5>{{$val->group_name}}</h5> </div>

														</a>

														<a href="#" title="" class="close-group"><img src="images/cross-img.svg" alt="cross-img"></a>

													</div>

												</div>

											@endforeach
										@else 
											<div class="no-found">
									<div class="alert alert-danger">No Member Found</div>
									</div>
										@endif 
									</div>

								</div>

								<div class="mesgs-main">
                                  <a href="#" title="" class="usr-svg btn-white">Create Group</a>
									<div class="mesgs">

									@if(count($userGroup) > 0)

										@if(count($groupConversation->groupconversation) > 0)

											<div class="usr-name">

												<figure>
													<img src="images/chat-group.png" alt="chat-group">
												</figure>
												<h3>{{$groupConversation->group_name}}</h3> 
											</div>

											<div class="msg_history">

												@foreach($groupConversation->groupconversation as $msg)

													@if($msg->user_id == Auth::id())

														<div class="outgoing_msg">

															<div class="sent_msg">

																<p>{{$msg->message}}</p>

															</div>

															<div class="outgoing_msg_img"> 
																<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="john"> 

															</div>
															<label>{{Auth::user()->name}}</label>

														</div>
													
													@else

														<div class="incoming_msg">

															<div class="incoming_msg_img"> 
																<img src="{{url('/upload/profileimages/'.$msg->userdetail->avatar)}}" alt="vicky">
																 
															</div>
															<label>{{$msg->userdetail->name}}</label>

															<div class="received_msg">

																<div class="received_withd_msg">

																	<p>{{$msg->message}}</p>

																</div>

															</div>
														</div>

													@endif
												@endforeach
											</div>

											<form id="group_conversation" action="{{url('/group-chat')}}" method="post">

												<div class="type_msg">

													@csrf

													<input type="hidden" class="form_control" id="group_id" name="group_id" value="{{$groupConversation->id}}">
													<input type="text" class="form_control" name="message" placeholder="Reply" />
													<input type="submit" value="Send" class="post-btn">
												</div>

											</form>

									@else 
										<div class="alert alert-danger">No Chat Found</div>
									@endif 
									@else 
										<div class="alert alert-danger">No Chat Found</div>
									@endif 
									

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection