@extends('layouts.dashboard')
@section('page_title')
Bio
@endsection
@section('custom_js')
<script type="text/javascript" src="{{ URL::asset('tinymce/js/tinymce/tinymce.min.js') }}" ></script>
<script>
jQuery(document).ready(function()
{
	tinymce.init({
		selector: ".editor",
		plugins: "link image",
		inline_styles: false,
		
	});
});
</script>
@endsection
@section('content')
	<div class="sidebar-rght story-page">
		@include('user.user_banner_section') 
		<div class="dashboard-wrapper">
			<div class="bio-page">
				<div class="bio-main">
					<div class="bio-head">
						<h1>This Is How I Want Stories To Look</h1>
						<figure>
							<a href="javascript:void();" title="" class="edit-pro">
								<svg><use xlink:href="#edit_img"></use></svg>
							</a>
						</figure>
					</div>

					<p>
						<strong>Dob:</strong> {{date('d M Y',strtotime($userBio->dob))}}
					</p>
					
					<p>{!! $userBio->bio_short_description !!}</p>
					<p>{!! $userBio->bio_description !!}</p>

				</div>

				<form class="post-create" name="my_account" id="my_account" action="" method="post" enctype="multipart/form-data">

					@csrf

				 	<h2>Bio</h2>

						<div class="med-sctn">

							<p>
								<label>Nick Name</label>
								<input type="text" name="name" class="cptl form_control" value="{{$userBio->name}}" placeholder="Nick Name" maxlength="50">
							</p>

							<p>
								<label>Email</label>
								<input type="text" name="email" class="form_control" value="{{$userBio->email}}" placeholder="Email" readonly>
							</p>

							<p>
								<label>DOB</label>
								<input type="text" name="dob" class="form_control" id="bio_datepicker" value="{{date('d M Y',strtotime($userBio->dob))}}" placeholder="DOB" readonly>
							</p>
							<p>
								<label>Title</label>
								<input type="text" name="bio_title" class="cptl form_control" value="{{$userBio->bio_title}}" placeholder="Title" maxlength="50">
							</p>

						</div>

							

							<p>
								<label>Description</label>
								<textarea class="form_control" name="bio_short_description" placeholder="Short Description">{{$userBio->bio_short_description}}</textarea>
							</p>

							<p>
								<p>
									<textarea id="summernote" class="form_control editor" name="bio_description" placeholder="Story" >{{$userBio->bio_description}}</textarea>
								</p>
							</p>

							<p>
								<input type="submit" value="Save" class="post-btn">
							</p>
						</form>
					</div>
				</div>
			</div>
@endsection

	