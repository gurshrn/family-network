@extends('layouts.dashboard')
@section('page_title')
My Story 
@endsection
@section('content')

<div class="sidebar-rght story-page">

	@include('user.user_banner_section') 

	<div class="dashboard-wrapper">
	
		<div class="post-main">
			
			
			<div class="post-main-bx my_story">
				<div class="post-info-sctn">
					<div class="post-info-sctn-lft">
						<figure>
							<img src="{{url('upload/profileimages/'.$userStory->userdetail['avatar'])}}" alt="user_img">
						</figure>
						<h2>{{$userStory->userdetail['name']}}</h2> 
					</div>
					<div class="post-info-sctn-rght">
						<div class="date-sctn"> 
							<span>
								<time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($userStory->created_at)); ?>T<?php echo date('H:i:s', strtotime($userStory->created_at)); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($userStory->created_at)); ?></time>
							</span> 
						</div> 
					</div>
				</div>
				<h1>
					<a href="#" title="">{{ $userStory->title }}</a>
				</h1>
				
				<div class="post-info-cntnt">
					
					{!! $userStory->description !!}
					
					@if($userStory->userphotos['user_photo'] != '')
						
						<figure>
							<img src="{{ URL::asset('upload/userphotos/'.$userStory->userphotos['user_photo']) }}" alt="timeline-full">
						</figure>
					
					@endif
					@if($userStory->uservideos['user_video'] != '')
						<video width="320" height="240" autoplay>
						  	<source src="{{ URL::asset('upload/uservideos/'.$userStory->uservideos['user_video']) }}" >
						</video>
						
					@endif

					<ul class="post-info-cmnt"> 
						<li>
							<a href="javascript:void(0)" id="like-story" data-attr="{{$storyId}}">
								<i class="fa {{ count($userStoryLike) == 0 ? 'fa-thumbs-o-up' : 'fa-thumbs-up'}}" id="likes-{{$storyId}}" aria-hidden="true"></i>
								<span class="count-like-{{$storyId}}">
									{{count($userStory->storylikes)}}
									@if(count($userStory->storylikes) == 1)
										{{'Like'}}
									@else
										{{'Likes'}}
									@endif
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)">
								<i class="fa fa-commenting-o" aria-hidden="true"></i>
								<span class="coments">
									{{count($storyComment)}} 
									@if(count($storyComment) == 1)
										{{'Comment'}}
									@else
										{{'Comments'}}
									@endif
								</span>
						</div>
							</a>
						</li>
					</ul>

					<div class="forum-comment">

						<h2>Comments</h2>

						<form id="story_comment" action="{{url('add-story-comment')}}" method="post">

							@csrf

							<input type="hidden" name="story_id" value="{{$storyId}}">

							<p class="forum-full">
								<input type="text" placeholder="Write Your Comment" name="story_comment" class="form_control">
							</p>

							<p>
								<input type="submit" value="Submit" class="post-btn">
							</p>

						</form>
					</div>
					
				</div>
			</div>
			<div class="forum-comment-sctn">

					

					<div class="forum-comment-main story-comment-data">

						@if(count($storyComment) > 0)
							@foreach($storyComment as $comm)

								<div class="forum-comment-bx">

									<figure><img src="{{url('/upload/profileimages/'.$comm->user['avatar'])}}" alt="user-sml-img1"></figure>

									<div class="forum-txt">

										<h6>{{$comm->user['name']}}
											<small>
												<time class="timeago"><?php echo $comm['created_at']->diffForHumans(); ?>
												</time>
											</small>
										</h6>

										<p>{{ucfirst($comm['comment'])}}</p>

									</div>
								</div>

							@endforeach
						@else
							<div class="forum-comment-bx no-comment">No comment available</div>
						@endif

					</div>
				</div>	
		</div>
	</div>
</div>
@endsection