<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CS_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('admin');
        $this->load->model('home');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	/*=================Get all user detail=================*/
	
	public function index()	
	{
		$this->load->view('backend/admin-login.php');			
	}
	
	public function loginAdmin()
	{
		
	
		$results = $this->admin->login($_POST);
		if (count($results) > 0) 
		{
			$this->session->set_userdata('admin-userRefId', $results[0]->userRefId);
            $this->session->set_userdata('admin-firstName', $results[0]->first_name);
            $this->session->set_userdata('logged_in_back', 'backend');
            $result['success'] = true;
            $result['success_message'] = 'Login Successfully';
            $result['url'] = site_url('admin/dashboard');
       	}
		else
		{
			$result['success'] = false;
            $result['error_message'] = 'Username or Password is wrong,Please check';
		
		}
		echo json_encode($result);exit;
	}
	
	public function logout()
	{
		$userloggedin = $this->session->userdata('logged_in_back');
		
		if(isset($userloggedin) && !empty($userloggedin)) 
		{
			$this->session->unset_userdata('admin-userRefId');
			$this->session->unset_userdata('admin-firstName');
			$this->session->unset_userdata('logged_in_back');
			$this->session->sess_destroy();
		}
        redirect(site_url('admin'));
	}
	
	public function users()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'User';		
		$output['userdetail'] = $this->admin->getAllUsers();		
		$this->load->view('backend/admin-index.php',$output);	
	}
	
	public function dashboard()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Dashboard';		
		$output['offerPurchased'] = $this->admin->countOfferPurchased();	
		$output['offerSold'] = $this->admin->countOfferSold();	
		$output['recentSold'] = $this->admin->countRecentOfferSold();	
		$output['earnedCommission'] = $this->admin->totalEarnedCommission();
		$this->load->view('backend/dashboard.php',$output);
	}

	/*=================get all offers detail=================*/
		
	public function getAllOffers()	
	{	
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Offer';		
		$output['offers'] = $this->admin->getOffers();		
		$this->load->view('backend/admin-offers.php',$output);	
	}
	
	/*=================Update user status=================*/
	
	public function updateStatus()	
	{		
		$output = $this->admin->statusUpdate($_POST);		
		echo json_encode($output);exit;	
	}
	
	/*=================get offers detail for view offers=================*/
	
	public function getOfferDetailById()
	{
		$output['offers'] = $this->admin->offerDetalById($_POST['id']);
		$this->load->view('modal/view-offers.php',$output);
	}
	
	/*=================get offers detail for edit offers=================*/
	
	public function editOfferDetail($id)
	{
		$output['parenturl'] = 'Offer';		
		$output['offers'] = $this->admin->offerDetalById($id);
		$output['creditNetwork'] = $this->home->getCreditNetwork();		
		$this->load->view('backend/admin-edit-offer.php',$output);
	}
	
	/*===============Offer search================*/
	
	public function getOfferDetailByStatus()
	{
		$output['parenturl'] = 'Offer';		
		$output['offers'] = $this->admin->getOfferByStatus($_POST);
		$this->load->view('backend/edit-offer-pagination.php',$output);
	}
	
	/*===================get detail of credit network===================*/
	
	public function getCreditNetwork()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Credit network';		
		$output['credits'] = $this->admin->getCreditsNetwork();
		$this->load->view('backend/credit-network.php',$output);
	}
	
	/*===================Add credit network===================*/
	
	public function addCreditNetwork()
	{
		$tablename ='credit_networks';
	
		$results = $this->admin->commonInsert($_POST,$tablename);
		if($results['success'])
		{
			$result['success'] = $results['success'];
			$result['url'] = site_url('admin/credits-network');
			$result['success_message'] = $results['success_message'];
        }
		else
		{
			$result['success'] = $results['success'];
			$result['error_message'] = $results['error_message'];
        }
		echo json_encode($result);exit;
	}
	
	/*===================Add network operator===================*/
	
	public function addNetworkOperator()
	{
		$tablename ='network_operator';
	
		$results = $this->admin->commonInsert($_POST,$tablename);
		if($results['success'])
		{
			$result['success'] = $results['success'];
			$result['url'] = site_url('admin/network-operator');
			$result['success_message'] = $results['success_message'];
        }
		else
		{
			$result['success'] = $results['success'];
			$result['error_message'] = $results['error_message'];
        }
		echo json_encode($result);exit;
	}
	public function commonEdit()
	{
		$result = $this->admin->getDetailById($_POST['editId'],$_POST['tablename']);
		echo json_encode($result);exit;
	}
	public function getNetworkOpertaor()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Network operator';		
		$output['network'] = $this->admin->getAllNetworkOperator();
		$this->load->view('backend/network-operator.php',$output);
	}
	public function getCommisionValues()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Setting';		
		$output['childurl'] = 'Commission';		
		$output['commission'] = $this->admin->getCommission();
		$this->load->view('backend/admin-commission.php',$output);
	}
	public function addCommission()
	{
		$tablename ='credit_commission';
	
		$results = $this->admin->commonInsert($_POST,$tablename);
		if($results['success'])
		{
			$result['success'] = $results['success'];
			$result['url'] = site_url('admin/commission-percent');
			$result['success_message'] = $results['success_message'];
        }
		else
		{
			$result['success'] = $results['success'];
			$result['error_message'] = $results['error_message'];
        }
		echo json_encode($result);exit;
	}
	public function getTransactionDetail($userRefId = NULL)
	{
		$output['parenturl'] = 'Transaction History';	
		$output['transaction'] = $this->admin->transactionDetial($userRefId);
		$this->load->view('backend/transaction-detail.php',$output);
	}
	public function commissionByUser()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Commission by User';		
		$output['commission'] = $this->admin->getCommissionByUser();
		$this->load->view('backend/commission-by-user.php',$output);
	}
	public function getMessagesDetail()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Notifications';		
		$output['chat'] = $this->admin->getChatMessagesDetail();
		$this->load->view('backend/admin-chat-messages.php',$output);
	}
	public function getChatHistory()
	{
		$userrefId = $this->session->userdata('admin-userRefId');
		if($userrefId == '')
		{
			redirect(site_url('/admin'));
		}
		$output['parenturl'] = 'Notifications';		
		$output['chatHistory'] = $this->admin->getChatHistory($_POST);
		$output['userRefId'] = $_POST['userRefId'];
		$this->load->view('backend/admin-chat.php',$output);
	}
	public function searchTransaction()
	{
		$output['parenturl'] = 'Transaction History';		
		$output['transaction'] = $this->admin->getSearchByTransaction($_POST);
		$this->load->view('backend/transaction-detail-pagination.php',$output);
	}
	public function footerList()
	{
		$output['parenturl'] = 'Setting';		
		$output['childurl'] = 'Footer';		
		$output['footer'] = $this->admin->getFrontendFooterList();
		$this->load->view('backend/frontend-footer-section.php',$output);
	}
	public function editFooterDetail($id)
	{
		$output['parenturl'] = 'Setting';		
		$output['editTitle'] = $this->admin->editFooter($id);
		$this->load->view('backend/add-footer-section',$output);
	}
	public function addFooterDetail()
	{
		$tablename ='credit_frontend_footer';
		$postdata = $this->input->post();
		
	
		$results = $this->admin->commonInsert($postdata,$tablename);
		if($results['success'])
		{
			$result['success'] = $results['success'];
			$result['url'] = site_url('admin/network-operator');
			$result['success_message'] = $results['success_message'];
        }
		else
		{
			$result['success'] = $results['success'];
			$result['error_message'] = $results['error_message'];
        }
		echo json_encode($result);exit;
	}
	public function addBlog()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$tablename ='credit_blog';
			$postdata = $this->input->post();
			if (isset($_FILES['image']) && !empty($_FILES['image']['size'])) 
			{
				$count = count($_FILES['image']['size']);
				$path = 'assets/upload/images/';
				$imagename = $_FILES['image'];
				$date = time();
				$exps = explode('.', $imagename['name']);
				
				$_FILES['image']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
				$_FILES['image']['type'] = $imagename['type'];
				$_FILES['image']['tmp_name'] = $imagename['tmp_name'];
				$_FILES['image']['error'] = $imagename['error'];
				$_FILES['image']['size'] = $imagename['size'];
				
				$config['upload_path'] = $path;
				$config['allowed_types'] = '*';
				$config['max_size'] = '100000';
				$config['max_width'] = '100024';
				$config['max_height'] = '100768';
				
				$this->load->library('upload', $config);
				$this->upload->do_upload('image');
				$data = $this->upload->data();
				$name_array = $data['file_name'];
				$names = $name_array;
				$postdata['image'] = $names;
			}
			$postdata['addedondate'] = date('Y-m-d');
		
			$results = $this->admin->commonInsert($postdata,$tablename);
			if($results['success'])
			{
				$result['success'] = $results['success'];
				$result['url'] = site_url('admin/blog');
				$result['success_message'] = $results['success_message'];
			}
			else
			{
				$result['success'] = $results['success'];
				$result['error_message'] = $results['error_message'];
			}
			echo json_encode($result);exit;
		}
		$output['parenturl'] = 'Setting';		
		$output['childurl'] = 'Footer';		
		$this->load->view('backend/add-blog.php',$output);
		
	}
	public function blogList()
	{
		$output['parenturl'] = 'Setting';		
		$output['childurl'] = 'Footer';		
		$output['footer'] = $this->admin->getBlogListDetail();
		$this->load->view('backend/blog.php',$output);
	}
	public function editBlog($id)
	{
		$output['parenturl'] = 'Setting';		
		$output['editBlog'] = $this->admin->getBlogDetailById($id);
		$this->load->view('backend/add-blog',$output);
	}
	public function blogDetail()
	{
		$output['parentUrl']= "";
        $output['title']    = "";
		$output['blogDetail'] = $this->admin->getBlogListDetail();
		$this->load->view('frontend/blog-detail',$output);
	}
	public function blogInternalDetail($id)
	{
		$output['parentUrl']= "";
        $output['title']    = "";
		$output['blogDetail'] = $this->admin->getBlogDetailById($id);
		$this->load->view('frontend/blog-internal-detail',$output);
	}
	public function faqList()
	{
		$output['parenturl'] = 'Setting';		
		$output['childurl'] = 'Footer';		
		$output['footer'] = $this->admin->getFaqList();
		$this->load->view('backend/faq.php',$output);
	}
	public function editFaq($id)
	{
		$output['parenturl'] = 'Setting';		
		$output['editFaq'] = $this->admin->getFaqDetailById($id);
		$this->load->view('backend/add-faq',$output);
	}
	public function addFaq()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$tablename ='credit_faq';
			$postdata = $this->input->post();
			
		
			$results = $this->admin->commonInsert($postdata,$tablename);
			if($results['success'])
			{
				$result['success'] = $results['success'];
				$result['url'] = site_url('admin/faq');
				$result['success_message'] = $results['success_message'];
			}
			else
			{
				$result['success'] = $results['success'];
				$result['error_message'] = $results['error_message'];
			}
			echo json_encode($result);exit;
		}
		$output['parenturl']= "Setting";
        $this->load->view('backend/add-faq',$output);
	}
	public function faqDetail()
	{
		$output['parentUrl']= "";
        $output['title']    = "";
		$output['faqDetail'] = $this->admin->getFaqList();
		$this->load->view('frontend/faq-detail',$output);
	}

	
	
	
}
