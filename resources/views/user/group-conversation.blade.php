<div class="usr-name">

	<figure>
		<img src="{{url('/upload/groupphotos/'.$groupDetail->profile_image)}}" alt="chat-group">
	</figure>
	<h3>{{$groupDetail->group_name}}</h3> 

</div>

<div class="msg_history">
		
	@if(count($groupConversation) > 0)

		@foreach($groupConversation as $msg)

			@if($msg->user_id == Auth::id())

				<div class="outgoing_msg">

					<div class="sent_msg">

						<p>{{$msg->message}}</p>

					</div>
					<label>{{Auth::user()->name}}</label>

					<div class="outgoing_msg_img"> 
						<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="john"> 
					</div>

				</div>
			@endif
			@if($msg->user_id != Auth::id())

				<div class="incoming_msg">

					<div class="incoming_msg_img"> 
						<img src="{{url('/upload/profileimages/'.$msg->userdetail->avatar)}}" alt="vicky"> 
					</div>
					<label>{{$msg->userdetail->name}}</label>

					<div class="received_msg">

						<div class="received_withd_msg">

							<p>{{$msg->message}}</p>

						</div>

					</div>
				</div>

			@endif

		@endforeach
	@endif
</div>
	<form id="group_conversation" action="{{url('/group-chat')}}" method="post">

		<div class="type_msg">

			@csrf

			<input type="hidden" class="form_control" id="group_id" name="group_id" value="{{$groupDetail->id}}">
			<input type="text" class="form_control" name="message" placeholder="Reply" />
			<input type="submit" value="Send" class="post-btn">
		</div>

	</form>

	


@extends('includes.footer-dashboard')