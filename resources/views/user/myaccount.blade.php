@extends('layouts.dashboard')
@section('page_title')
My Account
@endsection 
@section('content')
<div class="sidebar-rght"> 
	<div class="dashboard-wrapper tp_89 pd_none">
	   <div class="create-post">
		  <h2>My Account</h2>
		  <form class="post-create" name="my_account" id="my_account" action="" method="post" enctype="multipart/form-data">
			@csrf
			 
			 <div class="radio_list">
			<label class="lt_0" for="gender_male">
				<input type="radio" id="gender_male" value="male" name="gender" @if($UserDetail->gender=='male') checked  @endif >
				<span class="circle"></span>Male
			</label>
			<label for="gender_female">
				<input type="radio" id="gender_female" value="female" name="gender" @if($UserDetail->gender=='female') checked  @endif>
				<span class="circle"></span>Female 
			</label>
			</div>
			 <p>
				<input type="text" class="form_control" name="name" placeholder="name" value="{{$UserDetail->name}}" maxlength="50">
			 </p>
			  <p>
				<input type="text" class="form_control" name="email" placeholder="Email Address" value="{{$UserDetail->email}}" readonly>
			 </p>
			<p class="slct">
				<select class="placeholder1 form_control" name="dob">
					<option value="">Year of Birth</option>
					<?php
					for($i=1980; $i<=date('Y'); $i++ ) {
					?>	
						<option value="<?php echo $i; ?>" <?php if($i==$UserDetail->dob) { echo 'selected'; } ?> ><?php echo $i; ?></option>
					<?php 
					}
					?>
				</select> 
			</p>
			
			 <div class="input-group form_control">
				<span class="input-group-btn">
				   <div class="btn1 custom-file-uploader">
					  <input type="file" name="profile_img" onchange="showMyImage(this);this.form.filename.value = this.files.length ? this.files[0].name : ''" accept="image/*"   />
					  Upload Image
				   </div>
				</span>
				<input type="text" name="filename" class="txtsctn" readonly><img src="images/attach.svg" alt="attach" class="attch-file"> 
			 </div>
			 <p>
			 	<img id="thumbnil" src="<?php if($UserDetail->avatar != ''){ echo url('upload/profileimages/'.$UserDetail->avatar); }?>" alt="image" style="<?php if($UserDetail->avatar != ''){ echo 'display:block'; }else{ echo 'display:none'; }?> "/>
			 </p>
			 <p>
				<input type="submit"  class="post-btn" value="Save">
			 </p>
		  </form>
	   </div>
	</div>	
</div>
	
@endsection
    