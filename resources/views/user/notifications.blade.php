@extends('layouts.dashboard')
@section('page_title')
Notifications
@endsection 
@section('content')
<div class="sidebar-rght"> 
    <div class="container">
      <h2>Notifications</h2>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Notification</th>
            <th>Notification Date</th>
          </tr>
        </thead>
        <tbody>
          @if(count($getNotifications) > 0)
            @foreach($getNotifications as $val)

              @if($val->model_name == 'Story')
                @php $url = url('story/'.$val->table_id); @endphp
              @elseif($val->model_name == 'UserGroup')
                @php $url = url('group-detail/'.$val->table_id); ;@endphp
              @elseif($val->model_name == 'Forum')
                @php $url = url('forum-detail/'.$val->table_id); ;@endphp
              @else
                @php $url = '';@endphp
              @endif

              <tr>
                
                <td><a href="{{$url}}" id="check_notification" data-attr="{{$val->id}}">{{$val->notification}}</a></td>
                <td><?php echo $val->created_at->diffForHumans(); ?></td>
              </tr>

            @endforeach
          @else 
            <tr><td colspan="2">No Notifications Found</td></tr>
          @endif 

        </tbody>
      </table>
  </div>

</div>
  
@endsection
    



















