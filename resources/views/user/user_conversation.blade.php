<div class="usr-name">

	<h3>{{$userDetail->name}}</h3>

</div>

<div class="msg_history">

	@if(count($getConversation) > 0)

		@foreach($getConversation->conversationmsg as $msg)

			@if($msg->sender_id == Auth::id())

				<div class="outgoing_msg">
					<div class="sent_msg">
						<p>{{$msg->message}}</p>
					</div>	
					<div class="outgoing_msg_img"> 
						<img src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="john"> 
					</div>
				</div>

			@else
		
				<div class="incoming_msg">

					<div class="incoming_msg_img"> 
						<img src="{{url('/upload/profileimages/'.$userDetail->avatar)}}" alt="vicky"> 
					</div>
					<div class="received_msg">
						<div class="received_withd_msg">
							<p>{{$msg->message}}</p>
						</div>
					</div>
				</div>
			
			@endif
		@endforeach
	@endif

	<form id="user_conversation" action="{{url('/chat')}}" method="post">

		<div class="type_msg">

			@csrf

			<input type="hidden" class="form_control" name="reciever_id" value="{{$userDetail->id}}">
			<input type="hidden" class="form_control" id="chat_id" name="chat_id" value="{{(count($getConversation)>0)?$getConversation->id:''}}">
			<input type="text" class="form_control" name="message" placeholder="Reply" />
			<input type="submit" value="Send" class="post-btn">
		</div>

	</form>
</div>

@extends('includes.footer-dashboard')