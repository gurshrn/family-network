@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection 
@section('content')
@php
use App\User;
$obj=new User(); 
@endphp
<div class="sidebar-rght">
	<div class="container-fluid">
		<div class="dashboard-wrapper pd_none">
			<div class="post-main">
				@if (count($userStory) > 0)
				@foreach($userStory as $story)
				
					<div class="post-main-bx my_story">
						<div class="post-info-sctn">
							<div class="post-info-sctn-lft">
								<figure>
									<img src="{{url('upload/profileimages/'.$story->userdetail['avatar'])}}" alt="user_img">
								</figure>
								<h2>{{$story->userdetail['name']}}</h2> 
							</div>
							<div class="post-info-sctn-rght">
								<div class="date-sctn"> <span><time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($story->created_at)); ?>T<?php echo date('H:i:s', strtotime($story->created_at)); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($story->created_at)); ?></time></span> 
								</div>
							</div>
						</div>
						<h1>
							<a href="#" title="">{{ $story->title }}</a>
						</h1>
						<div class="post-info-cntnt">
							<p>{{ $story->excerpt }} 
								<a href="{{ url('/story/') }}/{{ $story->id  }}" title="" class="opn_stry">Open Story</a>
							</p>
							@if($story->userphotos['user_photo'] != '')
								<figure>
									<img src="upload/userphotos/{{$story->userphotos['user_photo']}}" alt="timeline-full">
								</figure>
							@endif
						 
							<ul class="post-info-cmnt"> 
								<li>
									<a href="javascript:void(0)" id="like-story" data-attr="{{$story->id}}">
										<i class="fa" id="likes-{{$story->id}}" aria-hidden="true"></i>
										<span class="count-like-{{$story->id}}">
											{{count($story->storylikes)}}
											@if(count($story->storylikes) == 1)
												{{'Like'}}
											@else
												{{'Likes'}}
											@endif
										</span>
									</a>
								</li>
								<li>
									<a href="">
										<i class="fa fa-commenting-o" aria-hidden="true"></i>
										<span>
											{{count($story->storycomments)}} 
											@if(count($story->storycomments) == 1)
												{{'Comment'}}
											@else
												{{'Comments'}}
											@endif
										</span>
									</a>
								</li>
							</ul>
						
						</div>
					</div>

				@endforeach
				 
				<input type="hidden" id="total_record" value="{{ $total_record }}">
				<input type="hidden" id="offset" value="2">   
				<a href="javascript:void(0);" class="load_more center-btn btn-blue" onclick="dashboard_story_pagination();">Load More</a>
				@else 
					<div class="alert alert-danger">No Story Found</div>
				@endif    
				
				<!--post-main-bx end -->
			</div>
			<!--post-main end -->
		</div>
	</div>
</div>
		
@endsection
    