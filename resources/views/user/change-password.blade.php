@extends('layouts.dashboard')
@section('page_title')
Change Password
@endsection 
@section('content')
<div class="sidebar-rght"> 
	<div class="dashboard-wrapper tp_89 pd_none">
	   <div class="create-post">
		  	<h2>Change Password</h2>

	 		<form class="post-create"  id="change_password" action="" method="post">

				@csrf

			 	<p>
					<input type="password" class="form_control" name="current_password" placeholder="Current Password">
			 	</p>

			 	<p>
					<input type="password" id="new_password" class="form_control" name="new_password" placeholder="New Password">
			 	</p>

			 	<p>
					<input type="password" id="confirm_password" class="form_control" name="confirm_password" placeholder="Confirm Password">
			 	</p>
			
				<p>
					<input type="submit"  class="post-btn" value="Save">
				</p>

	  		</form>
   		</div>
	</div>	
</div>
	
@endsection
    