@extends('layouts.dashboard')
@section('page_title')
Group
@endsection
@section('content')


	<div class="sidebar-rght grp-intrn">

		<div class="dashboard-wrapper pd_none">

			<div class="grp-intrn-main">

				<div class="forum-bx">

					<figure>
						<img src="{{url('/upload/groupphotos/'.$groupDetail->profile_image)}}" alt="forum-img">
					</figure>

					<div class="forum-cntnt">

						<h2>{{ucfirst($groupDetail->group_name)}}</h2>

						<p>{{ucfirst($groupDetail->description)}}</p>

						<div class="grp-btn"> 

							<a href="#" title="" class="btn-white" data-toggle="modal" data-target="#group-members-modal">View All Member</a> 

							@if($groupDetail->user_id == Auth::user()->id)

								<a href="javascript:void(0)" title="" class="btn-white" data-toggle="modal" data-target="#addmember_modal">Add Member</a> 
							
							@endif

						</div>

					</div>

				</div>

			</div>

			<div class="share-post"> <a href="{{url('/add-group-story?id='.$groupDetail->id)}}" title="" class="share-post-sctn">Share a New Story</a>

				<figure>

					<a href="create-post.html" title=""><img src="{{url('images/camera_icn.svg')}}" alt="camera_icn"></a>

				</figure>

			</div>

			<div class="post-main">
				

				@if(count($groupDetail->groupstory) > 0)

				@foreach($groupDetail->groupstory as $story)

					<div class="post-main-bx my_story user_my_story_<?php echo $story['id'];?>">

						<div class="post-info-sctn">

							<div class="post-info-sctn-lft">

								<figure>
									<img src="{{url('/upload/profileimages/'.$story->userdetail['avatar'])}}" alt="user_img">
								</figure>

								<h2>{{$story->userdetail['name']}}</h2> 
							</div>

							<div class="post-info-sctn-rght">

								<div class="date-sctn"> 
									<span>3 Hours Ago</span> 
									<a href="javacript:void();" title="" class="dot-sctn">
										<span></span>
										<span></span>
										<span></span>
									</a>

									<ul class="view_links1" style="display: none;">
										<li>
											<a href="{{url('edit-story?storyId='.$story['id'])}}" title="" id="crt_edit" class="edit_user_story">
												<svg>
													<use xlink:href="#edit_img"></use>
												</svg><span>Edit</span>
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" title="" data-attr="{{$story['id']}}" id="crt_del" class="delete_user_story">
												<svg>
													<use xlink:href="#del_img"></use>
												</svg>
												<span>Delete</span>
											</a>
										</li>
										<li>
											<a href="javascript:void(0)" title="" data-attr="{{$story['id']}}" id="crt_hide" class="hide_user_story">
												<svg>
													<use xlink:href="#hide_img"></use>
												</svg><span>Hide</span></a>
										</li>
									</ul>

								</div>

							</div>

						</div>

						<h1><a href="#" title="">{{$story['title']}}</a></h1>

						<div class="post-info-cntnt">

							<p>{{$story['excerpt']}}... <a href="{{ url('/story/') }}/{{ $story['id']  }}" title="" class="opn_stry">Open Story</a></p>

							@if($story->userphotos['user_photo'] != '')
								<figure>
									
									<img src="<?php echo url('upload/userphotos/'.$story->userphotos["user_photo"]);?>" alt="timeline-full">
								</figure>
							@endif

							<ul class="post-info-cmnt"> 
								<li>
									<a href="javascript:void(0)" id="like-story" data-attr="{{$story->id}}">
										<i class="fa" id="likes-{{$story->id}}" aria-hidden="true"></i>
										<span class="count-like-{{$story->id}}">
											{{count($story->storylikes)}}
											@if(count($story->storylikes) == 1)
												{{'Like'}}
											@else
												{{'Likes'}}
											@endif
										</span>
									</a>
								</li>
								<li>
									<a href="{{ url('/story/') }}/{{ $story['id']  }}">
										<i class="fa fa-commenting-o" aria-hidden="true"></i>
										<span>
											{{count($story->storycomments)}} 
											@if(count($story->storycomments) == 1)
												{{'Comment'}}
											@else
												{{'Comments'}}
											@endif
										</span>
									</a>
								</li>
							</ul>

						</div>

					</div>

				@endforeach
				@endif

			</div>

		</div>

	</div>

@endsection
	<div class="modal fade" id="group-members-modal" role="dialog">
	    <div class="modal-dialog">
	    
	      <div class="modal-content">

	      	<form id="add-relation" method="post" action="{{'/add-member'}}">
	      		@csrf
	      		<input type="hidden" name="relation_type" class="relation_type">
	      	</form>
	        
	        <div class="modal-body add-member-body">

	        	<a href="javascript:void(0)"><p>{{$groupDetail->groupAdminDetail->name}}</p></a>

	        	@if(count($groupDetail->members) > 0)
					@foreach($groupDetail->members as $val)
	        		
	        			<a href="javascript:void(0)"><p>{{$val->userDetail->name}}</p></a>
	        		
	        		@endforeach
	        	@endif
	        	
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div>
	      
	    </div>
  	</div>

  	<!-- Add Member From Group detail page Modal -->

  	@if($groupDetail->user_id == Auth::user()->id)

  	<div class="modal fade" id="addmember_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h2 class="modal-title" id="exampleModalLabel1">Add Member</h2>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
						<span aria-hidden="true">&times;</span> 
					</button>

				</div>

				<div class="modal-body">

					<form id="add_group_member" method="post" action="{{url('/add-group-member')}}">

						@csrf

						<input type="hidden" name="group_id" value="{{$groupDetail->id}}">
						<p>
							<select class="cptl memb-chosen form_control" name="member_list[]" multiple>

								

									@if(count($groupDetail) > 0)
									
										@foreach($invitedUser as $val)

											<option value="{{$val[0]['id']}}">{{$val[0]['name']}}</option>
						        		
						        		@endforeach
						        	@endif
					        
								
							</select>
						</p>
						<input type="submit" value="Add" class="post-btn">
					</form>

				</div>
			</div>
		</div>
	</div>

	@endif