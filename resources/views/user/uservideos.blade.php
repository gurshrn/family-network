@extends('layouts.dashboard')
@section('page_title')
Photos 
@endsection
@section('content')

	<div class="sidebar-rght story-page">

		@include('user.user_banner_section')

		<div class="dashboard-wrapper photo-wrapper">

			<div class="photo-page video-page">

				<h2>Videos</h2>

				<form class="add-create" id="add_video" action="{{ url('/videos') }}" method="post" enctype="multipart/form-data">
							
					@csrf 

					<div class="browse-section">

						<div class="custom-file">

							<input type="file" name="user_video[]" class="custom-file-input"  multiple>

							<label class="form_control" for="customFile">Upload Videos</label>

						</div>

						<div class="photo-sbmt">

							<input type="submit" value="Upload" class="post-btn"> </div>

					</div>

				</form>

				<div class="photo-list">

					@if (count($userVideos) > 0)

						@php $i=0; @endphp

						@foreach($userVideos as $videos)

						

						<div class="photo-bx">

							<!--Modal: Name-->

							<div class="modal fade" id="modal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

								<div class="modal-dialog" role="document">

									<!--Content-->

									<div class="modal-content">

										<div class="modal-header">

										<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>

										</div>

										<!--Body-->

										<div class="modal-body mb-0 p-0">

											<div class="embed-responsive embed-responsive-16by9 z-depth-1-half">

												<iframe class="embed-responsive-item" src="{{url('/upload/uservideos/'.$videos->user_video)}}" allowfullscreen="" data-gtm-yt-inspected-2340190_699="true" id="864247770" data-gtm-yt-inspected-2340190_908="true"></iframe>

											</div>

										</div>
									</div>
								</div>
							</div>

							<!--Modal: Name-->

							<div class="video-thumb">

							<figure>

								<a>
									<video width="320" height="240" controls>
									  	<source src="{{url('/upload/uservideos/'.$videos->user_video)}}" type="video/mp4">
									</video>
									
								</a>

							</figure>

							<div class="video-overlay"> <a href="#" title="" alt="video" data-toggle="modal" data-target="#modal{{$i}}"><span><i class="fa fa-play" aria-hidden="true"></i></span></a> </div></div>

						</div>

						<?php $i++;?>

						@endforeach
					@else 
						<div class="alert alert-danger">No Videos Found</div>
					@endif 
					

				</div>
			</div>
		</div>

	</div>

@endsection