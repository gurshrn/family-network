@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection 
@section('content')


<div class="sidebar-rght story-page">
	<div class="dashboard-wrapper dashboard-full">
		<div class="tree-main">

            <ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
							
					<a class="nav-link active" id="home-tab1" data-toggle="tab" href="#home1" role="tab" aria-controls="home1" aria-selected="true"><?php echo $myTree['tree_name'];?></a>

				</li>
            

            @php    $i=2; @endphp
				
                @foreach($invitedByTree as $val)
                
					@if(!empty($val))
				
						<li class="nav-item">
							
							<a class="nav-link" id="home-tab<?php echo $i;?>" data-toggle="tab" href="#home<?php echo $i;?>" role="tab" aria-controls="home<?php echo $i;?>" aria-selected="true"><?php echo $val['tree_name'];?></a>

						</li>

						@php $i++; @endphp  
					@endif
				@endforeach
            
				
            </ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane active" id="home1" role="tabpanel" aria-labelledby="home-tab1">
                    <div id="basicdiagram_1" style="width: 100%; height: 480px; border-style: dotted; border-width: 1px;" />
                    </div>
                </div>
			@php    $j=2; @endphp
            @foreach($invitedByTree  as $invited)
			
				@if(!empty($invited))
					
                
					<div class="tab-pane fade" id="home<?php echo $j;?>" role="tabpanel" aria-labelledby="home-tab<?php echo $j;?>">
						<div id="basicdiagram_<?php echo $j;?>" style="width: 100%; height: 480px; border-style: dotted; border-width: 1px;" />
						</div>
					</div>
            
            
					@php $j++; @endphp
				@endif
            @endforeach 
			
			
            </div>
            
  </div></div>



@endsection

@section('dashboardTree')



	<script>
	
			
	<?php 
		if(count($myTree['root_user']) > 0) 
		{ 
			if($myTree['root_user'][0]['profile_image'] != '')
			{
   
			 	$rootUserImage = url('upload/profileimages/'.$myTree['root_user'][0]['profile_image']); 
			}
	
			else
			{
		
				if($myTree['root_user'][0]['gender'] == 'female') 
				{
			
					$rootUserImage = url('upload/profileimages/aunt.png');
				}
				else
				{
					$rootUserImage = url('upload/profileimages/father.png');
				}
			}
			
	?>
			
		
			var items = [

				{ "id":  <?php echo $myTree['root_user'][0]['id']; ?>, "title": "<?php echo $myTree['root_user'][0]['first_name']; ?>", "spouses" : [], "parents": [<?php echo $myTree['root_user'][0]['parent']; ?>], "image": "<?php echo $rootUserImage;?>","description":"<div class='overlay-cntnt'><p><label>Nick Name</label><span><?php echo $myTree['root_user'][0]['first_name'];?></span></p><p><label>DOB</label><span><?php echo $myTree['root_user'][0]['borndate'];?> </span></p><p><label>Relation</label><span>Me</span></p><p><a href='<?php echo url('/edit-profile/'.$myTree['root_user'][0]['id']);?>' class='post-btn'>Edit</a><a href='javascript:void(0)' data-attr='<?php echo $myTree['root_user'][0]['id'];?>' id='add-user-member' class='post-btn'>Add Member</a></p></div>"},


				<?php 
					foreach($myTree['userTree'] as $my_tree) 
					{
						if($my_tree['profile_image'] != '')
						{
			   
						 	$userImage = url('upload/profileimages/'.$my_tree['profile_image']); 
						}
				
						else
						{
					
							if($my_tree['gender'] == 'female') 
							{
						
								$userImage = url('upload/profileimages/aunt.png');
							}
							else
							{
								$userImage = url('upload/profileimages/father.png');
							}
						}
					
						if($my_tree['borndate'] != '')
						{
							$birthdate = date('m/d/Y',strtotime($my_tree['borndate']));
						}
						else
						{
							$birthdate = '';
						}
				?>
				
				
		
			
						{ "id":  <?php echo $my_tree['id']; ?>, "title": "<?php echo $my_tree['first_name'] .' '.$my_tree['last_name']; ?>", "spouses" : [<?php echo $result=(new App\User)->getSpouse($my_tree['id']);?>],  "parents": [<?php echo $my_tree['parent']; ?>], "image": "<?php echo $userImage;?>" ,"description":"<div class='overlay-cntnt'><p><label>Nick Name</label><span><?php echo $my_tree['first_name'];?></span></p><p><label>DOB</label><span><?php echo $birthdate;?></span></p><p><label>Relation</label><span><?php echo ucfirst($my_tree['relation']);?></span></p><p><a href='<?php echo url('/edit-profile/'.$my_tree['id']);?>' class='post-btn'>Edit</a><a href='javascript:void(0)' id='add-user-member' data-attr='<?php echo $my_tree['id'];?>' class='post-btn'>Add Member</a></p></div>"},
			<?php	} ?>
			
			];

			tree(items,'1');
			
	<?php } 
		
		if(count($invitedByTree) > 0)
		{

			$j = 2;

		
			foreach($invitedByTree as $key=>$invite_root_user)
			{
				if($invite_root_user['root_user'][0]['profile_image'] != '')
				{
	   
				 	$invitedRootUserImage = url('upload/profileimages/'.$invite_root_user['root_user'][0]['profile_image']); 
				}
		
				else
				{
			
					if($invite_root_user['root_user'][0]['gender'] == 'female') 
					{
				
						$invitedRootUserImage = url('upload/profileimages/aunt.png');
					}
					else
					{
						$invitedRootUserImage = url('upload/profileimages/father.png');
					}
				}
				
				
	?>

                var items = [
                    
                    { "id":  <?php echo $invite_root_user['root_user'][0]['id']; ?>, "title": "<?php echo $invite_root_user['root_user'][0]['first_name'] .' '.$invite_root_user['root_user'][0]['last_name']; ?>", "spouses" : [],  "parents": [<?php echo $invite_root_user['root_user'][0]['parent']; ?>], "image": "<?php echo $invitedRootUserImage;?>" ,"description":"<div class='overlay-cntnt'><p><label>Nick Name</label><span><?php echo $invite_root_user['root_user'][0]['first_name'];?></span></p><p><label>DOB</label><span><?php echo $invite_root_user['root_user'][0]['borndate'];?></span></p><p><label>Relation</label><span>Me</span></p></div>"},



				
                <?php
           
       				foreach($invite_root_user['userTree'] as $invited_user_tree)
                	{

						if($invited_user_tree['profile_image'] != '')
                        {
                            $invitedUserImage = url('upload/profileimages/'.$invited_user_tree['profile_image']);
                        }
                        else
                        {
                            if($invited_user_tree['gender'] == 'female')
                            {
                                $invitedUserImage = url('upload/profileimages/aunt.png');
                            }
                            else
                            {
                                $invitedUserImage = url('upload/profileimages/father.png');
                            }
                            
                        }
                        if($invited_user_tree['borndate'] != '')
                        {
                            $birthdate = date('m/d/Y',strtotime($invited_user_tree['borndate']));
                        }
                        else
                        {
                            $birthdate = '';
                        }
                ?>
                    
                    { "id":  <?php echo $invited_user_tree['id']; ?>, "title": "<?php echo $invited_user_tree['first_name'] .' '.$invited_user_tree['last_name']; ?>", "spouses" : [<?php echo $result=(new App\User)->getSpouse($invited_user_tree['id']);?>],  "parents": [<?php echo $invited_user_tree['parent']; ?>], "image": "<?php echo $invitedUserImage;?>" ,"description":"<div class='overlay-cntnt'><p><label>Nick Name</label><span><?php echo $invited_user_tree['first_name'];?></span></p><p><label>DOB</label><span><?php echo $birthdate;?></span></p><p><label>Relation</label><span><?php echo ucfirst($invited_user_tree['relation']);?></span></p></div>"},
                <?php } ?>
              	 
                ];

                tree(items,'{{$j}}')
            <?php  $j++; 
                 } 

                 }
                
                 
            ?>
                 
 
                /*var items = [
                    { id: 0, spouses: [1], title: "Mary Spencer", image: "../images/photos/m.png" },
                    { id: 1, spouses: [0], title: "David Kirby", image: "../images/photos/d.png" },
                    { id: 10, spouses: [], title: "Mary Spencer",image: "../images/photos/m.png" },
                    { id: 11, spouses: [10], title: "David Kirby", image: "../images/photos/d.png" },
                    
                    { id: 2,parents: [0,1], spouses: [], title: "Mary Spencer", image: "../images/photos/m.png" },
                    { id: 3, parents: [10,11],spouses: [2], title: "David Kirby",  image: "../images/photos/d.png" },
                    { id: 4, parents: [2, 3], title: "Brad Williams", image: "../images/photos/b.png" },
                    { id: 5, parents: [2, 3], spouses: [ 7], title: "Mike Kirby", image: "../images/photos/m.png"},
                    { id: 7, title: "Sara Kemp", image: "../images/photos/s.png" },
                    { id: 8, parents: [7,5], title: "Leon Kemp",image: "../images/photos/l.png" }
                ];*/
                function tree(items,id){
                var options = new primitives.famdiagram.Config();
                
                options.pageFitMode = primitives.common.PageFitMode.None;
                options.items = items;
                options.cursorItem = 2;
                options.linesWidth = 6;
                options.linesColor = "#ddd";
                options.hasSelectorCheckbox = primitives.common.Enabled.False;
                options.normalLevelShift = 20;
                options.dotLevelShift = 20;
                options.lineLevelShift = 20;
                options.normalItemsInterval = 10;
                options.dotItemsInterval = 10;
                options.lineItemsInterval = 10;
                options.arrowsDirection = primitives.common.GroupByType.Parents;
                options.showExtraArrows = true;
                options.shapeType =primitives.common.ShapeType.Circle;
                jQuery("#basicdiagram_"+id).famDiagram(options);
               
                }
                
            </script>
          
@stop

    