@extends('layouts.dashboard')
@section('page_title')
Group
@endsection
@section('content')

	<div class="sidebar-rght story-page">

		<div class="dashboard-wrapper dashboard-full">

			<div class="group-main">

				<div class="group-create">
					<a href="{{url('/create-group')}}" title="" class="post-btn create-grp">Create Group</a>
				</div>

				<ul class="nav nav-tabs" id="myTab" role="tablist">

					<li class="nav-item"> 
						<a class="nav-link active" id="grp-tab" data-toggle="tab" href="#grp" role="tab" aria-controls="grp" aria-selected="true">My Group</a> 
					</li>

					<li class="nav-item"> 
						<a class="nav-link" id="followgrp-tab" data-toggle="tab" href="#followgrp" role="tab" aria-controls="followgrp" aria-selected="false">Followed Group</a> 
					</li>

				</ul>

				<div class="tab-content" id="myTabContent">

					<div class="tab-pane fade show active" id="grp" role="tabpanel" aria-labelledby="grp-tab">

						<div class="mygroup">
							@if(count($userGroups) > 0)
								@foreach($userGroups as $val)

									<div class="mygroup-bx">

										<a href="{{url('/group-detail/'.$val->id)}}">
											<figure>
												<img src="{{url('/upload/groupphotos/'.$val->profile_image)}}" alt="group-img">
											</figure>

											<h5>{{$val->group_name}}</h5>

											<p>{{count($val->members)+count($val)}} Member</p>
										</a>

									</div>
								@endforeach
							@else 
								<div class="alert alert-danger">No Group Found</div>
							@endif  
					</div>
				</div>

					<div class="tab-pane fade" id="followgrp" role="tabpanel" aria-labelledby="followgrp-tab">

						<div class="mygroup">

							@if(count($followedGroups) > 0)
								@foreach($followedGroups as $groups)



								 	<div class="mygroup-bx">

								 		<a href="{{url('/group-detail/'.$groups->groupDetail->id)}}">

										 	<figure>
										 		<img src="{{url('/upload/groupphotos/'.$groups->groupDetail->profile_image)}}" alt="group-img">
										 	</figure>

										 	<h5>{{$groups->groupDetail->group_name}}</h5>

										 	<p>{{count($groups->groupDetail->members)+count($groups->groupDetail->groupAdminDetail)}} Member</p>
										</a>

								 	</div><!-- mygroup-bx end -->

								@endforeach
							
							@else 
								<div class="alert alert-danger">No Followed Groups Found</div>
							@endif 

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection

		