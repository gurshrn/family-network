
@if(count($memberList->getmembers) > 0)
	@foreach($memberList->getmembers as $val)
		@if(count($val->user) > 0)

	 	<div class="chat_list active_chat">
			<div class="chat_people">
				<a href="javscript:void(0)" data-attr="{{$val->user->id}}" title="" id="get_conversation_detail">
					<div class="chat_img"> 
						<img src="{{url('upload/profileimages/'.$val->user->avatar)}}" alt="john"> 
					</div>
					<div class="chat_ib">
						<h5>{{$val->user->name}}</h5> 
					</div>
				</a>
			</div>
		</div>

		@endif

	@endforeach
@endif

@if(count($invitedUser)>0)
	@foreach($invitedUser as $invUser)
		@if(!empty($invUser[0]))
			<div class="chat_list active_chat">
				<div class="chat_people">
					<a href="javscript:void(0)" data-attr="{{$invUser[0]['id']}}" title="" id="get_conversation_detail">
						<div class="chat_img"> 
							<img src="{{url('upload/profileimages/'.$invUser[0]['avatar'])}}" alt="john"> 
						</div>
						<div class="chat_ib">
							<h5>{{$invUser[0]['name']}}</h5> 
						</div>
					</a>
				</div>
			</div>
		@endif

	@endforeach
@endif

@if(count($memberList->parentUser) > 0)
	<div class="chat_list active_chat">
		<div class="chat_people">
			<a href="javscript:void(0)" data-attr="{{$memberList->parentUser->id}}" title="" id="get_conversation_detail">
				<div class="chat_img"> 
					<img src="{{url('upload/profileimages/'.$memberList->parentUser->avatar)}}" alt="john"> 
				</div>
				<div class="chat_ib">
					<h5>{{$memberList->parentUser->name}}</h5> 
				</div>
			</a>
		</div>
	</div>
@endif
