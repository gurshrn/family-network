@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection
@section('custom_css')
@endsection
@section('custom_js')
<script type="text/javascript" src="{{ URL::asset('tinymce/js/tinymce/tinymce.min.js') }}" ></script>
<script>
jQuery(document).ready(function()
{
	tinymce.init({
		selector: ".editor",
		plugins: "link image",
		inline_styles: false,
		
	});
});
</script>
@endsection

@section('content')
<div class="sidebar-rght">
	<h2>Edit Story</h2>
	<div class="container-fluid">
		
		<form class="post-create" name="create_story" id="create_story" action="{{url('/edit-story')}}" method="post" enctype="multipart/form-data">

			<?php 
				if(isset($_GET['id']))
				{
					$groupId = $_GET['id'];
				}
				else
				{
					$groupId = '';
				}
			?>

			
			<input type="hidden" name="story_id" value="{{$storyDetail->id}}">
			<input type="hidden" name="user_id" value="{{$storyDetail->user_id}}">
			@csrf 
			<p>
				<input type="text" class="form_control" name="title" placeholder="Title" value="{{$storyDetail->title}}" maxlength="50">
			</p>
			
			<div class="input-group form_control">
				
				<input type="hidden" name="user_photo_id" value="{{$storyDetail->user_photo_id}}">

				<span class="input-group-btn">
					<div class="btn1 custom-file-uploader">

	                	<input type="file" name="user_photo" onchange="this.form.filename.value = this.files.length ? this.files[0].name : ''" />

	                		Upload Image

	              	</div>
				</span>

				<input type="text" name="filename" class="txtsctn" readonly>
				<img src="images/attach.svg" alt="attach" class="attch-file"> 
			</div>
			<p>
			 	<img id="thumbnil" src="<?php if($storyDetail->userphotos != ''){ echo url('upload/userphotos/'.$storyDetail->userphotos['user_photo']); }?>" alt="image" style="<?php if($storyDetail->userphotos != ''){ echo 'display:block'; }else{ echo 'display:none'; }?> "/>
			 </p>
			
			<div class="input-group form_control"> 

				<input type="hidden" name="user_video_id" value="{{$storyDetail->user_video_id}}">

				<span class="input-group-btn">
					<div class="btn1 custom-file-uploader">

                		<input type="file" name="user_video" onchange="this.form.filename1.value = this.files.length ? this.files[0].name : ''" />

                			Upload Video              
                	</div>
				</span>

				<input type="text" name="filename1" class="txtsctn" readonly>
				<img src="images/attach.svg" alt="attach" class="attch-file"> 
			</div>
			<p style="<?php if($storyDetail->uservideos != ''){ echo 'display:block'; }else{ echo 'display:none'; }?>">

				<video width="320" height="240" controls >
				  	<source src="{{url('upload/uservideos/'.$storyDetail->uservideos['user_video'])}}" type="video/mp4">
				</video>
			 	
			 </p>

			 @if($storyDetail->group_id == '')

			<p>
				<select class="cptl chosen form_control" name="member_list[]" data-placeholder="Tag Members..." multiple>
					<?php 
						if(isset($userRel))
						{
							$sel = '';
							foreach($userRel as $val)
							{

								
								if($storyDetail->storymembers != '')
								{
									foreach($storyDetail->storymembers as $mem)
									{
										if($mem['member_id'] == $val[0]['id'])
										{
											$sel = "selected='selected'";
										}
										else
										{
											$sel = '';
										}
									}
								}
								else
								{
									$sel = '';
								}

								
								echo '<option value="'.$val[0]['id'].'" '.$sel.'>'.$val[0]['name'].'</option>';
								
							}
						}
					?>
				</select>
			</p>

			@endif

			<p>
				<textarea id="excerpt" class="form_control" name="excerpt" placeholder="Short Decription" >{{$storyDetail->excerpt}}</textarea>
			</p> 

			<p>
				<textarea id="summernote" class="form_control editor" name="description" placeholder="Story" >{{$storyDetail->description}}</textarea>
			</p>
			
			<p>
				<input type="submit"  class="post-btn" value="Save">
			</p>

		</form>
	</div>
</div>
		
@endsection
    