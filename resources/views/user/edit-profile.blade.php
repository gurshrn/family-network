@extends('layouts.dashboard')
@section('page_title')
My Account
@endsection 
@section('content')
<div class="sidebar-rght"> 
	<div class="dashboard-wrapper tp_89 pd_none">
	   <div class="create-post">
		  <h2>Edit Member</h2>
		  <!---"post-create" This class remove from form-->
		  	<form class="post-forum"  id="edit_profile" action="{{url('/update-edit-profile')}}" method="post" enctype="multipart/form-data">
			@csrf

				<input type="hidden" name="editId" value="{{$memberDetail->id}}">
			 
			 	<div class="radio_list">
					<label class="lt_0" for="gender_male">
						
						<input type="radio" id="gender_male" value="male" name="gender" @if($memberDetail->gender=='male') checked  @endif >
						<span class="circle"></span>Male

					</label>
					<label for="gender_female">
						
						<input type="radio" id="gender_female" value="female" name="gender" @if($memberDetail->gender=='female') checked  @endif>
						<span class="circle"></span>Female 

					</label>
					<label for="gender_unknown">
						
						<input type="radio" id="gender_unknown" value="unknown" name="gender" @if($memberDetail->gender=='unknown') checked  @endif>
						<span class="circle"></span>Unknown 

					</label>
				</div>
				<div class="inpt-med1">
			 	<p>
					
					<input type="text" class="form_control" name="first_name" placeholder="First Name" value="{{$memberDetail->first_name}}" maxlength="50">

			 	</p>
			 	<p>
					
					<input type="text" class="form_control" name="last_name" placeholder="Last Name" value="{{$memberDetail->last_name}}" maxlength="50">

			 	</p>
				</div>
				<div class="inpt-med1">
			  	<p>
					
					<input type="text" class="form_control" name="email" placeholder="Email Address" value="{{$memberDetail->email_address}}" <?php if($memberDetail->email_address != ''){ echo 'readonly';} ?>>

			 	</p>
				<p >
					<input type="text" class="form_control" id="datepicker" name="borndate" placeholder="Birth Date" value="<?php if($memberDetail->borndate != ''){ echo date('m/d/Y',strtotime($memberDetail->borndate));}?>">
				</p> 
				
				</div>
                <div class="inpt-med1">
				<p >
					<input type="text" class="form_control" name="bornplace" placeholder="Born Place" value="<?php if($memberDetail->bornplace != ''){ echo $memberDetail->bornplace;}?>">
				</p>
				</div>
				<div class="radio_list">

						<label class="lt_0" for="living">

							<input type="radio" checked="" id="living" name="living" value="Living" @if($memberDetail->live_dead=='Living') checked  @endif> <span class="circle"></span>Living</label>

						<label for="deceased">

							<input type="radio" id="deceased" name="living" value="Deceased" @if($memberDetail->live_dead=='Deceased') checked  @endif> <span class="circle"></span>Deceased</label>

					

					</div>
			
			 	<div class="input-group form_control">
					<span class="input-group-btn">
					   <div class="btn1 custom-file-uploader">
					   		<input type="file" name="profile_img" onchange="showMyImage(this);this.form.filename.value = this.files.length ? this.files[0].name : ''" accept="image/*"   />
						  Upload Image
					   </div>
					</span>
					<input type="text" name="filename" class="txtsctn" readonly>
					<img src="{{ url('images/attach.svg')}}" alt="attach" class="attch-file"> 
			 	</div>
				
				<div class="upload-img">
			 	<p>
			 		<img id="thumbnil" src="<?php if($memberDetail->profile_image != ''){ echo url('upload/profileimages/'.$memberDetail->profile_image); }?>" alt="image" style="<?php if($memberDetail->profile_image != ''){ echo 'display:block'; }else{ echo 'display:none'; }?> "/>
			 	</p>
				</div>
				<div class="tp-24">
			 	<p>
					<input type="submit"  class="post-btn" value="Save">
			 	</p>
				</div>
		  	</form>
	   	</div>
	</div>	
</div>
	
@endsection
    