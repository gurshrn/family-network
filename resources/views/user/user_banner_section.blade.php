<div class="timeline-sctn">
 <figure><img class="timeline-pic" src="{{url('/upload/profileimages/'.Auth::user()->cover_img)}}" alt="timeline-img"></figure>
 <div class="p-image">
	<img src="{{ URL::asset('images/camera-img.svg') }}" alt="camera-img">
	<form name="my_cover_image" id="my_cover_image" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="ChangeUserCoverImage">
		@csrf  
		<input type="file" name="profile_img" onchange="MyCoverImage(this)">
		<input type="submit" name="submit" style="display:none;">  
	</form>   
</div>
</div>
<div class="timeline-image">
	<div class="timeline-lft-main">
	<div class="timeline-lft">
	
	 <figure>
		<img class="profile-pic" src="{{url('/upload/profileimages/'.Auth::user()->avatar)}}" alt="profile-img">
	 </figure>   
	 <div class="p-image">
		<figure>
			<img src="{{ URL::asset('images/camera-img.svg') }}" alt="camera-small-img">
		</figure> 
		<form name="my_image" id="my_image" action="" method="post" enctype="multipart/form-data">
			<input type="hidden" name="action" value="ChangeUserImage">
			@csrf 
			<input type="file" name="profile_img" onchange="MyImage(this)">
			<input type="submit" name="submit" style="display:none;">  
		</form> 
	 </div>
	</div>
	<div class="timeline-name"><h2>{{Auth::user()->name}}</h2></div>
		<ul>
		 <li class="{{ (Request::path()=='bio' ? 'current-nav' : '') }}">
		 	<a href="{{ url('/bio') }}" title="">Bio</a></li> 
			<li class="{{ (Request::path()=='my-story' ? 'current-nav' : '') }}">
				<a href="{{ url('/my-story') }}" title="">Stories</a>
			</li>  
			<li class="{{ (Request::path()=='photos' ? 'current-nav' : '') }}"><a href="{{url('/photos')}}" title="">Photos</a></li> 
			<li class="{{ (Request::path()=='videos' ? 'current-nav' : '') }}"><a href="{{url('/videos')}}" title="">Videos</a></li> 
		</ul>
	</div>
</div>