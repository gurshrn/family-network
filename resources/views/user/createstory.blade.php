@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection
@section('custom_css')
@endsection
@section('custom_js')
<script type="text/javascript" src="{{ URL::asset('tinymce/js/tinymce/tinymce.min.js') }}" ></script>
<script>
jQuery(document).ready(function()
{
	tinymce.init({
		selector: ".editor",
		plugins: "link image",
		inline_styles: false,
		
	});
});
</script>
@endsection

@section('content')
<div class="sidebar-rght story-page">
	<div class="dashboard-wrapper dashboard-full">
	  <div class="create-forum add-member">
		   <h2>Create Story</h2>
		   
		   <form class="post-forum" name="create_story" id="create_story" action="{{url('/create-story')}}" method="post" enctype="multipart/form-data">

			<?php 
				if(isset($_GET['id']))
				{
					$groupId = $_GET['id'];
				}
				else
				{
					$groupId = '';
				}
			?>

			
			<input type="hidden" name="user_id" value="{{$user_id}}">
			<input type="hidden" name="group_id" value="{{$groupId}}">
			@csrf 
			<p>
				<input type="text" class="form_control" name="title" placeholder="Title" maxlength="30">
			</p>
			
			<div class="input-group form_control"> 
				<span class="input-group-btn">
					<div class="btn1 custom-file-uploader">

	                	<input type="file" name="user_photo" class="user_photo" onchange="this.form.filename.value = this.files.length ? this.files[0].name : ''" />

	                		Upload Image

	              	</div>
				</span>

				<input type="text" name="filename" class="txtsctn" readonly>
				<img src="images/attach.svg" alt="attach" class="attch-file"> 
			</div>
			
			<div class="input-group form_control"> 
				<span class="input-group-btn">
					<div class="btn1 custom-file-uploader">

                		<input type="file" name="user_video" onchange="this.form.filename1.value = this.files.length ? this.files[0].name : ''" />

                			Upload Video              
                	</div>
				</span>

				<input type="text" name="filename1" class="txtsctn" readonly>
				<img src="images/attach.svg" alt="attach" class="attch-file"> 
			</div>

			@if($groupId == '')

			<p>
				<select class="cptl chosen form_control" name="member_list[]" data-placeholder="Tag Members..." multiple>
					@if(isset($userRel))
					@foreach($userRel as $val)
						<option value="{{$val[0]['id']}}">{{$val[0]['name']}}</option>
					@endforeach
					@endif
				</select>
			</p>

			@endif

			<p>
				<textarea id="excerpt" class="form_control" name="excerpt" placeholder="Short Decription" ></textarea>
			</p> 

			<p>
				<textarea id="summernote" class="form_control editor" name="description" placeholder="Story" ></textarea>
			</p>
			
			<p class="tp-24">
				<input type="submit"  class="post-btn create_story" value="Save">
			</p>

		</form>
	  </div>
</div>

</div>
		
@endsection
    