<script src="{{ URL::asset('js/jquery3.3.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
	<script src="{{ URL::asset('js/primitives.min.js?5100') }}"></script>
	<script src="{{ URL::asset('js/primitives.jquery.min.js?5100') }}"></script>
	<script src="{{ URL::asset('js/popper.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/wow.min.js') }}"></script>
	<script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
	<script src="{{ URL::asset('js/taginput.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
	<script type="text/javascript" src="{{ URL::asset('js/form.js') }}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/toastr.js') }}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.timeago.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>
	
	@yield('custom_js')
	
	<script type="text/javascript" src="{{ URL::asset('js/my-custom.js') }}"></script>
	<script src="{{ URL::asset('js/dashboard-custom.js') }}"></script> 
<div class="dashboard-wrapper">
	
	<div class="post-main">
			
	@if (count($userStory) > 0)
	@foreach($userStory as $story)

	<div class="post-main-bx my_story user_my_story_<?php echo $story['id'];?>">
		<div class="post-info-sctn">
			<div class="post-info-sctn-lft">

				<figure>
					<img src="{{url('upload/profileimages/'.$story->userdetail['avatar'])}}" alt="user_img">
				</figure>

				<h2>{{$story->userdetail['name']}}</h2> 
			</div>

			<div class="post-info-sctn-rght">
				<div class="date-sctn"> 
					<span>
					<time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($story['created_at'])); ?>T<?php echo date('H:i:s', strtotime($story['created_at'])); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($story['created_at'])); ?>
						</time>
					</span> 
				
				
						
				
				
				</div> 
			</div>
		</div>
		<h1>
			<a href="#" title="">{{ $story['title'] }}</a>
		</h1>
		<div class="post-info-cntnt">
			<p>{{ $story['excerpt'] }} <a href="{{ url('/story/') }}/{{ $story['id']  }}" title="" class="opn_stry">Open Story</a>
			</p>

			@if($story->userphotos['user_photo'] != '')
				<figure>
					<img src="upload/userphotos/{{$story->userphotos['user_photo']}}" alt="timeline-full">
				</figure>
			@endif
			 
			<ul class="post-info-cmnt"> 
				<li>
					<a href="javascript:void(0)" id="like-story" data-attr="{{$story->id}}">
						<i class="fa" id="likes-{{$story->id}}" aria-hidden="true"></i>
						<span class="count-like-{{$story->id}}">
							{{count($story->storylikes)}}
							@if(count($story->storylikes) == 1)
								{{'Like'}}
							@else
								{{'Likes'}}
							@endif
						</span>
					</a>
				</li>
				<li>
					<a href="{{ url('/story/') }}/{{ $story['id']  }}">
						<i class="fa fa-commenting-o" aria-hidden="true"></i>
						<span>
							{{count($story->storycomments)}} 
							@if(count($story->storycomments) == 1)
								{{'Comment'}}
							@else
								{{'Comments'}}
							@endif
						</span>
					</a>
				</li>
			</ul>
			
		</div>
	</div>
	@endforeach
	
	@endif