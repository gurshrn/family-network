<form id="paypalform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">          
<input name="cmd" value="_xclick-subscriptions" type="hidden"> 
<input type="hidden" name="business" value="samriti.business@imarkinfotech.com"> 
<input name="currency_code" value="USD" type="hidden">  
<input type="hidden" id="custom" name="custom" value="<?php echo $data['user_id'];?>">
<input name="item_name" value="<?php echo $data['plan'];?>" type="hidden"> 
<input type="hidden" name="a3" value="<?php echo $data['plan_price'];?>">
<input type="hidden" name="p3" value="1">
<?php if($data['plan_type'] == 'monthly')
{
	$billing_cycle = 'M';
}
else
{
	$billing_cycle = 'M';
} ?>
<input type="hidden" name="t3" value="<?php echo $billing_cycle; ?>">
<input type="hidden" name="src" value="1">
<input type="hidden" name="srt" value="20">
<input name="return" value="{{url('/pricing')}}" type="hidden">  
<input name="notify_url" value="{{url('/paypal-ipn')}}" type="hidden"> 
<input name="imk_course" id="pub_pay_submit_btn" value="Pay with Paypal" type="submit" style="display: none"> 
</form> 

<script type="text/javascript" src="{{ URL::asset('js/jquery3.3.1.min.js') }}"></script>

<script>
	jQuery('#paypalform').submit();
</script>