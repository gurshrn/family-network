@extends('layouts.default')
@section('page_title')
Home
@endsection
@section('content')
	@if(!empty($page->body))
		{!! $page->body !!}
				
	@endif
	{!! $page->block_content !!}
@endsection
    