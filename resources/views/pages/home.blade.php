@extends('layouts.default')
@section('page_title')
Home
@endsection
@section('content')
<section class="banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6">
				<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="images/family_img.png" alt="family_img"></figure>
			</div>
			<div class="col-sm-12 col-md-6 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h1>Share your Stories with the Ones who matter Most.</h1> 
				<a href="#" class="started_btn eff-5">Get Started</a> </div>
		</div>
	</div>
	
</section>
<!-- banner end -->
	<section class="story_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="images/story_img.png" alt="story_img"></figure>
				</div>
				<div class="col-sm-12 col-md-6 story_cntnt wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
					<h2>Everyone loves <br> a Good Story</h2>
					<p>Yearloom lets you record and share the best stories of your life with your family. The childhood pranks, teenage adventures, moments of tragedy and all the life changing experiences that make you one of a kind. Tell your stories so that present and future generations can get to know the real you.</p> <a href="#" title="" class="started_btn eff-5">Learn More</a> </div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="images/dsktop_img.png" alt="dsktop_img"></figure>
				</div>
				<div class="col-sm-12 col-md-6 story_cntnt wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
					<h2>Build your Family Tree</h2>
					<p>Connect deeply with your very first social network: Your family. Build an integrated family tree with our easy-to-use Tree Builder. Access both sides of your family tree and easily locate and learn about any family member. </p> <a href="#" title="" class="started_btn eff-5" data-toggle="modal" data-target="#signup_modal">Start building your Family Tree</a> </div>
			</div>
		</div>
	</section>
	<!-- story_sec end -->
	<section class="testimonial_sec">
		<div class="container">
			<div id="home_testi" class="owl-carousel owl-theme">
				<div class="item">
					<div class="testi_bx">
						<figure><img src="images/john_img.jpg" alt="john_img"></figure>
						<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.”</p>
						<h4>John Smith</h4> </div>
				</div>
				<div class="item">
					<div class="testi_bx">
						<figure><img src="images/john_img.jpg" alt="john_img"></figure>
						<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.”</p>
						<h4>John Smith</h4> </div>
				</div>
			</div>
		</div>
	</section>
	<!--testimonial_sec end -->
	<section class="story_sec story_sec_odd">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="images/wrd_img.png" alt="wrd_img"></figure>
				</div>
				<div class="col-sm-12 col-md-6 story_cntnt wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
					<h2>Lost for Words?</h2>
					<p>It’s totally normal to not know which story to tell. So every week we’ll send you a question about your life that you can use as inspiration to get started.</p> <a href="#" title="" class="started_btn eff-5">See How it Works</a> </div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="images/gallery_img.png" alt="gallery_img"></figure>
				</div>
				<div class="col-sm-12 col-md-6 story_cntnt wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
					<h2>Store Photos, Video and 
Audio Safely</h2>
					<p>Record and upload your stories in text, video, photos or audio. Yearloom is built to safely store your memories for the future, no matter the format. </p>
				</div>
			</div>
		</div>
	</section>
	<!-- story_sec end -->
	<section class="testimonial_sec_odd" style="background:url(images/testi_bg1.jpg) repeat;">
		<div class="container">
			<div id="home_testi1" class="owl-carousel owl-theme">
				<div class="item">
					<div class="testi_bx">
						<figure><img src="images/john_img.jpg" alt="john_img"></figure>
						<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.”</p>
						<h4>John Smith</h4> </div>
				</div>
				<div class="item">
					<div class="testi_bx">
						<figure><img src="images/john_img.jpg" alt="john_img"></figure>
						<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.”</p>
						<h4>John Smith</h4> </div>
				</div>
			</div>
		</div>
	</section>
	<!--testimonial_sec end -->
	<section class="story_sec story_sec_odd">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="images/secure_img.png" alt="secure_img"></figure>
				</div>
				<div class="col-sm-12 col-md-6 story_cntnt wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
					<h2>Private, secure and all yours</h2>
					<p>Your treasures are well guarded. Everything you store with us is 100% yours and we will never share it with anyone. You choose who to share your content with and you can retrieve or delete any of your content at any time.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="images/happy_girl.png" alt="happy_girl"></figure>
				</div>
				<div class="col-sm-12 col-md-6 story_cntnt wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
					<h2>Welcome to the family</h2>
					<p>Sign up now for free and start telling the stories that made you, you.</p> <a href="#" title="" class="started_btn eff-5" data-toggle="modal" data-target="#signup_modal">Sign Up</a> </div>
			</div>
		</div>
	</section>
	<!-- story_sec end -->
@endsection