@extends('layouts.default')
@section('page_title')
Pricing
@endsection 
@section('content')
<?php
	$result=(new App\Models\UserSubscription)->getSubscription();
	
?>
<section class="about_page pricing_page">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Pricing</h2>
				<div class="pricing_main">

					@if(count($result) > 0)
						@foreach($result as $val)

							@php 
								$features = explode(",",$val->choose_plan_options); 

							@endphp


						 	<div class="pricing_bx">
								<div class="pricing_heading">
									<div class="pricing_heading_inr">

										<input type="hidden" class="plan_type" value="monthly">
										<input type="hidden" class="plan_price" value="{{$val->price}}">

							 			<h2 class="plan">{{$val->plan}}</h2>

										<h5>
											<?php 
												if($val->price == 0)
												{
													echo 'Free';
												}
												else
												{
													echo $val->price;
												}
											?>
										</h5>
									</div>
							 	</div>
								<div class="pricing_cntnt_bx">
									 <p>
									 	@if(in_array("my-profile", $features))
									 		<img src="images/grn_tck.svg" alt="grn_tck">
									 	@else
									 		<img src="images/cross_icn.svg" alt="grn_tck">
									 	@endif
									 		<span>Profile Page</span>
									 	</p>
									 <p>
									 	@if(in_array("my-tree", $features))
									 		<img src="images/grn_tck.svg" alt="grn_tck">
									 	@else
									 		<img src="images/cross_icn.svg" alt="grn_tck">
									 	@endif
									 	<span>Family Tree Builder</span>
									 </p>
									 <p>
									 	@if(in_array("family-members", $features))
									 		<img src="images/grn_tck.svg" alt="grn_tck">
									 	@else
									 		<img src="images/cross_icn.svg" alt="grn_tck">
									 	@endif
									 	<span>View family member profiles and stories</span>
									 </p>
									 <p>
									 	@if(in_array("my-stories", $features))
									 		<img src="images/grn_tck.svg" alt="grn_tck">
									 	@else
									 		<img src="images/cross_icn.svg" alt="grn_tck">
									 	@endif
									 	<span>Write stories, upload and store video, photos and audio</span>
									 </p>
									 <p>
									 	@if(in_array("questions", $features))
									 		<img src="images/grn_tck.svg" alt="grn_tck">
									 	@else
									 		<img src="images/cross_icn.svg" alt="grn_tck">
									 	@endif
									 	<span>Weekly Inspiration Question</span>
									 </p>
									 <p>
									 	@if(in_array("my-forums", $features))
									 		<img src="images/grn_tck.svg" alt="grn_tck">
									 	@else
									 		<img src="images/cross_icn.svg" alt="grn_tck">
									 	@endif
									 	<span>Forums</span>
									 </p>
									 <a href="javascript:void(0)" title="" class="strt_here  <?php if(!Auth::user()){ echo 'user_sign_up';} else{ echo 'subscribe_now';}?>">Start Here</a>
								</div>
							</div>
						@endforeach
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
<section class="start_tell">
 <div class="container">
	 <h2>Start telling your stories today.</h2>
	 <h3>Get a 30 day Free Trial today!</h3>
	 <a href="#" class="started_btn eff-5">Start Here</a>
 </div>
</section>
@endsection
    