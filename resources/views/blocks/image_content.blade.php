<div class="row">
		<div class="col-sm-4 rght_0">
			<figure class="wow zoomIn" data-wow-delay="0.1s" data-wow-duration="1000ms">
			<img src="{{ imageUrl($blockData->image, 540, null, ['crop' => false]) }}" alt="reader_img"></figure>
		</div>
		<div class="col-sm-8 lft_0">
			<div class="abt_cntnt_bx">
			<div class="cstm_heading">
				<h3>{{ $blockData->title ?? '' }}</h3>
				
				</div>
				{!! $blockData->html_content ?? '' !!}
				<a href="{{ $blockData->link ?? 'javascript:;' }}" title="" class="view_btn pblsh_btn">{{ $blockData->button_text ?? 'Learn More' }}</a>
			</div>
		</div>
</div>



