<?php
/*  echo "<pre>";
print_r($blockData);
echo "</pre>";  */
?>
<section class="story_sec {{$blockData->image_position!=0?'img-pos-right':'img-pos-left'}}" style="background-color:{{$blockData->background_color}}">
		<div class="container"> 
			<div class="row">
				@if($blockData->image!='')
					<div class="col-sm-12 col-md-6">
						<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="{{ imageUrl($blockData->image, 897, null, ['crop' => false]) }}" alt="story_img"></figure>
					</div>
				@endif
				<div class="col-sm-12 col-md-6 story_cntnt wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
					<h2>{{$blockData->title}}</h2>
					{!! $blockData->html_content !!}
					
					@if($blockData->button_text!='')
					<a href="{{$blockData->link}}" title="" class="started_btn eff-5 {{$blockData->button_class}}">{{$blockData->button_text}}</a> </div> 
					@endif
			</div>
		</div>
</section>

