<section class="testimonial_sec_odd" style="background:url(images/testi_bg1.jpg) repeat;">
		<div class="container">
			<div id="home_testi1" class="owl-carousel owl-theme">
				<div class="item">
					<div class="testi_bx">
						<figure><img src="images/john_img.jpg" alt="john_img"></figure>
						<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.”</p>
						<h4>John Smith</h4> </div>
				</div>
				<div class="item">
					<div class="testi_bx">
						<figure><img src="images/john_img.jpg" alt="john_img"></figure>
						<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus enim, elementum ut interdum ut, tempor ac lacus. Ut gravida congue nisi sed egestas.”</p>
						<h4>John Smith</h4> </div>
				</div>
			</div>
		</div>
	</section>  