<section class="how_works_page {{$blockData->image_position!=0?'img-pos-right':'img-pos-left'}}" style="background-color:{{$blockData->background_color}};">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="{{ imageUrl($blockData->image, 540, null, ['crop' => false]) }}" alt="works_img"></figure>
			</div>
			<div class="col-md-6 col-sm-12 wow fadeIn" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h2>{{$blockData->title}}</h2>
				{!! $blockData->html_content !!}
			</div>
		</div>
	</div>
</section>

