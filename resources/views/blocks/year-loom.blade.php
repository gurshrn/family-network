@php
$result=(new App\Yearloom)->GetYearloom();
@endphp
<section class="yearloom_sectn" style="background-color:{{$blockData->background_color}};">
<div class="container"> 
	<div class="row"> 
		<div class="col-sm-12">
			<h2>{{$blockData->title}}</h2> 
		</div>
	</div>
	@if(!empty($result)) 
		<div class="row loom_bx">
			@foreach($result as $value)
				<div class="col-md-4 col-sm-12 loom_bx_innr wow slideInUp" data-wow-delay="0.1s" data-wow-duration="1000ms">
					<h4>{{$value->title}}</h4>
					{!! $value->content !!}
				</div>
			@endforeach
		</div>
	@endif
</div>
</section>



