<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_relation', function (Blueprint $table)
		{
            $table->string('email_address')->nullable()->after('relation');
			$table->timestamp('borndate')->nullable()->after('email_address');
            $table->string('bornplace')->nullable()->after('borndate');
            $table->string('live_dead')->nullable()->after('bornplace'); 
        });   
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
