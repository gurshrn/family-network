<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('pages.home');
});
*/

Auth::routes(); 

Route::group(['middleware' => 'auth'], function () {
   
   	
    Route::any('/delete-story-upload', 'DashboardController@deleteStoryUpload');  
	Route::any('/update-notification-status', 'DashboardController@updateNotificationStatus');  
    Route::any('/user-notification', 'DashboardController@getUserNotification');  
    Route::post('/add-group-member', 'DashboardController@addGroupMember');  
    Route::any('/chat', 'DashboardController@chat');  
    Route::any('/group-chat', 'DashboardController@groupChat');  
    Route::any('/search-member', 'DashboardController@searchMember');  
    Route::get('/get-conversation-detail', 'DashboardController@getConversationDetail');  
    Route::get('/get-group-conversation-detail', 'DashboardController@getGroupConversationDetail');  
    Route::any('/change-password', 'UserController@changePassword');  
    Route::get('/dashboard', 'DashboardController@dashboard');  
	Route::get('/tree', 'DashboardController@userTree')->name('my-tree');  
	Route::any('/my-account', 'UserController@myAccount')->name('my-profile');  
	Route::any('/create-story', 'DashboardController@createStory');  
	Route::get('/my-story', 'DashboardController@myStory')->name('stories');   
	Route::get('/like-user-story', 'DashboardController@likeUserStory');   
	Route::any('/photos', 'DashboardController@addPhotos');   
	Route::any('/videos', 'DashboardController@addVideos');   
	Route::any('/group', 'DashboardController@showGroup');   
	Route::any('/create-group', 'DashboardController@createGroup');   
	Route::post('/create-add-member', 'DashboardController@addMember');
	Route::get('/forum', 'DashboardController@forum')->name('my-forums');
	Route::any('/create-forum', 'DashboardController@createForum');
	Route::post('/add-forum-comment', 'DashboardController@addForumComment');
	Route::post('/add-story-comment', 'DashboardController@addStoryComment');
	Route::get('/get-member-list', 'DashboardController@getMemberList');
	Route::get('/get-member-email', 'DashboardController@getMemberEmail');
	Route::post('/invite-member', 'DashboardController@inviteMember');
	Route::any('/questions', 'DashboardController@questions')->name('questions');
	Route::post('/update-edit-profile', 'DashboardController@updateProfile');
	Route::get('/delete-user-story', 'DashboardController@deleteUserStory');
	Route::get('/bio', 'DashboardController@userBio');
	Route::any('/edit-story', 'DashboardController@editStory');
	Route::post('/dashboard_story_load_more', 'DashboardController@dashboardStoryPagination'); 
	Route::post('/mystory_load_more', 'DashboardController@myStoryPagination');  
	Route::get('/edit-profile/{slug}', 'DashboardController@editProfile');
	Route::get('/forum-detail/{slug}', 'DashboardController@forumDetail');
	Route::get('/add-group-story', 'DashboardController@createStory');
	Route::get('/edit-story/{slug}', 'DashboardController@editStory');
	Route::any('/group-detail/{slug}', 'DashboardController@groupDetail');   
	Route::get('/story/{slug}', 'DashboardController@storyDetail')->name('story_id');
});

Route::any('/paypal-subscription', 'DashboardController@paypalSubscription');
Route::post('/forgot-password', 'UserController@forgotPassword');
Route::post('/reset-password', 'UserController@resetPassword');

Route::any('/test-mail', 'DashboardController@testMail');

Route::any('/register-family-tree', 'PagesContoller@getPage')->name('home');



//User Ajax

Route::any('/logout', 'HomeController@logout'); 
Route::post('/showuserregisterform', 'UserController@ShowUserRegisterForm'); 
Route::any('/registeruser', 'UserController@registerUser');
Route::post('/my_account_ajax', 'UserAjaxContoller@MyAccount'); 
Route::post('/create_story_ajax', 'UserAjaxContoller@CreateStoryAjax'); 
Route::post('/register', 'Auth\RegisterController@register');
   

Route::post('/change_user_image_ajax', 'UserController@changeUserImage');    
Route::post('/change_user_cover_image_ajax', 'UserController@changeUserCoverImage');    
Route::post('/add-member', 'UserAjaxContoller@ShowAddMemberForm');    
    
 

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

 
Route::get('crop-image', 'ImageController@index');
Route::post('crop-image', ['as'=>'upload.image','uses'=>'ImageController@uploadImage']);
 
 


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
 
Route::get('/{slug?}', 'PagesContoller@getPage')->name('home');

 
