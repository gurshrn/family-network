jQuery(window).on('load', function () {
	jQuery("#preloader").fadeOut();
});
jQuery(document).ready(function () {
    jQuery(".user_view").click(function () {
        jQuery(".view_links").toggle();
    });


    jQuery('.menu-icon').click(function () {
        jQuery("#mySidenav").toggleClass('MyClass');
        jQuery("#main1").toggleClass('tg');
    });


/**** Toggle Responsive Menu ****/

   jQuery(document).on('click', '.cta', function () {
        jQuery(this).toggleClass('active')
    })
	
	    /**** Home Dotted Section ****/
    jQuery(".dot-sctn").click(function () {
        jQuery(this).parent().find(".view_links1").toggle();
       
    }); 
/**** Browse Image ****/

var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                jQuery('.timeline-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    jQuery("#fileupload").on('change', function () {
        readURL(this);
    });
 
/**** Custom Scroll *****/

    /* jQuery(window).on("load", function () {
        jQuery(".inbox_chat,.msg_history").mCustomScrollbar({
            theme: "minimal-dark"
        });
    }); */

   
	
	/**** Profile Image ****/

var readURL2 = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                jQuery('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    jQuery("#file-upload1").on('change', function () {
        readURL2(this);
    });
	/**** Bio Edit Form ****/
jQuery('.edit-pro ').on('click', function() {

    jQuery('form').toggleClass('new-form')  ;
	jQuery('.bio-main').toggleClass('new-form1')  ;

});
	/**** Custom Browse ****/
	 jQuery('.custom-file input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        jQuery('.form_control').html(fileName);
    });
			/**** Select Placeholder ****/
	jQuery('select').change(function () {
		if (jQuery(this).children('option:first-child').is(':selected')) {
			jQuery(this).addClass('placeholder1');
		}
		else {
			jQuery(this).removeClass('placeholder1');
		}
	});

		jQuery(".drop-down").click(function () {
		jQuery(this).next().toggle();
		jQuery(this).toggleClass("open");
	});
		wow = new WOW({
		boxClass: 'wow', // default
		animateClass: 'animated', // default
		offset: 0, // default
		mobile: false, // default
		live: true // default
	})
	wow.init();
	/**** Date picker *****/
/* jQuery(function () {
	jQuery("#datepicker-place").datepicker();
}); */
});