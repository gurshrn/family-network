jQuery(document).ready(function()
{
/*=============Show register modal when invited user click on link from email===========*/

	$(window).on('load',function(){

		var urlString = getUrlVars();
		if(urlString == 'userRefId,invitedById')
		{
			user_sign_up(getUrlVars()["userRefId"],getUrlVars()["invitedById"]);
		}
		if(urlString == 'userId')
		{
			user_forgot_password(getUrlVars()["userId"]);
		}
		
	});
	
	jQuery(document).on('click','#open_login_popup',function(){
		jQuery('#login_pop').modal('show');
	});

	jQuery(document).on('click','.forgot_password',function(){
		jQuery('#forgot_password').modal('show');
		jQuery('#login_pop').modal('hide');
	});

	jQuery(document).on('click','#check_notification',function(){
		var id = $(this).attr('data-attr');
		jQuery.ajax({
			url: '/update-notification-status',
			type: 'Get',
			data:{id:id},
			success: function(result) {    
					
			} 
		});
	});

	function user_forgot_password(userId)
	{
		
		jQuery('#user_reset_password').modal('show');
		jQuery('.forgot_user_id').val(userId);
	}

	

/*=========get url string for show register modal when invited user click on link from email========*/
	
	function getUrlVars()
	{
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}

/*=========Show register modal for normal user========*/

	jQuery('.user_sign_up').click(function(){
		user_sign_up();
	});

/*=================Common user sign up modal================*/

	function user_sign_up(userRefId,invitedById)
	{
		
		jQuery('#open_source').find('.modal-title').empty().append('Create My Family Tree');
		jQuery('#open_source').find('.modal-body').empty().append('Processing...');
		
		jQuery.ajax({
			url: '/showuserregisterform',
			type: 'POST',
			data:{userRefId:userRefId,invitedById:invitedById},
			headers: {
			'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			},
			success: function(result) { 
				if(result != '')
				{
					jQuery('#open_source').modal(); 
					jQuery('#open_source').find('.modal-body').empty().append(result);
				} 
				else
				{
					toastr.error('You have already login');
				} 
			} 
		});
	}
	
		
	var token=jQuery('meta[name=csrf-token]').attr("content");
	var time=jQuery('.timeago').length;
	if(time!=0)
	{	
		jQuery("time.timeago").timeago();
	}

	jQuery('#user_forgot_password').validate({
		rules: { 
			email: {
				required: true,
				email:true
			},
		},
		submitHandler: function(form) {
			
			jQuery.ajax({ 
				type: "POST",
				data: jQuery(form).serialize(), 
				url: form.action,
				dataType:"json",
				success: function(data) 
				{
					if(data.success == true)
					{
						toastr.success(data.msg);
						window.location.href = '/tree';
					}
					else
					{
						toastr.error(data.msg);
					} 
				}
			});
			
		}	 
		
	});	

	jQuery('#user_login').validate({
		rules: { 
			email: {
				required: true,
				email:true
			},
			password: {
				required: true 
			}
		},
		submitHandler: function(form) {
			
			jQuery.ajax({ 
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/login',
				success: function(data) 
				{
					if(data.success == 'true')
					{
						toastr.success(data.message);
						window.location.href = '/tree';
					}
					else
					{
						toastr.error(data.message);
					} 
				}
			});
			
		}	 
		
	});	

	jQuery('#user_register').submit(function(e) {
	    e.preventDefault();
	}).validate({
		rules: { 
			first_name: {
				required: true 
			},
			last_name: {
				required: true 
			},
			email: {
				required: true 
			},
			father_first_name: {
				required: true 
			},
			father_last_name: {
				required: true 
			},
			mother_first_name: {
				required: true 
			},
			mother_last_name: {
				required: true 
			} 
		},
		submitHandler: function(form) {
			var errors="";
			
			var len=jQuery('input[name=gender]:checked').length;
			if(len==0)
			{
				toastr.error('Please select your gender');
				return false;	 
			}	
			jQuery('#user_register').find('input[type=submit]').attr('disabled',true);
			jQuery('#user_register').find('input[type=submit]').val('Processing...');
			
			jQuery.ajax({ 
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/register',
				dataType:"json",
				success: function(data) 
				{
					jQuery('#user_register').find('input[type=submit]').removeAttr('disabled');
					jQuery('#user_register').find('input[type=submit]').val('Get Started');
					if(data.success == true)
					{
						toastr.success(data.msg);
						window.location.href = data.url;
					}
					else
					{
						
                       toastr.error(data.msg);
                        
                    }
				} 
			});
			return false;
		}	 
	});	

/*================Change password validation================*/

	jQuery('#change_password').validate({
		rules: { 
			current_password: 
			{
				required: true,
			},
			confirm_password: 
			{
				required: true,
				equalTo : "#new_password"
			},
			new_password: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Save';
			commonAjaxSubmit(form,btnVal);
		}	 
	});

/*================Change password validation================*/

	jQuery('#user_reset_password_form').validate({
		rules: { 
			confirm_password: 
			{
				required: true,
				equalTo : "#new_password"
			},
			new_password: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Submit';
			commonAjaxSubmit(form,btnVal);
		}	 
	});
	
	
	jQuery('#login_form').validate({
		rules: { 
			email: {
				required: true,
				email:true
			},
			password: {
				required: true 
			}
		},
		submitHandler: function(form) {
			
			jQuery.ajax({ 
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/loginuser',
				success: function(data) 
				{
					 
					
				} 
			});
			
		}	 
		
	});	

	setTimeout(function(){
	
		jQuery('#my_account').validate({
			rules: { 
				name: {
					required: true
				},
				email: {
					required: true,
					email:true
				},
				dob: {
					required: true 
				}
			}, 
			submitHandler: function(form) {

				jQuery(form).find('input[type=submit]').attr('disabled',true);
				jQuery(form).find('input[type=submit]').val('Processing...');
				
				jQuery(form).ajaxSubmit({
					type: "POST",
					data: jQuery(form).serialize(), 
					url: '/my-account',
					dataType:"json",
					success: function(data) 
					{ 
						if(data.success == 1)
						{
							toastr.success(data.msg);
							jQuery(form).find('input[type=submit]').attr('disabled',false);
							jQuery(form).find('input[type=submit]').val('Save');		
							setTimeout(function(){
							  location.reload();
							}, 1500);
						}
						else
						{
							toastr.error(data.msg);
						}
					} 
				});
				
			}	 
			
		});
	});	
	
	
	jQuery('#my_image').validate({
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/change_user_image_ajax',
				success: function(data) 
				{
					if(data=='1')
					{
						location.reload();		
					} 
				} 
			});
			
		}	 
		
	});	
	
	jQuery('#my_cover_image').validate({
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/change_user_cover_image_ajax',
				success: function(data)  
				{ 
					if(data=='1')
					{
						location.reload();		
					} 
				} 
			});
			
		}	 
		
	});	

/*================Add story Photo validation================*/

	jQuery('#add_photo').validate({
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: jQuery(form).action, 
				success: function(data)  
				{ 

					if(data == 1)
					{
						
						toastr.success('Image uploaded successfully');		
						setTimeout(function(){
						  location.reload();
						}, 2000);	
					} 
					else
					{
						toastr.error('Image not uploaded successfully');		
						setTimeout(function(){
						  location.reload();
						}, 2000);	
					}
				} 
			});
			
		}	 
		
	});	

/*================Add story Video validation================*/

	jQuery('#add_video').validate({

		rules: { 
			"user_video[]": {
				required: true,
				extension: "mp3|mpeg|mp4|webm|mov"
			},
			
		}, 
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: jQuery(form).action, 
				success: function(data)  
				{ 

					if(data!='' && data != 0)
					{
						
						toastr.success('Video uploaded successfully');		
						setTimeout(function(){
						  location.reload();
						}, 2000);	
					} 
				} 
			});
			
		}	 
		
	});	

/*================Add member validation================*/

	jQuery('#add-member-detail').validate({
		rules: { 
			first_name: 
			{
				required: true,
			},
			last_name: 
			{
				required: true 
			}
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Add Member';

			commonAjaxSubmit(form,btnVal);
			
		}	 
		
	});	

/*================Create group validation================*/

	jQuery('#create-group').validate({
		rules: { 
			group_name: 
			{
				required: true,
			},
			profile_image:
			{
				required: false,
				accept:"jpg,png,jpeg,gif"
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Create Group';
			
			commonAjaxSubmit(form,btnVal);
			
		}	 
		
	});	

/*================Create group validation================*/

	jQuery('#add_group_member').validate({
		
		submitHandler: function(form) 
		{
			var btnVal = 'Add';
			
			commonAjaxSubmit(form,btnVal);
			
		}	 
		
	});	

/*================Create forum validation================*/

	jQuery('#create-forum').validate({
		rules: { 
			title: 
			{
				required: true,
			},
			cat_id: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
			image:
			{
				required: true,
				extension:"jpg,png,jpeg,gif"
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Create';
			
			commonAjaxSubmit(form,btnVal);
			
		}	 
		
	});

/*================Add forum comment validation================*/

	jQuery('#forum_comment').validate({
		rules: { 
			forum_comment: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			jQuery(form).find('input[type=submit]').attr('disabled',true);
			jQuery(form).find('input[type=submit]').val('Processing...');
			jQuery.ajax({ 
				type: form.method,
				data: jQuery(form).serialize(), 
				url: form.action,
				dataType:"json",
				success: function(data) 
				{
					if(data != '')
					{
						jQuery('.no-comment').html('');
						jQuery('.forum-comment-data').prepend(data.html);
						if(data.count == 1)
						{
							jQuery('.coments').html(data.count+' Comment');
						}
						else
						{
							jQuery('.coments').html(data.count+' Comments');
						}
						jQuery('#forum_comment').trigger('reset');
						jQuery(form).find('input[type=submit]').attr('disabled',false);
						jQuery(form).find('input[type=submit]').val('Submit');
						toastr.success('Comment added successfully');
					}
					else
					{
						toastr.error('Comment not added successfully');
					}
				} 
			});
			
		}	 
		
	});

/*================Add story comment validation================*/

	jQuery('#story_comment').validate({
		rules: { 
			story_comment: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			jQuery(form).find('input[type=submit]').attr('disabled',true);
			jQuery(form).find('input[type=submit]').val('Processing...');
			jQuery.ajax({ 
				type: form.method,
				data: jQuery(form).serialize(), 
				url: form.action,
				dataType:"json",
				success: function(data) 
				{
					if(data != '')
					{
						jQuery('.no-comment').html('');
						jQuery('.story-comment-data').prepend(data.html);
						if(data.count == 1)
						{
							jQuery('.coments').html(data.count+' Comment');
						}
						else
						{
							jQuery('.coments').html(data.count+' Comments');
						}
						
						jQuery(form).trigger('reset');
						jQuery(form).find('input[type=submit]').attr('disabled',false);
						jQuery(form).find('input[type=submit]').val('Submit');
						toastr.success('Comment added successfully');
					}
					else
					{
						toastr.error('Comment not added successfully');
					}
				} 
			});
			
		}	 
		
	});
/*================Add forum comment validation================*/

	jQuery('#user_invite_member').validate({
		rules: { 
			member_id: 
			{
				required: true,
			},
			user_email: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Invite';
			commonAjaxSubmit(form,btnVal);
		}	 
	});
/*================question answer validation================*/

	
	jQuery(document).on('click','.answer-btn',function(){
		
		 var formId = jQuery(this).parents().find('form').attr('id');
		jQuery('#'+formId).validate({
			rules: { 
				user_answer: 
				{
					required: true,
				},
				user_photo:
				{
					required: false,
					extension:"jpg,png,jpeg,gif"
				},
			},
			submitHandler: function(form) 
			{
				var btnVal = 'Submit';
				commonAjaxSubmit(form,btnVal);
			}
		});
	});

/*================Chat Module validation================*/

	jQuery('#user_conversation').validate({
		rules: 
		{ 
			message: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			jQuery(form).find('input[type=submit]').attr('disabled',true);
			jQuery(form).find('input[type=submit]').val('Processing...');

			jQuery.ajax({ 
				type: form.method,
				data: new FormData(form), 
				url: form.action,
				processData: false,
		        contentType: false,
				dataType:"json",
				success: function(data) 
				{
					jQuery(form).find('input[type=submit]').attr('disabled',false);
					jQuery(form).find('input[type=submit]').val('Send');
					
					if(data.success == true)
					{
						jQuery(form).trigger('reset');
						jQuery('#chat_id').val(data.chatId);
						jQuery('.msg_history').append(data.html);
					}
					else
					{
						toastr.error(data.msg);
					}
				} 
			});
			
		}	 
		
	});	

/*================Group Chat Module validation================*/

	jQuery('#group_conversation').validate({
		rules: 
		{ 
			message: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			jQuery(form).find('input[type=submit]').attr('disabled',true);
			jQuery(form).find('input[type=submit]').val('Processing...');

			jQuery.ajax({ 
				type: form.method,
				data: new FormData(form), 
				url: form.action,
				processData: false,
		        contentType: false,
				dataType:"json",
				success: function(data) 
				{
					jQuery(form).find('input[type=submit]').attr('disabled',false);
					jQuery(form).find('input[type=submit]').val('Send');
					
					if(data.success == true)
					{
						jQuery(form).trigger('reset');
						jQuery('#group_id').val(data.groupId);
						jQuery('.msg_history').append(data.html);
					}
					else
					{
						toastr.error(data.msg);
					}
				} 
			});
			
		}	 
		
	});	

/*================Edit profile validation================*/

	jQuery('#edit_profile').validate({
		submitHandler: function(form) 
		{
			var btnVal = 'Save';
			commonAjaxSubmit(form,btnVal);
		}
	});
/*================Create story validation================*/	
	
	setTimeout(function(){
		jQuery('#create_story').validate({
			rules: { 
				title: {
					required: true,

				},
				content: {
					required: true
				},
				user_video:{
					required: false,
           	 		extension: "mp3|mpeg|mp4|webm|mov"
				},
				user_photo:{
					required: false,
           	 		extension:"jpg,png,jpeg,gif"
				},
			}, 
			submitHandler: function(form) {
				jQuery('#create_story').find('input[type=submit]').val('Processing...');
				jQuery('#create_story').find('input[type=submit]').attr('disabled',true);
				jQuery(form).ajaxSubmit({
					type: "POST",
					data: jQuery(form).serialize(), 
					url: form.action,
					dataType:"json",
					success: function(data) 
					{ 
						
						if(data != 0)
						{
							jQuery('#create_story').find('input[type=submit]').val('Save');
							jQuery('#create_story').find('input[type=submit]').removeAttr('disabled');
							
							toastr.success(data.msg);
							setTimeout(function(){
							  window.location.href = data.url;
							 
							}, 2000);
							
						}
						else
						{
							toastr.error(data.msg);
						}
						/*if(data!='0')
						{
							jQuery('#create_story').find('input[type=submit]').val('Save');
							jQuery('#create_story').find('input[type=submit]').removeAttr('disabled');
							
							toastr.success('Your Story has been published');
							jQuery('#create_story').trigger('reset');		 
						}*/ 
					} 
				});
				
			}	 
			
		});	
	}, 500);

});	

function commonAjaxSubmit(form,btnVal)
{
	jQuery(form).find('input[type=submit]').attr('disabled',true);
	jQuery(form).find('input[type=submit]').val('Processing...');

	jQuery.ajax({ 
		type: form.method,
		data: new FormData(form), 
		url: form.action,
		processData: false,
		contentType: false,
		dataType:"json",
		success: function(data) 
		{
			jQuery(form).find('input[type=submit]').attr('disabled',false);
			jQuery(form).find('input[type=submit]').val(btnVal);
			
			if(data.success == true)
			{
				jQuery(form).trigger('reset');
				toastr.success(data.msg);
				if(data.url)
				{
					setTimeout(function(){
					  window.location.href = data.url;
					}, 2000);
				}
				else
				{
					setTimeout(function(){
					  location.reload();
					}, 2000);
				}
					
			}
			else
			{
				toastr.error(data.msg);
			}
		} 
	});
}

function mystory_pagination()
{
	var total_record=jQuery('#total_record').val();
	var offset=jQuery('#offset').val();
	jQuery('.load_more').html('Processing...');  
	jQuery.ajax({ 
		url: '/mystory_load_more',
		type: 'POST',
		data: "offset="+offset, 
		headers: {
		'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		}, 
		success: function(result) {    
			jQuery('.load_more').html('Load More');  
			offset=parseInt(offset)+1; 
			jQuery('#offset').val(offset);
			jQuery(result).insertAfter('.my_story:last');  
			
			var len=parseInt(offset)-1;
			if(len==total_record)
			{
				jQuery('.load_more').hide();		
			}
			jQuery("time.timeago").timeago();		
		}   
	});  
	
	
}	 


function dashboard_story_pagination()
{
	var total_record=jQuery('#total_record').val();
	var offset=jQuery('#offset').val();
	jQuery('.load_more').html('Processing...');  
	jQuery.ajax({ 
		url: '/dashboard_story_load_more',
		type: 'POST',  
		data: "offset="+offset,
		dataType:"html",  
		headers: {
		'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		}, 
		success: function(result) {    
			jQuery('.load_more').html('Load More');  
			offset=parseInt(offset)+1; 
			jQuery('#offset').val(offset);
			jQuery(result).insertAfter('.my_story:last');  
			
			var len=parseInt(offset)-1;
			console.log(len);
			if(len==total_record)
			{
				jQuery('.load_more').hide();		
			}
			jQuery("time.timeago").timeago();		
		}   
	}); 
}	


function showMyImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {           
		var file = files[i];
		var imageType = /image.*/;     
		if (!file.type.match(imageType)) 
		{
			toastr.error('This File type not allowed here');
			jQuery('input[type=file]').val('');
			jQuery('#thumbnil').hide();
			return false;	 
		}           
		var img=document.getElementById("thumbnil");            
		img.file = file;    
		var reader = new FileReader();
		reader.onload = (function(aImg) { 
			return function(e) { 
				aImg.src = e.target.result; 
				jQuery('#thumbnil').show();
			}; 
		})(img);
		reader.readAsDataURL(file);
	}    
}


function MyImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {           
		var file = files[i];
		var imageType = /image.*/;     
		if (!file.type.match(imageType))
		{
			toastr.error('File type allowed jpeg/png/jpg only');
			return false;	 
		}           
		jQuery('#my_image').find('input[type=submit]').click();
	}    
}



function imageValidation(fileInput)
{
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) 
	{           
		var file = files[i];
		var imageType = /image.*/;     
		if (!file.type.match(imageType))
		{
			toastr.error('File type allowed jpeg/png/jpg only');
			jQuery('input[type=file]').val('');
			$('input[type="submit"]').prop('disabled', true);
			jQuery('#thumbnil').hide();
			return false;	 
		} 
		else
		{
			$('input[type="submit"]').prop('disabled', false);
		} 


		
	}    
}

function MyCoverImage(fileInput) {
	var error=0;
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {           
		var file = files[i];
		var imageType = /image.*/;     
		if (!file.type.match(imageType)) {
			toastr.error('File type allowed jpeg/png/jpg only');
			return false;
			 
		}           
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function (e) {
			var image = new Image();
			image.src = e.target.result;
		}  
		jQuery('#my_cover_image').find('input[type=submit]').click();
	}    
}



