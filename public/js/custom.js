jQuery(window).on('load', function () {
	jQuery("#preloader").fadeOut();
});
$('#datepicker').datepicker({
	changeMonth: true, 
    changeYear: true,
    format: 'd/m/yyyy',
    maxDate: 'today',
});
$('#bio_datepicker').datepicker({
	changeMonth: true, 
    changeYear: true,
    format: 'd/m/yyyy',
    maxDate: 'today',
});
$('.datepicker').datepicker({
	changeMonth: true, 
    changeYear: true,
    format: 'd/m/yyyy',
    maxDate: 'today',
});
jQuery(function (jQuery) {
	//Preloader
	// Wow 
		wow = new WOW({
		boxClass: 'wow', // default
		animateClass: 'animated', // default
		offset: 0, // default
		mobile: false, // default
		live: true // default
	})
	wow.init();

	
	// Bootstrap Slider 
	jQuery('.carousel').carousel({
		interval: 3000
		, cycle: true
	});

	/**** Team Slider ****/
 var owl = jQuery("#home_testi");
    owl.owlCarousel({
        itemsCustom: [
                [768, 1]
                , [992, 1]
                , [1200, 1]
                , [2000, 1]
				]
        , navigation: true
        , pagination: false
        , slideSpeed: 1000
        , scrollPerPage: true
		, autoPlay: true
    });
	var owl = jQuery("#home_testi1");
    owl.owlCarousel({
        itemsCustom: [
                [768, 1]
                , [992, 1]
                , [1200, 1]
                , [2000, 1]]
        , navigation: true
        , pagination: false
        , slideSpeed: 1000
        , scrollPerPage: true
		, autoPlay: true
    });
	
		/**** Select Placeholder ****/
	jQuery('select').change(function () {
		if (jQuery(this).children('option:first-child').is(':selected')) {
			jQuery(this).addClass('placeholder1');
		}
		else {
			jQuery(this).removeClass('placeholder1');
		}
	});
	/**** Textarea First Letter Capital ****/
	jQuery('textarea.form_control').on('keypress', function (event) {
		var $this = jQuery(this)
			, thisVal = $this.val()
			, FLC = thisVal.slice(0, 1).toUpperCase();
		con = thisVal.slice(1, thisVal.length);
		jQuery(this).val(FLC + con);
	});
	
	
	
	

		var menu = $('.js-item-menu');
		var sub_menu_is_showed = -1;
		for (var i = 0; i < menu.length; i++) {
			$(menu[i]).on('click', function (e) {
				e.preventDefault();
				$('.js-right-sidebar').removeClass("show-sidebar");
				if (jQuery.inArray(this, menu) == sub_menu_is_showed) {
					$(this).toggleClass('show-dropdown');
					sub_menu_is_showed = -1;
				}
				else {
					for (var i = 0; i < menu.length; i++) {
						$(menu[i]).removeClass("show-dropdown");
					}
					$(this).toggleClass('show-dropdown');
					sub_menu_is_showed = jQuery.inArray(this, menu);
				}
			});
		}
		$(".js-item-menu, .js-dropdown").click(function (event) {
			event.stopPropagation();
		});
		$("body,html").on("click", function () {
			for (var i = 0; i < menu.length; i++) {
				menu[i].classList.remove("show-dropdown");
			}
			sub_menu_is_showed = -1;
		});
	
	
	
});

jQuery(document).ready(function () {
   jQuery('#login_pop .forgot_password').click(function(){ 
	    jQuery('#login_pop').modal('hide');
   });
   
  setTimeout(function () {
		jQuery('.fixed-btn').removeClass('back');   
	}, 5000);
	    
	   
	   
});





jQuery(window).on('load', function () {
	setTimeout(function () {
		jQuery('body').addClass('loaded');
	}, 100);
});
// grab the initial top offset of the navigation 
var stickyNavTop = jQuery('body').offset().top;
// our function that decides weather the navigation bar should have "fixed" css position or not.
var stickyNav = function () {
	var scrollTop = jQuery(window).scrollTop(); // our current vertical position from the top
	// if we've scrolled more than the navigation, change its position to fixed to stick to top,
	// otherwise change it back to relative
	if (scrollTop > 10) {
		jQuery('header').addClass('sticky animated fadeInDown');
	}
	else {
		jQuery('header').removeClass('sticky fadeInDown');
	}
};
stickyNav();
// and run it again every time you scroll
jQuery(window).scroll(function () {
	stickyNav();
});

/*=============Dashboard show select member popup============*/


jQuery(document).on('click','#add-user-member',function(){
	var memberId = jQuery(this).attr('data-attr');
	jQuery('#select-member-modal').modal('show');
	jQuery('.member_id').val(memberId);
});

/*=============Dashboard show add member popup============*/

jQuery(document).on('click','.select-member',function(){
	var memberType = jQuery(this).attr('data-attr');
	jQuery('.relation_type').val(memberType);
	if($('.relation_type').val() != '')
	{
		jQuery('#add-relation').submit();
	}
});

/*===========Show invite friend modal=============*/

jQuery(document).on('click','#invite-family',function(){
	jQuery('#invite_member_modal').modal('show');
});

/*============On select member get member email from invite friend modal===========*/

jQuery(document).on('change','.select_family_members',function(){
	jQuery('#member_email').css('display','block');
	var memberId = jQuery(this).val();
	jQuery.ajax({ 
		type: "GET",
		data:{memberId:memberId},
		url: '/get-member-email',
		dataType : "html",
		success: function(data) 
		{
			jQuery('.user_email').val(data);
		} 
	});
});

/*=============Show story delete popup==============*/

jQuery(document).on('click','.delete_user_story',function(){
	var storyId = $(this).attr('data-attr');
	jQuery('.userStoryId').val(storyId);
	jQuery('#delete_modal').modal('show');
});

/*===========Delete user story on click yes==============*/

jQuery(document).on('click','.yes-delete',function(){
	var storyId = $('.userStoryId').val();
	jQuery.ajax({ 
		type: "GET",
		data:{storyId:storyId},
		url: '/delete-user-story',
		dataType : "html",
		success: function(data) 
		{
			if(data == 1)
			{
				toastr.success('Story deleted successfully');
				$('#delete_modal').modal('hide');
				setTimeout(function(){
				  location.reload();
				}, 1500);
			}
			else
			{
				toastr.error('Story not deleted successfully');
			}
		} 
	});
});

/*=============Hide user story============*/

jQuery(document).on('click','.hide_user_story',function(){
	var storyId = $(this).attr('data-attr');
	$('.user_my_story_'+storyId).hide();
});

/*=============Like user story==============*/

jQuery(document).on('click','#like-story',function(){
	var storyId = $(this).attr('data-attr');
	jQuery.ajax({ 
		type: "GET",
		data:{storyId:storyId},
		url: '/like-user-story',
		dataType : "json",
		success: function(data) 
		{
			if(data.success == 1)
			{
				count = data.likes+1;
				toastr.success(data.msg);
				$('#likes-'+storyId).removeClass('fa-thumbs-o-up');
				$('#likes-'+storyId).addClass('fa-thumbs-up');
				countLiketext(count,storyId);
			}
			else
			{
				count = data.likes-1;
				toastr.error(data.msg);
				$('#likes-'+storyId).addClass('fa-thumbs-o-up');
				$('#likes-'+storyId).removeClass('fa-thumbs-up');
				countLiketext(count,storyId);
				
			}
		} 
	});
});

/*==========get chat conversation on click member==========*/

jQuery(document).on('click','#get_conversation_detail',function(){
	var memberId = $(this).attr('data-attr');
	jQuery.ajax({ 
		type: "GET",
		data:{memberId:memberId},
		url: '/get-conversation-detail',
		dataType : "html",
		success: function(data) 
		{
			jQuery('.mesgs').html(data);
		} 
	});
});

/*==========seacrh member for chat=========*/

jQuery(document).on('keyup','.search-member',function(){
	var keyword = $(this).val();
	jQuery.ajax({ 
		type: "GET",
		data:{keyword:keyword},
		url: '/search-member',
		dataType : "html",
		success: function(data) 
		{
			jQuery('.chat_members').html(data);
		} 
	});
});

/*==========get group conversation on click group=========*/

jQuery(document).on('click','#group_chat',function(){
	var groupId = $(this).attr('data-attr');
	jQuery.ajax({ 
		type: "GET",
		data:{groupId:groupId},
		url: '/get-group-conversation-detail',
		dataType : "html",
		success: function(data) 
		{
			jQuery('.mesgs').html(data);
		} 
	});
});

jQuery(document).on('click','.delete-story-image',function(){
	var id = $(this).attr('data-attr');
	jQuery('.id').val(id);
	jQuery('.action').val('delete-story-upload');
	jQuery('.type').val('image');
	jQuery('#delete-msg').html('Are you sure you want to delete this photo?');
	jQuery('#delete_upload_modal').modal('show');
});

/*===========Delete user story on click yes==============*/

jQuery(document).on('click','.delete-yes',function(){
	var id = $('.id').val();
	var action = $('.action').val();
	var type = $('.type').val();
	jQuery.ajax({ 
		type: "GET",
		data:{id:id,type:type},
		url: '/'+action,
		dataType : "html",
		success: function(data) 
		{
			
			if(data == 1)
			{
				$('#delete_upload_modal').modal('hide');
				toastr.success('Story deleted successfully');
				
				setTimeout(function(){
				  location.reload();
				}, 1500);
			}
			else
			{
				toastr.error('Story not deleted successfully');
			}
		} 
	});
});

/*============On click start here subscription pop up open==========*/

jQuery(document).on('click','.subscribe_now',function(){
	var plan = $('.plan').html();
	var plan_type = $('.plan_type').val();
	var plan_price = $('.plan_price').val();
	$('#subscribe_modal').modal('show');
	$('.user_plan').val(plan);
	$('.user_plan_type').val(plan_type);
	$('.user_plan_price').val(plan_price); 
});


function countLiketext(count,storyId)
{
	console.log(count);
	if(count == 0 || count == 1)
	{
		text = 'Like';
	}
	else
	{
		text = 'Likes';
	}
	$('.count-like-'+storyId).html(count+' '+text);

}


