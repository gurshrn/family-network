<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Story extends Model
{
    public function GetUserStory($userId,$offset,$limit)
	{
		$result = DB::table('stories')->select('stories.*', 'user_photos.user_photo', 'user_videos.user_video')->leftJoin('user_photos', 'user_photos.id', '=', 'stories.user_photo_id')->leftJoin('user_videos', 'user_videos.id', '=', 'stories.user_video_id')->leftJoin('story_members', 'story_members.story_id', '=', 'stories.id')->where('stories.user_id', '=' , $userId)->orWhere('member_id',$userId)->orderBy('stories.id', 'desc')->offset($offset)->limit($limit)->get();
		
		$total_record = DB::table('stories')->select('stories.*', 'user_photos.user_photo', 'user_videos.user_video')->leftJoin('user_photos', 'user_photos.id', '=', 'stories.user_photo_id')->leftJoin('user_videos', 'user_videos.id', '=', 'stories.user_video_id')->leftJoin('story_members', 'story_members.story_id', '=', 'stories.id')->where('stories.user_id', '=' , $userId)->orWhere('member_id',$userId)->orderBy('stories.id', 'desc')->get()->count();
		
		$array = array('result' => $result, 'total_record' => $total_record);
		return json_encode($array); 
	}
	public function getUserDetail($user_id)
	{
		$result = DB::table('users')->where('id', $user_id)->get();
		return $result[0];
	}

	public function GetAllUserStory($offset,$limit)
	{
		$result = DB::table('stories')->orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
		$total_record = DB::table('stories')->orderBy('id', 'desc')->get()->count();
		
		$array = array('result' => $result, 'total_record' => $total_record);
		return json_encode($array);    
	}	


	public function GetStoryDetailById($story_id)
	{
		$result = DB::table('stories')->select('stories.*', 'user_photos.user_photo', 'user_videos.user_video')->leftJoin('user_photos', 'user_photos.id', '=', 'stories.user_photo_id')->leftJoin('user_videos', 'user_videos.id', '=', 'stories.user_video_id')->where('stories.id', '=' , $story_id)->get();
		
		$total_record = DB::table('stories')->select('stories.*', 'user_photos.user_photo', 'user_videos.user_video')->leftJoin('user_photos', 'user_photos.id', '=', 'stories.user_photo_id')->leftJoin('user_videos', 'user_videos.id', '=', 'stories.user_video_id')->where('stories.id', '=' , $story_id)->get()->count();
		
		$array = array('result' => $result, 'total_record' => $total_record);
		return json_encode($array);  
	}	

	
}
