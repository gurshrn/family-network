<?php


use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


function imageUrl($imagePath = '', $width = null, $height = null, $config = array())
{
    // Available configs
    $quality = isset($config['quality']) ? $config['quality'] : 100;
    $crop = isset($config['crop']) ? !!($config['crop']) : true;

    $cacheTtl = (\Config::get('app.env') !== 'local') ? 60 : 0;
    $cacheKey = "imageResize/$imagePath/$width/$height/$quality/$crop";

    return Cache::remember($cacheKey, $cacheTtl, function () use ($imagePath, $height, $width, $quality, $crop) {
        $storage = Storage::disk(config('voyager.storage.disk'));

        // Don't continue when original file doesn't exist
        if (!($storage->exists($imagePath))) {
            return null;
        }

        // Setup the image URLs
        // - You can add ASSET_URL=http://... to your .env to reference images through a CDN
        $hostname = \Config::get('app.url');
        $urlPrefix = $hostname . '/storage/';

        // Return original image if height and width are 0
        if ((int)$width === null && (int)$height === null) {
            return $urlPrefix . $imagePath;
        }

        // Absolute path to full size image
        $storagePath = storage_path() . '/app/public/';

        // Create the new image path
        $splitAt = strrpos($imagePath, '/');
        $imageDir = substr($imagePath, 0, $splitAt);
        $imageName = substr($imagePath, $splitAt + 1);
        $resizedImagePath = "resized/" . $imageDir . "-$width" . "x$height/" . $imageName;

        // No need to continue if image already exists
        if ($storage->exists($resizedImagePath)) {
            return $urlPrefix . $resizedImagePath;
        }

        // Create the new image
        $resizedImage = Image::make($storagePath . $imagePath);

        // Crop/Resize always needs height AND width
        $width = empty($width) ? 1600 : $width;
        $height = empty($height) ? 1600 : $height;

        // Shall we crop or resize?
        if ($crop) {
            $resizedImage->fit((int)$width, (int)$height, function ($constraint) {
                $constraint->upsize();
            });
        } else {
            $resizedImage->resize((int)$width, (int)$height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }

        $resizedImage->encode('', (int)$quality);

        // And put it where it needs to go
        $storage->put($resizedImagePath, (string)$resizedImage, 'public');

        return $urlPrefix . $resizedImagePath;
    });
}


/**
 *  Get User Details
 * @return mixed
 */
function getUserDetail()
{	
	print_r('fdsfd');die;
}

function isArtist(App\User $user = null)
{	
	$checkUser = ($user ?: \Auth::user());
	return ($checkUser->is_artist) ? true : false;
}

function isUser(App\User $user = null)
{	
	$checkUser = ($user ?: \Auth::user());
	return (!$checkUser->is_artist && !$checkUser->is_publisher) ? true : false;
}

function isPublisher(App\User $user = null)
{	
	$checkUser = ($user ?: \Auth::user());
	return ($checkUser->is_publisher) ? true : false;
}

/**
 *  Check selected option
 * @return bool
 */
function checkSelected($str, $old = '', $val = '')
{	
	if(is_array($val)){	
		return in_array($str, old($old, $val)) ? 'selected' : '';
	}
	else
		return $str==old($old, $val) ? 'selected' : '';
}

/**
 *  get Manga list
 * @return bool
 */
function getMangaByRole()
{	
	if(isArtist() || isPublisher())
		return App\Models\Manga::where('user_id', \Auth::user()->id)->get();
	elseif(isUser())
		return App\Models\Manga::latest()->has('pages')->has('author')->where('status','Published')->get();
	else
		return App\Models\Manga::latest()->get();

}

/**
 *  get Manga Volume list
 * @return bool
 */
function getMangaVolumes($manga_id)
{	
	return App\Models\MangaVolume::where('manga_id', $manga_id)->orderBy('id')->get();
}

/**
 *  get Manga Chapters list
 * @return bool
 */
function getMangaChapters($manga_id,$volume_id=null)
{	
	if($volume_id)
		return App\Models\MangaChapter::where('manga_id', $manga_id)->where('manga_volume_id', $volume_id)->orderBy('id')->get();
	else
		return App\Models\MangaChapter::where('manga_id', $manga_id)->orderBy('id')->get();

}

/**
 *  get Manga Chapter Pages list
 * @return bool
 */
function getMangaPages($manga_id,$chapter_id)
{	
		return App\Models\MangaPage::where('manga_id', $manga_id)->where('manga_chapter_id', $chapter_id)->orderBy('page_no')->get();
}

/**
 *  get Category list
 * @return bool
 */
function getCategories()
{	
	return App\Models\Category::orderBy('name')->get();
}

/**
 *  get Artist list
 * @return bool
 */
function getArtists()
{	
	if(isAdmin())
		return App\Models\Artist::orderBy('name')->get();
	return App\Models\Artist::where('user_id',\Auth::user()->id)->orderBy('name')->get();
}



/**
 *  get manag release status list
 * @return bool
 */
function getManagReleaseStatus()
{	
	$data= ['Ongoing','Completed'];
	return $data;
}


/**
 *  get manag status list
 * @return bool
 */
function getManagStatus()
{	
	$data= ['New','Published','Rejected'];
	return $data;
}


/**
 *  get gender list
 * @return bool
 */
function getGender()
{	
	$data= ['Male','Female'];
	return $data;
}

/**
 *  get Manga Recent List
 * @return bool
 */
function getMangaRecent($count=null)
{	
	if(!$count)
		return App\Models\Manga::where('status','Published')->has('author')->has('pages')->latest()->get();
	else
		return App\Models\Manga::where('status','Published')->has('author')->has('pages')->latest()->take($count)->get();	

}

/**
 *  get Manga Recent List
 * @return bool
 */
function getMangaLatestChapter($count=null)
{	
	if(!$count)
		return App\Models\Manga::where('status','Published')->has('author')->has('pages')->has('chapters')->with(['pages' => function ($q) {
					  $q->latest(); // sorting related table, so we can use first on the collection
					}])->get();
	else
		return App\Models\Manga::where('status','Published')->has('author')->has('pages')->has('chapters')->with(['pages' => function ($q) {
					  $q->latest(); // sorting related table, so we can use first on the collection
					}])->take($count)->get();	

}

/**
 *  get Manga Recent List
 * @return bool
 */
function getMangaPopular($count=null)
{	
	if(!$count)
		return App\Models\Manga::where('status','Published')->has('author')->has('pages')->has('chapters')->with(['pages' => function ($q) {
					  $q->latest(); // sorting related table, so we can use first on the collection
					}])->get();
	else
		return App\Models\Manga::where('status','Published')->has('author')->has('pages')->has('chapters')->with(['pages' => function ($q) {
					  $q->latest(); // sorting related table, so we can use first on the collection
					}])->take($count)->get();	

}

/**
 *  get blog list
 * @return bool
 */
function getBlogs($count=null)
{	
	if(!$count)
		return App\Models\Post::published()->latest()->get();
	else
		return App\Models\Post::published()->latest()->take($count)->get();	

}

function checkFollowManga($manga_id)
{
	if(\Auth::check())
		return \App\Models\FollowManga::where('manga_id',$manga_id)->where('user_id',Auth::user()->id)->first();
	return false;
}

function checkFollowArtist($artist_id,$type)
{
	if(\Auth::check())
		return \App\Models\FollowArtist::where('artist_id',$artist_id)->where('type',$type)->where('user_id',Auth::user()->id)->first();
	return false;
}

function mangaRatings($manga_id)
{
	$ratingsValue=\App\Models\MangaRating::where('manga_id',$manga_id);
	return  ($ratingsValue->count()>0)?($ratingsValue->sum('rating')/$ratingsValue->count()):'0';
}

function getSlides()
{	
	return App\Models\Slider::orderBy('display_order')->get();

}

function getPagesCount($chapter_id){
	return App\Models\MangaPage::where('manga_chapter_id', $chapter_id)->count();
}

function getMangaPage($manga_id,$chapter_id,$page_no)
{	
	return App\Models\MangaPage::where('manga_id', $manga_id)->where('manga_chapter_id', $chapter_id)->where('page_no', $page_no)->latest()->first();
}

/**
  * Gera a paginação dos itens de um array ou collection.
  *
  * @param array|Collection      $items
  * @param int   $perPage
  * @param int  $page
  * @param array $options
  *
  * @return LengthAwarePaginator
  */
function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
