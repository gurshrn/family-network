<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    public function userdetail()
    {
    	return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function userphotos()
    {
    	return $this->belongsTo('App\Models\UserPhoto','user_photo_id','id');
    }
    public function uservideos()
    {
    	return $this->belongsTo('App\Models\UserVideo','user_video_id','id');
    }
    public function storymembers()
    {
    	return $this->hasMany('App\Models\StoryMember','story_id','id');
    }
    public function storycomments()
    {
        return $this->hasMany('App\Models\StoryComment','story_id','id');
    }
    public function storylikes()
    {
        return $this->hasMany('App\Models\StoryLike','story_id','id');
    }
    public function groupMembers()
    {
        return $this->hasMany('App\Models\GroupMember','group_id','group_id');
    }
}
