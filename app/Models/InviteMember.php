<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InviteMember extends Model
{
	public function user()
    {
    	return $this->belongsTo('App\Models\User','member_id','id');
    }
    
   
}
