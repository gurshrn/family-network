<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserConversation extends Model
{
    public function conversationmsg()
    {
    	return $this->hasMany('App\Models\UserConversationMessage','chat_id','id');
    }
}
