<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    public function members()
    {
    	return $this->hasMany('App\Models\GroupMember','group_id','id');
    }
   
    public function groupAdminDetail()
    {
    	return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function groupstory()
    {
    	return $this->hasMany('App\Models\Story','group_id','id');
    }
    public function groupconversation()
    {
    	return $this->hasMany('App\Models\GroupConversation','group_id','id');
    }
    public function userdetail()
    {
    	return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function invitedUsers()
    {
    	return $this->hasMany('App\Models\User','invited_by_id','user_id');
    }
    
    
}
