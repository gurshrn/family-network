<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    public function userrelation()
    {
    	return $this->belongsTo('App\Models\UserRelation','member_id');
    }
    public function userDetail()
    {
    	return $this->belongsTo('App\Models\User','member_id','id');
    }
    public function groupDetail()
    {
    	return $this->belongsTo('App\Models\UserGroup','group_id','id');
    }
}
