<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
       protected $fillable = [
           'name', 'email', 'password','dob','gender','role_id','invited_by_id'
       ];

       /**
        * The attributes that should be hidden for arrays.
        *
        * @var array
        */
       protected $hidden = [
           'password', 'remember_token',
       ];

           
 

   	public function getmembers()
   	{
   		return $this->hasMany('App\Models\InviteMember','user_id','id');
   	}
   	public function parentUser()
   	{
   		return $this->belongsTo('App\Models\User','invited_by_id','id');
   	}
   	public function userForum()
   	{
   		return $this->hasMany('App\Models\UserForum','user_id','id');
   	}
   
   	
}
