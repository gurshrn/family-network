<?php

namespace App\Models;
use Auth;
use DB;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    public function save(array $options = [])
    {
        $options = implode(",",$_POST['choose_plan_options']);
        $this->choose_plan_options = $options;
        $this->added_by = Auth::user()->id;
        parent::save();
    } 
    public function getSubscription()
    {
    	$result = DB::table('user_subscriptions')->get();
		return $result;
    }
}
