<?php

namespace App\Http\Controllers;
//use App\User;
use App\Story;
use App\Models\UserRelation;
use App\Models\User; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; 
use Auth;
class UserAjaxContoller extends Controller
{

	public function MyStoryPagination(Request $request)
	{
		$offset=$request->offset;  
		$limit=1;
		
		$offset = ( $offset - 1 ) * $limit;
		
		$obj=new User();  
		$objStory=new Story(); 
		$user=Auth::user();
		$userId = Auth::id();	
		$array=$obj->getUserDetail($userId);
		$resultStory=$objStory->GetUserStory($userId,$offset,$limit);
		
		$result=json_decode($resultStory);
		
		$data=array('user_id'=>$userId,'UserDetail'=>$array,'resultStory'=>$result->result,'total_record'=>$result->total_record);	
		return view('user.mystory_load_more',$data);		
		  
	}
	
	public function DashboardStoryPagination(Request $request)
	{
		$offset=$request->offset;  
		$limit=1;
		
		$offset = ( $offset - 1 ) * $limit;
		
		$obj=new Story(); 
		$objStory=new Story(); 
		$user=Auth::user();
		$userId = Auth::id();	
		$array=$obj->getUserDetail($userId);
		$resultStory=$objStory->GetAllUserStory($offset,$limit);
		
		$result=json_decode($resultStory);
		/* echo "<pre>";
		print_r($result);
		echo "</pre>"; */
		
		$data=array('user_id'=>$userId,'UserDetail'=>$array,'resultStory'=>$result->result,'total_record'=>$result->total_record);	
		
		return view('user.dashboard_story_load_more',$data);	
	}	
	
	
	public function ChangeUserImage(Request $request)
	{
		$userobj=new User();
		$user_id = Auth::user()->id; 	
		$input = $request->all();
		$input['profile_img'] = time().'.'.$request->profile_img->getClientOriginalExtension();
		$request->profile_img->move(storage_path('app/public/users'), $input['profile_img']);
		$array=array(
			'avatar'=>'users/'.$input['profile_img']	
		);  
		 
		$userobj->UpdateUserAccount($array,$user_id);
		echo "1";
		
	}	
	
	public function ChangeUserCoverImage(Request $request)
	{
		$userobj=new User();
		$user_id = Auth::user()->id; 	
		$input = $request->all();
		$input['profile_img'] = time().'.'.$request->profile_img->getClientOriginalExtension();
		$request->profile_img->move(storage_path('app/public/users'), $input['profile_img']);
		$array=array(
			'cover_img'=>'users/'.$input['profile_img']	
		);   
		 
		$userobj->UpdateUserAccount($array,$user_id);
		echo "1";
		
	}

	public function ShowAddMemberForm(Request $request)
	{
		$data = array('relation'=>$request->input('relation_type'),'memberId'=>$request->member_id);
		return view('user.add-member',$data);
	}

	public function AddMember(Request $request)
	{
		$userobj=new User();

		$user_id = Auth::user()->id;
		$user_rel_detail = $userobj->getUserRelId($user_id);
		$user_rel_id = $user_rel_detail->id;
		
		
		$gender = $request->gender;
		$memberType = lcfirst($request->member_type);
		
		if($memberType == 'daughter' || $memberType == 'son')
		{
			$level = -1;
		}
		if($memberType == 'mother' || $memberType == 'father')
		{
			$level = 1;
		}
		if($memberType == 'brother' || $memberType == 'sister')
		{
			$level = 0;
		}
		
		$rootUser=array(
			'user_id'=>$user_id,
			'first_name'=>$request->first_name,
			'last_name'=>$request->last_name,
			'relation'=>$memberType,
			'level'=>$level,
			'email_address'=>$request->email,
			'borndate'=>$request->birth_date,
			'bornplace'=>$request->birth_place,
			'live_dead'=>$request->living,
		);

		$root_id = $userobj->createrootuser($rootUser);

		if($root_id != 0 && $root_id != '')
		{
			if($memberType == 'daughter' || $memberType == 'son')
			{
				$userRelId = $root_id;
				$rel_id = $user_rel_id;
			}
			if($memberType == 'mother' || $memberType == 'father')
			{
				$userRelId = $user_rel_id;
				$rel_id = $root_id;
			}
			if($memberType == 'brother' || $memberType == 'sister')
			{
				$userRelId = $root_id;
				$rel_id = $user_rel_id;
			}

			$member_relation = array(
				'user_id'=>$userRelId,
				'rel_id'=>$rel_id
			);

			$member_id = $userobj->createMemberRelation($member_relation);

			$result['success'] = true;
			$result['msg'] = 'Member added successfully';
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Member not added successfully';
		}
		echo json_encode($result);exit;
	}  
	
}
