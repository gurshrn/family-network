<?php

namespace App\Http\Controllers\Auth;

//use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\InviteMember;
use App\Models\UserRelation;
use Auth;

class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'dob' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            //'password' => ['required', 'string', 'min:6', 'confirmed'],
            'gender' => ['required', 'string'],
            //'register_from' => ['required', 'string', 'min:6', 'confirmed'],
        ]); 
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data,$password)
    {
        return User::create([
            'name' => $data['first_name'].' '.$data['last_name'],
            'email' => $data['email'],
            'dob' =>date('Y-m-d',strtotime($data['dob'])),  
            'password' => Hash::make($password),
            'gender' =>$data['gender'],  
            //'register_from' =>$data['register_from'], 
            'role_id' => 2,
            'invited_by_id' => $data['invitedById']
 
        ]); 
    }
    public function register(Request $request)  
    {
        $data=$request->all();
        $length=8;
        $validation = $this->validator($request->all());
        if ($validation->fails())  
        {  
            $getEmailVal = $validation->errors()->toArray();
            $emailVal = $getEmailVal['email'][0];
            return response()->json(['success' => 'false',
                        'msg' => $emailVal]);
        }
        else
        {
            $length = 8;
            $password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

            $user = $this->create($request->all(),$password);
            \Auth::attempt(["email"=>$data['email'],"password"=>$password]);
            $rootUser = new UserRelation();

            
            $rootUniqueId = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

            $rootUser->user_id = $user->id;
            $rootUser->first_name = $data['first_name'];
            $rootUser->last_name = $data['last_name'];
            $rootUser->unique_id = $rootUniqueId;
            $rootUser->relation = 0;
            $rootUser->relation_with = 0;
            $rootUser->borndate = date('Y-m-d',strtotime($data['dob']));
            $rootUser->live_dead = 'Living';
            $rootUser->email_address = $data['email'];            
            $rootUser->save();

            $fatherRel = new UserRelation();

            $fatherUniqueId = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

            $fatherRel->user_id = $user->id;
            $fatherRel->first_name = $data['father_first_name'];
            $fatherRel->last_name = $data['father_last_name'];
            $fatherRel->unique_id = $fatherUniqueId;
            $fatherRel->relation = 'Father';
            $fatherRel->gender = 'male';
            $fatherRel->relation_with = $rootUser->id;

            $fatherRel->save();

            $motherRel = new UserRelation();

            $motherUniqueId = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

            $motherRel->user_id = $user->id;
            $motherRel->first_name = $data['mother_first_name'];
            $motherRel->last_name = $data['mother_last_name'];
            $motherRel->unique_id = $motherUniqueId;
            $motherRel->relation = 'Mother';
            $motherRel->gender = 'female';
            $motherRel->relation_with = $rootUser->id;
            
            $motherRel->save();

            $parentsId = $fatherRel->id.','.$motherRel->id;

            UserRelation::where('id', $rootUser->id)->update(['parent' => $parentsId]); 

            $email=$data['email'];

            $email_data = array('password'=>$password);

            \Mail::send('mail', $email_data, function ($message) use($email){
            //
                $message->subject('Family Network Register');
                $message->to($email);
            });

            if($user->id != 0 && $user->id != '')
            {
                if($data['invitedById'] != '')
                {
                    $getInvitedBy = InviteMember::where('user_id',$data['invitedById'])->where('member_id',$data['memberId'])->orderBy('id','DESC')->first();
                    $updateStatus = array('status'=>'Accepted','member_id'=>$user->id);
                    InviteMember::where('user_id',$data['invitedById'])->where('member_id',$data['memberId'])->update($updateStatus);

                }
                $result['success'] = true;
                $result['msg'] = 'Register successfully';
                $result['url'] = url('/dashboard');
            }
            else
            {
                $result['success'] = false;
                $result['msg'] = 'Register not successfully';
            }

            //if (Auth::user()){
            return response()->json($result);
            //}
        }  
    }
}
