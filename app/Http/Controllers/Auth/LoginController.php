<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers; 

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';  
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest')->except('logout');
    }
    public function index() 
    {
        if(!\Auth::check() ){
            if(Page::where('slug',substr(parse_url(\URL::previous())['path'],1))->count()>0)
                return redirect()->back()->with('login','1');
            else
                 return redirect('/')->with('login','1');
            //return view('auth.login');
        } else {
            return redirect('/');
        }

     }

    public function login(Request $request) 
    {
        $login = $request->input('email');

        $login_type = filter_var( $login, FILTER_VALIDATE_EMAIL ) ? 'email' : 'username';

        $request->merge([ $login_type => $login ]);

        if ( $login_type == 'email' ) 
        {
            $this->validate($request, [
                'email'    => 'required|email',
                'password' => 'required',
            ]);

            $credentials = $request->only( 'email', 'password' );

        } 

        if( $this->auth->attempt($credentials, $request->has('remember')) )
        {
            return response()->json(['success' => 'true',
                        'message' => 'Logged in successfully.']);
        }

        return response()->json([ 'success' => false,
                    'message' => 'These credentials do not match our records.']);
    }

}
