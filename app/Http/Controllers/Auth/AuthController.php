<?php


namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\UserRelation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Auth;
use Socialite;

class AuthController extends Controller
{
    // Some methods which were generated with the app
    
    /**
     **_ Redirect the user to the OAuth Provider.
     _**
     **_ @return Response
     _**/
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();

    }

    /**
     _ Obtain the user information from provider.  Check if the user already exists in our
     _ database by looking up their provider_id in the database.
     _ If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     _ redirect them to the authenticated users homepage.
     _
     _ @return Response
     _/
    

    /**
     _ If a user has registered before using social auth, return the user
     _ else, create a new user object.
     _ @param  $user Socialite user object
     _ @param $provider Social auth provider
     _ @return  User
     */
    
    public function handleProviderCallback($provider)
    {

        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true); 
        return redirect('/dashboard'); 
    }     
     
     
    public function findOrCreateUser($user, $provider)
    {
       
        $userobj=new User();
        $authUser = User::where('email', $user->email)->first();
        if ($authUser) 
        {
            return $authUser;
        }

        $length = 8;
        $password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
         
        $user=User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'password' => Hash::make($password),
            'provider' => $provider, 
            'provider_id' => $user->id
        ]); 

        $email = $user->email;

		
		$userinfo=json_decode($user);
		$user_id=$userinfo->id;
		
		$userRelation = new UserRelation();
		
		$userRelation->user_id = $user_id;
		$userRelation->first_name = $user->name;
		$userRelation->last_name = $user->name;
		$userRelation->email_address = $user->email;
		$userRelation->relation = 0;
		$userRelation->save();

        $data = array('password'=>$password);

        \Mail::send('mail', $data, function ($message) use($email){
            //
            $message->subject('Family Network Register');
            $message->to($email);
        });
		
		return $user;  
	} 

}    
?>