<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRelation;
use App\Story;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Auth; 
use Mail;
class UserController extends Controller
{
    
/*=============Show Register view===============*/

    public function ShowUserRegisterForm(Request $request)
	{
		if(!Auth::user())
		{
			if($request->all())
			{
				$getMemberDetail = UserRelation::where('unique_id',$request->userRefId)->first();
				
				$email = $getMemberDetail->email_address;
				$checkUser = User::where('email',$email)->first();
				if(count($checkUser) == 0)
				{
					$data = array(
						'email'=>$getMemberDetail->email_address,
						'first_name'=>$getMemberDetail->first_name,
						'last_name'=>$getMemberDetail->last_name,
						'invitedById'=>$request->invitedById,
						'memberId'=>$request->userRefId,
						'id'=>$getMemberDetail->id,
					);
				}
			}
			else
			{
				$data =array();
			}
			return view('user.registeruser',['memberDetail'=>$data]);  
		}
	}

/*=============Register User===============*/

	public function registerUser(Request $request)
	{

		$userobj=new User();
		
		$email=$request->email;
		$first_name=$request->first_name;
		$last_name=$request->last_name;
		$name=$first_name.' '.$last_name;
        $password='password';
		$email_exist=User::where('email',$email)->count();
		
		if($email_exist > 0) 
		{
			$result['success'] = false;
			$result['msg'] = 'Email already exist';
		} 
		else 
		{
			$length = 8; 

			$userobj->name = $name;
			$userobj->email = $email;
			$userobj->dob = date('Y-m-d',strtotime($request->input('dob')));
			$userobj->password = Hash::make($password);
			$userobj->gender = $request->input('gender');
			$userobj->role_id = 2;
			$userobj->invited_by_id = $request->input('invitedById');

			$userobj->save();

			$rootUser = new UserRelation();

			
			$rootUniqueId = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

			$rootUser->user_id = $userobj->id;
			$rootUser->first_name = $first_name;
			$rootUser->last_name = $last_name;
			$rootUser->gender = $request->input('gender');
			$rootUser->unique_id = $rootUniqueId;
			$rootUser->relation = 0;
			$rootUser->relation_with = 0;
			$rootUser->borndate = date('Y-m-d',strtotime($request->input('dob')));
			$rootUser->live_dead = 'Living';
			$rootUser->email_address = $email;
			
			
			$rootUser->save();

			$fatherRel = new UserRelation();

			$fatherUniqueId = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

			$fatherRel->user_id = $userobj->id;
			$fatherRel->first_name = $request->father_first_name;
			$fatherRel->last_name = $request->father_last_name;
			$fatherRel->unique_id = $fatherUniqueId;
			$fatherRel->relation = 'Father';
			$fatherRel->gender = 'male';
			$fatherRel->relation_with = $rootUser->id;

			$fatherRel->save();

			$motherRel = new UserRelation();

			$motherUniqueId = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

			$motherRel->user_id = $userobj->id;
			$motherRel->first_name = $request->mother_first_name;
			$motherRel->last_name = $request->mother_last_name;
			$motherRel->unique_id = $motherUniqueId;
			$motherRel->relation = 'Mother';
			$motherRel->gender = 'female';
			$motherRel->relation_with = $rootUser->id;
			
			$motherRel->save();

			$parentsId = $fatherRel->id.','.$motherRel->id;

			UserRelation::where('id', $rootUser->id)->update(['parent' => $parentsId]);
			\Mail::send('mail', array(), function ($message) use($email){
            //
	            $message->subject('Family Network Register');
	            $message->to($email);
        	});
            Auth::attempt(['email' => $email, 'password' => $password], 'false');
          	if($userobj->id != 0 && $userobj->id != '')
          	{
          		$result['success'] = true;
          		$result['msg'] = 'Register successfully';
          	}
          	else
          	{
          		$result['success'] = false;
          		$result['msg'] = 'Register not successfully';
          	}
          	
		}  
		echo json_encode($result);exit;
	}

/*==============User my account============*/
	
	public function myAccount(Request $request)
	{
		$userId = Auth::user()->id;	

		if($request->isMethod('post'))
		{

			$array=array(
				'name'=>$request->name,	
				'dob'=>date('Y-m-d',strtotime($request->dob)),	
				'email'=>$request->email,	
				'bio_title'=>$request->bio_title,	
				'bio_short_description'=>$request->bio_short_description,	
				'bio_description'=>$request->bio_description,	
			); 

			
			$userId = User::where('id', $userId)->update($array);
			if($userId)
			{
				$result['success'] = true;
				$result['msg'] = 'Profile updated successfully';
			}
			else
			{
				$result['success'] = false;
				$result['msg'] = 'Profile not updated successfully';
			}
			echo json_encode($result);exit;
		} 
		$array = User::where('id',$userId)->first();
		$data  = array('user_id'=>$userId,'UserDetail'=>$array);
		return view('user.myaccount',$data);
	}

/*================Change timeline image==============*/	

	public function changeUserCoverImage(Request $request)
	{
		$userobj = new User();
		$user_id = Auth::user()->id; 	
		$input = $request->all();
		$profile_img = time().'.'.$request->profile_img->getClientOriginalExtension();
		$request->profile_img->move(public_path('upload/profileimages'), $profile_img);

		$data = array('cover_img'=>$profile_img);

		User::where('id',$user_id)->update($data);
		echo "1";
		
	} 

/*================Change profile image from story==============*/	

	public function ChangeUserImage(Request $request)
	{
		$userobj = new User();
		$user_id = Auth::user()->id; 	
		$input = $request->all();
		$profile_img = time().'.'.$request->profile_img->getClientOriginalExtension();
		$request->profile_img->move(public_path('upload/profileimages'), $profile_img);
		$data=array(
			'avatar'=> $profile_img	
		);  
		 
		User::where('id',$user_id)->update($data);
		echo "1";
		
	}	

/*============Change Password============*/

	public function changePassword(Request $request)
	{
		$userId = Auth::user()->id;	
		if($request->isMethod('post'))
		{
			$current_password = Auth::User()->password;           
		    if(Hash::check($request->current_password, $current_password))
		    { 
		    	if(Hash::check($request->new_password, $current_password))
		    	{
		    		$result['success'] = false;
					$result['msg'] = 'You have entered current password';
		    	}  
		    	else
		    	{
		    		$obj_user = User::find($userId);
			        $obj_user->password = Hash::make($request['new_password']);
			        $obj_user->save(); 
			        $result['success'] = true;
					$result['msg'] = 'Password changed successfully';
		    	}        
		    }
			else
			{
				$result['success'] = false;
				$result['msg'] = 'Current password wrong';
			}
			echo json_encode($result);exit;
		}
		return view('user.change-password');
	}

	public function forgotPassword(Request $request)
	{
		$userDetail = User::where('email',$request->email)->first();
		if(count($userDetail) > 0)
		{
			$encyptedId = Crypt::encryptString($userDetail->id);
			$forgot_url = url('/?userId='.$encyptedId);
			$email = $userDetail->email;
			$data = array('url'=>$forgot_url);
			\Mail::send('forgot-password-mail', $data, function ($message) use($email){
            //
	            $message->subject('Forgot Password');
	            $message->to($email);
        	});

			$result['success'] = true;
			$result['msg'] = 'Kindly check your email to reset the password';
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Invalid Email';
		}
		echo json_encode($result);exit;
	}

	public function resetPassword(Request $request)
	{
		$userId = Crypt::decryptString($request->userId);
		$obj_user = User::find($userId);
        $obj_user->password = Hash::make($request['new_password']);
        $obj_user->save(); 
        $result['success'] = true;
		$result['msg'] = 'Password changed successfully';
		$result['url'] = url('/');
		echo json_encode($result);exit;
	}

	public function myStory()
	{

		$offset=0;
		$limit=1;
		$obj=new Story(); 
		$objStory=new Story(); 
		$user=Auth::user();
		$userId = Auth::id();	
		$array=$obj->getUserDetail($userId);
		$resultStory=$objStory->GetUserStory($userId,$offset,$limit);

		$result=json_decode($resultStory);

		$data=array('user_id'=>$userId,'UserDetail'=>$array,'resultStory'=>$result->result,'total_record'=>$result->total_record);	
		return view('user.mystory',$data);		
	}	 
	
	
	






	
	
}
