<?php

namespace App\Http\Controllers;
use App\Models\UserGroup;
use App\Models\UserRelation;
use App\Models\GroupMember;
use App\Models\UserRelationParent;
use App\Models\UserRelationPartner;
use App\Models\User;
use App\Models\Story;
use App\Models\StoryMember;
use App\Models\UserPhoto;
use App\Models\UserVideo;
use App\Models\UserForum;
use App\Models\ForumComment;
use App\Models\StoryComment;
use App\Models\InviteMember;
use App\Models\UserQuestion;
use App\Models\ForumCategory;
use App\Models\StoryLike;
use App\Models\UserConversation;
use App\Models\UserConversationMessage;
use App\Models\GroupConversation;
use App\Models\UserNotification;
use Auth; 
use Mail;
use Helper;
use DB;

use Illuminate\Http\Request;


class DashboardController extends Controller
{

/*==============Dashboard=============*/

	public function getUserNotification()
	{
		if(Auth::user()) 
		{
			$userId=Auth::user()->id;
			$getNotifications = UserNotification::where('user_id',$userId)->orderBy('id','DESC')->get();
			return view('user.notifications',['getNotifications'=>$getNotifications]);
		}
	}

	public function updateNotificationStatus()
	{
		$id = $_GET['id'];
		UserNotification::where('id',$id)->update(['status'=>1]);
	}

	public function testMail()
	{
		 \Mail::send('welcome', array(), function ($message){
            //
            $message->subject('Test Mail By Family Network');
            $message->to('neetu.rani@imarkinfotech.com');
        });
	}

/*=================User Dashboard================*/

	public function dashboard()
	{
		if(Auth::user()) 
		{
			$userId=Auth::user()->id;
			$offset=0;
			$limit=1;

			$userStory  = Story::select('stories.*','story_members.id as storyIds')->leftJoin('story_members','story_members.story_id','=','stories.id')->leftJoin('group_members','group_members.group_id','=','stories.group_id')->where('story_members.member_id',$userId)->orWhere('stories.user_id',$userId)->orWhere('group_members.user_id',$userId)->orWhere('group_members.member_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->orderBy('stories.id', 'desc')->offset($offset)->limit($limit)->get();

			
			$totalRecords = Story::select('stories.*','story_members.id as storyIds')->leftJoin('story_members','story_members.story_id','=','stories.id')->leftJoin('group_members','group_members.group_id','=','stories.group_id')->where('story_members.member_id',$userId)->orWhere('stories.user_id',$userId)->orWhere('group_members.user_id',$userId)->orWhere('group_members.member_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->orderBy('stories.id', 'desc')->get();


			
			$data=array('userStory'=>$userStory,'total_record'=>count($totalRecords));	
			return view('user.dashboard',$data);
		}
		else
		{
			return redirect('/');
		}
	}

/*=============User my stories==============*/

	public function myStory()
	{

		if(Auth::user()) 
		{
			$userId=Auth::user()->id;
			$offset=0;
			$limit=1;
			$keyword = '';
			if(isset($_GET['keyword']))
			{
				$userStory  = Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->where('title','LIKE', '%' . $_GET['keyword'] .'%')->orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
				$totalRecords  = Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->where('title','LIKE', '%' . $_GET['keyword'] .'%')->orderBy('id', 'desc')->get();
				$keyword = $_GET['keyword'];
			}
			else
			{
				$userStory  = Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
				
				$totalRecords = Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->orderBy('id', 'desc')->get();
			}
			
			$data=array('userStory'=>$userStory,'totalRecords'=>count($totalRecords),'keyword'=>$keyword);	
			return view('user.mystory',$data);
		}
	}

/*==============Story Detail when click on story=============*/

	public function storyDetail($story_id)
	{
		$userId=Auth::user()->id;
		
		$userStory  = Story::where('id',$story_id)->with(['userdetail','userphotos','uservideos','storymembers','storylikes'])->first();
		$storyComment = StoryComment::where('story_id',$story_id)->with('user')->orderBy('created_at','desc')->get();
		$userStoryLike = StoryLike::where('story_id',$story_id)->where('user_id',$userId)->first();
		$totalRecords = count($userStory);
		
		$data=array('userStoryLike'=>$userStoryLike,'storyId'=>$story_id,'userStory'=>$userStory,'total_record'=>$totalRecords,'storyComment'=>$storyComment);	
		return view('user.storydetail',$data);			 	
	}	

/*==============Add Story=============*/

	public function createStory(Request $request)
	{
		if(Auth::user()) 
		{
			$userId=Auth::user()->id;
			
			if($request->isMethod('post'))
			{
				$membersId = $request->input('member_list');
				if ($request->hasFile('user_photo')) 
			 	{
			 		$userPhoto = new UserPhoto();

			 		$user_photo = time().'.'.$request->file('user_photo')->getClientOriginalExtension();
		    		$request->file('user_photo')->move(public_path('upload/userphotos'), $user_photo);
					$userPhoto->user_id = $userId;
		    		$userPhoto->user_photo = $user_photo;
		    		$userPhoto->save();
		    	}
				
				if ($request->hasFile('user_video')) 
			 	{
			 		$userVideo = new UserVideo();

			 		$user_video = time().'.'.$request->file('user_video')->getClientOriginalExtension();
			 		$request->file('user_video')->move(public_path('upload/uservideos'), $user_video);
			 		$userVideo->user_id = $userId;
		    		$userVideo->user_video = $user_video;
		    		$userVideo->save();
		    	}
				
				$story = new Story();

				$story->user_id = $userId;
				$story->group_id = $request->group_id;
				$story->title = $request->title;
				$story->excerpt = $request->excerpt;
				$story->description = $request->description;
				if(isset($userPhoto->id))
				{
					$story->user_photo_id = $userPhoto->id;
				}
				if(isset($userVideo->id))
				{
					$story->user_video_id = $userVideo->id;
				}
				
				$storyId = $story->save();


				if($request->group_id != '')
				{

					$getGroupMembers = GroupMember::where('group_id',$request->group_id)->get();
					foreach($getGroupMembers as $grpMem)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $grpMem->member_id;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Story';
						$notifyData->notification = 'New story '.$request->title.' added by '.Auth::user()->name.' under group';
						$notifyData->table_id = $story->id;
						$notifyData->save();
					}
				}

				if($membersId != '')
				{
					foreach($membersId as $memId)
					{
						$storyMember = new StoryMember();
						
						$storyMember->story_id = $story->id;
						$storyMember->member_id = $memId;
						$storyMember->save();

						$notifyData = new UserNotification();
						$notifyData->user_id = $memId;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Story';
						$notifyData->notification = 'New story '.$request->title.' added by '.Auth::user()->name;
						$notifyData->table_id = $story->id;
						$notifyData->save();
					}
				}

				if($storyId != '' && $storyId != 0)
				{
					$result['success'] = true;
					$result['msg'] = 'Story published successfully';
					$result['url'] = '/my-story';
				}
				else
				{
					$result['success'] = false;
					$result['msg'] = 'Story not published successfully';
				}
				echo json_encode($result);exit;
			}
			$userRel = User::where('invited_by_id','!=','')->orderby('id','DESC')->get();
			$invitedId = array();
			$invitedUser = array();
			foreach($userRel as $val)
			{
				$invitedId[$val->id] = explode(",",$val->invited_by_id);
				if (in_array($userId, $invitedId[$val->id]))
				{
					$invitedUser[] = User::where('id',$val->id)->orderby('id','DESC')->get();
				}

			}
			$data=array('user_id'=>$userId,'userRel'=>$invitedUser);
			return view('user.createstory',$data);
		}
	}

/*==============Edit Story=============*/

	public function editStory(Request $request)
	{
		$userId=Auth::user()->id;
		if($request->isMethod('post'))
		{
			$membersId = $request->input('member_list');
			if ($request->hasFile('user_photo')) 
		 	{
		 		$userPhoto = new UserPhoto();

		 		$user_photo = time().'.'.$request->file('user_photo')->getClientOriginalExtension();
	    		$request->file('user_photo')->move(public_path('upload/userphotos'), $user_photo);
	    		if($request->user_photo_id != '')
	    		{
	    			UserPhoto::where('id',$request->user_photo_id)->update(['user_photo'=>$user_photo]);
	    		}
	    		else
	    		{
	    			$userPhoto->user_id = $request->user_id;
		    		$userPhoto->user_photo = $user_photo;
		    		$userPhoto->save();
	    		}
	    	}
			if ($request->hasFile('user_video')) 
		 	{
		 		$userVideo = new UserVideo();

		 		$user_video = time().'.'.$request->file('user_video')->getClientOriginalExtension();
		 		$request->file('user_video')->move(public_path('upload/uservideos'), $user_video);
		 		if($request->user_video_id != '')
	    		{
	    			UserVideo::where('id',$request->user_video_id)->update(['user_video'=>$user_video]);
	    		}
	    		else
	    		{
	    			$userVideo->user_id = $request->user_id;
		    		$userVideo->user_video = $user_video;
		    		$userVideo->save();
	    		}
		 	}

		 	$data = array(
		 		'title'=>$request->title,
		 		'excerpt'=>$request->excerpt,
		 		'description'=>$request->description,

		 	);
			
			if($request->user_photo_id == '' && $request->hasFile('user_photo'))
			{
				$data['user_photo_id'] = $userPhoto->id;
			}
			if($request->user_video_id == '' && $request->hasFile('user_video'))
			{
				$data['user_video_id'] = $userVideo->id;
			}
			
			$storyId = Story::where('id',$request->story_id)->update($data);
			if($storyId != 0 && $storyId != '')
			{
				$result['success'] = true;
				$result['msg'] = 'Story updated successfully';
				$result['url'] = '/my-story';
			}
			else
			{
				$result['success'] = true;
				$result['msg'] = 'Story not updated successfully';
			}
			echo json_encode($result);exit;
		}
		$storyId = $_GET['storyId'];
		$userRel = User::where('invited_by_id','!=','')->orderby('id','DESC')->get();
		$invitedId = array();
		$invitedUser = array();
		foreach($userRel as $val)
		{
			$invitedId[$val->id] = explode(",",$val->invited_by_id);
			if (in_array($userId, $invitedId[$val->id]))
			{
				$invitedUser[] = User::where('id',$val->id)->orderby('id','DESC')->get();
			}

		}
		$storyDetail = Story::where('id',$storyId)->with(['userphotos','uservideos','storymembers'])->first();
		return view('user.edit-story',['storyDetail'=>$storyDetail,'userRel'=>$invitedUser]);
		
	}

/*==========Delete user story==========*/

	public function deleteUserStory()
	{
		$storyId = $_GET['storyId'];

		Story::where('id',$storyId)->delete();
		StoryMember::where('story_id',$storyId)->delete();
		echo 1;
	}

/*==========Add comment for user story==========*/

	public function addStoryComment(Request $request)
	{
		$userId = Auth::id();
		$comment = new StoryComment();
		$comment->user_id = $userId;
		$comment->story_id = $request->story_id;
		$comment->comment = $request->story_comment;
		$comment->save();
		if($comment->id != '' && $comment->id != 0)
		{
			$getStoryMember = Story::where('id',$request->story_id)->with('storymembers','groupMembers')->first();
			if($getStoryMember->group_id != '')
			{
				if(count($getStoryMember->groupMembers) > 0)
				{
					foreach($getStoryMember->groupMembers as $val)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $val->member_id;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Comment';
						$notifyData->notification = 'New comment added by '.Auth::user()->name.' under group story '.$getStoryMember->title;
						$notifyData->table_id = $getStoryMember->id;
						$notifyData->save();
					}
				}

			}
			else
			{
				if(count($getStoryMember->storymembers) > 0)
				{
					foreach($getStoryMember->storymembers as $storyVal)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $storyVal->member_id;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Comment';
						$notifyData->notification = 'New comment added by '.Auth::user()->name.' under story '.$getStoryMember->title;
						$notifyData->table_id = $getStoryMember->id;
						$notifyData->save();
					}
				}
			}

			$commentDetail = StoryComment::where('id',$comment->id)->with('user')->first();
			$totalComment = StoryComment::where('story_id',$request->story_id)->get();
			$data = '';
			$data .= '<div class="forum-comment-bx"><figure><img src="'.url('/upload/profileimages/'.$commentDetail->user['avatar']).'" alt="user-sml-img1"></figure><div class="forum-txt"><h6>'.$commentDetail->user['name'].'<small><time class="timeago">'.$commentDetail->created_at->diffForHumans().'</time></small></h6><p>'.ucfirst($commentDetail->comment).'</p></div></div>';
			$result['html'] = $data;
			$result['count'] = count($totalComment);
			echo json_encode($result);exit;
		}
	}

/*==========Like user story==========*/

	public function likeUserStory(Request $request)
	{
		$userId = Auth::id();
		$likes = new StoryLike();
		$storyId = $request->storyId;
		$getUserLikes = StoryLike::where('user_id',$userId)->where('story_id',$storyId)->get();
		$getStoryLikes = StoryLike::where('story_id',$storyId)->get();
		$getStoryMember = Story::where('id',$storyId)->with('storymembers','groupMembers')->first();
		if(count($getUserLikes) > 0)
		{
			
			if($getStoryMember->group_id != '')
			{
				if(count($getStoryMember->groupMembers) > 0)
				{
					foreach($getStoryMember->groupMembers as $val)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $val->member_id;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Story';
						$notifyData->notification = 'Group Story '.$getStoryMember->title.' dis-like by '.Auth::user()->name;
						$notifyData->table_id = $getStoryMember->id;
						$notifyData->save();
					}
				}

			}
			else
			{
				if(count($getStoryMember->storymembers) > 0)
				{
					foreach($getStoryMember->storymembers as $storyVal)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $storyVal->member_id;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Story';
						$notifyData->notification = 'Story '.$getStoryMember->title.' dis-like by '.Auth::user()->name;
						$notifyData->table_id = $getStoryMember->id;
						$notifyData->save();
					}
				}
			}
			StoryLike::where('user_id',$userId)->where('story_id',$storyId)->delete();
			$result['success'] = false;
			$result['msg'] = 'Story dislike successfully';
			$result['likes'] = count($getStoryLikes);
		}
		else
		{
			if($getStoryMember->group_id != '')
			{
				if(count($getStoryMember->groupMembers) > 0)
				{
					foreach($getStoryMember->groupMembers as $val)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $val->member_id;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Like';
						$notifyData->notification = 'Group Story '.$getStoryMember->title.' like by '.Auth::user()->name;
						$notifyData->table_id = $getStoryMember->id;
						$notifyData->save();
					}
				}

			}
			else
			{
				if(count($getStoryMember->storymembers) > 0)
				{
					foreach($getStoryMember->storymembers as $storyVal)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $storyVal->member_id;
						$notifyData->sender_id = $userId;
						$notifyData->model_name = 'Like';
						$notifyData->notification = 'Group Story '.$getStoryMember->title.' like by '.Auth::user()->name;
						$notifyData->table_id = $getStoryMember->id;
						$notifyData->save();
					}
				}
			}
			$likes->user_id = $userId;
			$likes->story_id = $storyId;
			$likes->like = 1;
			$likes->save();
			$result['success'] = true;
			$result['msg'] = 'Story liked successfully';
			$result['likes'] = count($getStoryLikes);
		}
		echo json_encode($result);exit;
		
	}

	public function myStoryPagination(Request $request)
	{
		$userId=Auth::user()->id;
		$offset=$request->offset;
		$limit=1;
		$offset = ( $offset - 1 ) * $limit;
		$userStory  = Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->where('title','LIKE', '%' . $request->keyword)->orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
		
		$totalRecords =  Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->where('title','LIKE', '%' . $request->keyword)->orderBy('id', 'desc')->get();
		
		$data=array('userStory'=>$userStory,'totalRecords'=>count($totalRecords));	
		return view('user.mystory_load_more',$data);
	}

	public function dashboardStoryPagination(Request $request)
	{
		$userId=Auth::user()->id;
		$offset=$request->offset;
		$limit=1;

		$offset = ( $offset - 1 ) * $limit;
		
		$userStory  = Story::select('stories.*','story_members.id as storyIds')->leftJoin('story_members','story_members.story_id','=','stories.id')->leftJoin('group_members','group_members.group_id','=','stories.group_id')->where('story_members.member_id',$userId)->orWhere('stories.user_id',$userId)->orWhere('group_members.user_id',$userId)->orWhere('group_members.member_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->orderBy('stories.id', 'desc')->offset($offset)->limit($limit)->get();
		
		$totalRecords = Story::select('stories.*','story_members.id as storyIds')->leftJoin('story_members','story_members.story_id','=','stories.id')->leftJoin('group_members','group_members.group_id','=','stories.group_id')->where('story_members.member_id',$userId)->orWhere('stories.user_id',$userId)->orWhere('group_members.user_id',$userId)->orWhere('group_members.member_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers','storycomments','storylikes'])->orderBy('stories.id', 'desc')->get();

		$data=array('userStory'=>$userStory,'totalRecords'=>count($totalRecords));	
		
		return view('user.dashboard_story_load_more',$data);	
	}	


/*=============Add story photos===============*/

	public function addPhotos(Request $request)
	{
		$userId=Auth::user()->id;

		if($request->isMethod('post'))
		{
			if ($request->hasFile('user_photo')) 
		 	{
		 		$i=0;
		 		
		 		foreach($request->file('user_photo') as $val)
		 		{
		 			$userPhoto = new UserPhoto();
		 			$user_photo = time().'_'.$i.'.'.$val->getClientOriginalExtension();
		 			$val->move(public_path('upload/userphotos'), $user_photo);
	        		$userPhoto->user_id = $userId;
		    		$userPhoto->user_photo = $user_photo;
		    		$userPhoto->save();
		    		$i++;
		 		}
		 		echo 1;
        	}
		}
		else
		{
			$userPhotos = UserPhoto::where('user_id',$userId)->orderBy('id','DESC')->get();
			$data = array('userPhotos'=>$userPhotos);
			return view('user.userphotos',$data);
		}
	}

/*=============Add story videos===============*/

	public function addVideos(Request $request)
	{

		$userId=Auth::user()->id;

		if($request->isMethod('post'))
		{
			if ($request->hasFile('user_video')) 
		 	{
		 		$i=0;
		 		
		 		foreach($request->file('user_video') as $val)
		 		{
			 		$userVideo = new UserVideo();

			 		$user_video = time().'_'.$i.'.'.$val->getClientOriginalExtension();
			 		$val->move(public_path('upload/uservideos'), $user_video);
			 		$userVideo->user_id = $userId;
		    		$userVideo->user_video = $user_video;
		    		$userVideo->save();
		    		$i++;
		    	}
		    	echo 1;
			}
		}
		else
		{
			$userVideos = UserVideo::where('user_id',$userId)->get();
			$data = array('userVideos'=>$userVideos);
			return view('user.uservideos',$data);
		}
	}

/*=============Show User Tree=============*/

	public function userTree()
	{
		if(Auth::user()) 
		{
			$userId = Auth::user()->id;
			$invitedById = Auth::user()->invited_by_id;

			$rootarray = UserRelation::where('relation','0')->where('user_id',$userId)->get();
			$userTree  = UserRelation::where('relation','!=','0')->where('user_id',$userId)->get();
			$myTree=array('root_user'=>$rootarray,'userTree'=>$userTree,'tree_name'=>'My Tree');
			$invitedByTree = array();
			if($invitedById != '')
			{
				$getInvitedId = explode(",",$invitedById);
				foreach($getInvitedId as $val)
				{
					$invitedUserDetail = User::where('id',$val)->first();
					$invitedByRootArray = UserRelation::where('relation','0')->where('user_id',$val)->get();
					$invitedByUserTree = UserRelation::where('relation','!=','0')->where('user_id',$val)->get();
					$tree_name = $invitedUserDetail->name;
					$invitedByTree[] = array('root_user'=>$invitedByRootArray,'userTree'=>$invitedByUserTree,'tree_name'=>$tree_name);
				}
				
			}

			
			return view('user.tree',compact('myTree','invitedByTree'));	
		}		
	}

/*=============Add Members for tree=============*/

	public function addMember(Request $request)
	{
		
		$user_id = Auth::user()->id;
		
		$userRelation = new UserRelation();

		$checkEmail = UserRelation::where('email_address',$request->email)->get();

		$relWithId = $request->member_id;


		$memberType = lcfirst($request->member_type);

		$length = 8;
		$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		
		$userRelation->user_id = $user_id;
		$userRelation->unique_id = $randomString;
		$userRelation->first_name = $request->first_name;
		$userRelation->last_name = $request->last_name;
		$userRelation->relation = $memberType;
		$userRelation->gender = $request->gender;
		$userRelation->relation_with = $relWithId;
		$userRelation->email_address = $request->email;
		$userRelation->borndate = date('Y-m-d',strtotime($request->birth_date));
		$userRelation->bornplace = $request->birth_place;
		$userRelation->live_dead = $request->living;

		if ($request->hasFile('profile_image')) 
	 	{
	 		$user_photo = time().'.'.$request->file('profile_image')->getClientOriginalExtension();
    		$request->file('profile_image')->move(public_path('upload/profileimages'), $user_photo);
    		$userRelation->profile_image = $user_photo;
		}
        
        $userRelation->save();
		
		$rootId = $userRelation->id;

		if($rootId != '' && $rootId != 0)
		{
			if($memberType == 'mother' || $memberType == 'father')
			{
				$userDetail = UserRelation::where('id', $relWithId)->first();
				$userParent = $userDetail->parent;
				if($userParent != '')
				{
					$parentId = $userParent.','.$rootId;
				}
				else
				{

					$parentId = $rootId;
				}
				UserRelation::where('id', $relWithId)->update(['parent' => $parentId]);
			}
			if($memberType == 'daughter' || $memberType == 'son')
			{
		
					$userDetail = UserRelation::where('relation_with', $relWithId)->where('relation','spouse')->first();
					$spouseId = $userDetail['id'];

					

					if($spouseId == '')
					{
						UserRelation::where('id', $rootId)->update(['parent' => $relWithId]);
					}
					else
					{

						$parentId = $spouseId.','.$relWithId;
						UserRelation::where('id', $rootId)->update(['parent' => $parentId]);
					}
				
			}
			if($memberType == 'brother' || $memberType == 'sister')
			{
				$userDetail = UserRelation::where('id', $relWithId)->first();
				$userParent = $userDetail->parent;
				

				if($userParent != '')
				{
					UserRelation::where('id', $rootId)->update(['parent' => $userParent]);
				}
			}


			$result['success'] = true;
			$result['msg'] = 'Member added successfully';
			$result['url'] = '/tree';
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Member not added successfully';
		}


		echo json_encode($result);exit;
	}  

/*============Show group view============*/

	public function showGroup()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$userGroups = UserGroup::where('user_id',$userId)->with('members')->orderby('id','DESC')->get();
			$followedGroups = GroupMember::where('member_id',$userId)->with('groupDetail')->orderby('id','DESC')->get();
			return view('user.group',['userGroups'=>$userGroups,'followedGroups'=>$followedGroups]);
		}
	}

/*============Create group============*/

	public function createGroup(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$group = new UserGroup();
			
			
			if($request->isMethod('post'))
			{
				$membersId = $request->input('member_list');


				$group->group_name = $request->input('group_name');
		        $group->description = $request->input('description');
		        $group->user_id = $userId;

				if ($request->hasFile('profile_image')) 
			 	{
			 		$profile_image = time().'.'.$request->file('profile_image')->getClientOriginalExtension();
			 		$request->file('profile_image')->move(public_path('upload/groupphotos'), $profile_image);
			 		$group->profile_image = $profile_image;
			 	}

			 	$group->save();

			 	$groupId = $group->id;

				if($groupId != 0 && $groupId != '')
				{
					if($membersId != '')
					{
						foreach($membersId as $memId)
						{
							$groupMember = new GroupMember();
							
							$groupMember->group_id = $groupId;
							$groupMember->user_id = $userId;
							$groupMember->member_id = $memId;
							$groupMember->status = 'Accepted';
							$groupMember->save();

							$notifyData = new UserNotification();
							$notifyData->user_id = $memId;
							$notifyData->sender_id = $userId;
							$notifyData->model_name = 'UserGroup';
							$notifyData->notification = 'New group '.$request->input('group_name').' created by'.Auth::user()->id;
							$notifyData->table_id = $groupId;
							$notifyData->save();
						}
					}
					

					$success = true;
					$msg = 'Group added successfully';

				}
				else
				{
					$success = false;
					$msg = 'Group not added successfully';
				}

				$result['success'] = $success;
				$result['msg'] = $msg;
				echo json_encode($result);exit;
			}

			//$userRel = User::where('invited_by_id',$userId)->orderby('id','DESC')->get();
			$userRel = User::where('invited_by_id','!=','')->orderby('id','DESC')->get();
			$invitedId = array();
			$invitedUser = array();
			foreach($userRel as $val)
			{
				$invitedId[$val->id] = explode(",",$val->invited_by_id);
				if (in_array($userId, $invitedId[$val->id]))
				{
					$invitedUser[] = User::where('id',$val->id)->orderby('id','DESC')->get();
				}

			}
			return view('user.create-group',['userRel'=>$invitedUser]);
			
			
		}
	}

/*============Group detail============*/

	public function groupDetail($id)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$groupDetail = UserGroup::where('id',$id)->with(['members','groupAdminDetail'])->first();
			$userRel = User::where('invited_by_id','!=','')->orderby('id','DESC')->get();
			$invitedId = array();
			$invitedUser = array();
			foreach($userRel as $val)
			{
				$invitedId[$val->id] = explode(",",$val->invited_by_id);
				if (in_array($userId, $invitedId[$val->id]))
				{
					$invitedUser[] = User::where('id',$val->id)->orderby('id','DESC')->get();
				}

			}
			return view('user.group-internal',['groupDetail'=>$groupDetail,'invitedUser'=>$invitedUser]);
		}
	}

/*=============Add member in group============*/

	public function addGroupMember(Request $request)
	{
		$userId = Auth::id();
		$membersId = $request->input('member_list');
		$groupId = $request->input('group_id');
		if($groupId != '')
		{
			if($membersId != '')
			{
				foreach($membersId as $memId)
				{
					$groupMember = new GroupMember();
					
					$groupMember->group_id = $groupId;
					$groupMember->user_id = $userId;
					$groupMember->member_id = $memId;
					$groupMember->status = 'Accepted';
					$groupMember->save();
				}
			}
			$result['success'] = true;
			$result['msg'] = 'Member added successfully';
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Member not added successfully';
		}
		echo json_encode($result);exit;
	}

/*=============Forum list view===============*/

	public function forum()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$getInvitedUser =Auth::user()->invited_by_id;
			$forumDetail = UserForum::whereIn('user_id',[$getInvitedUser,Auth::user()->id])->get();
			return view('user.forum',['forumDetail'=>$forumDetail]);
		}
	}

	public function forumDetail($id)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$forumDetail = UserForum::where('id',$id)->with(['user','forumcategory'])->first();
			$forumComment = ForumComment::where('forum_id',$id)->with('user')->orderBy('created_at','desc')->get();
			return view('user.forum-internal',['forumDetail'=>$forumDetail,'forumId'=>$id,'forumComment'=>$forumComment]);
		}
	}

/*=============Create forum===============*/

	public function createForum(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$forum = new UserForum();
			if($request->isMethod('post'))
			{
				$forum->user_id = $userId;
				$forum->cat_id = $request->cat_id;
				$forum->title = $request->title;
				$forum->description = $request->description;
				if ($request->hasFile('image')) 
			 	{
			 		$image = time().'.'.$request->file('image')->getClientOriginalExtension();
			 		$request->file('image')->move(public_path('upload/forum'), $image);
			 		$forum->image = $image;
			 	}
			 	$forum->save();
			 	if($forum->id != '' && $forum->id != 0)
			 	{
			 		$getMembers = User::where('invited_by_id',$userId)->get();
			 		if(count($getMembers) > 0)
			 		{
			 			foreach($getMembers as $val)
						{
							$notifyData = new UserNotification();
							$notifyData->user_id = $val->id;
							$notifyData->sender_id = $userId;
							$notifyData->model_name = 'Forum';
							$notifyData->notification = 'New forum '.$request->title.' added by'.Auth::user()->id;
							$notifyData->table_id = $forum->id;
							$notifyData->save();
						}
			 		}
			 		$success = true;
					$msg = 'Forum created successfully';
				}
			 	else
			 	{
			 		$success = false;
					$msg = 'Forum not created successfully';
				}
			 	$result['success'] = $success;
				$result['msg'] = $msg;
				echo json_encode($result);exit;
			}
			$forumCat = ForumCategory::orderBy('id','desc')->get();
			return view('user.create-forum',['forumCat'=>$forumCat]);
		}
	}

/*=============Add forum comment===============*/

	public function addForumComment(Request $request)
	{
		$userId = Auth::id();
		$comment = new ForumComment();
		$comment->user_id = $userId;
		$comment->forum_id = $request->forum_id;
		$comment->comment = $request->forum_comment;
		$comment->save();
		if($comment->id != '' && $comment->id != 0)
		{
			$getMembers = User::where('invited_by_id',$userId)->get();
	 		if(count($getMembers) > 0)
	 		{
	 			foreach($getMembers as $val)
				{
					$notifyData = new UserNotification();
					$notifyData->user_id = $val->id;
					$notifyData->sender_id = $userId;
					$notifyData->model_name = 'Forum';
					$notifyData->notification = 'New comment added in forum';
					$notifyData->table_id = $request->forum_id;
					$notifyData->save();
				}
	 		}
			$commentDetail = ForumComment::where('id',$comment->id)->with('user')->first();
			$allComments = ForumComment::where('forum_id',$request->forum_id)->get();
			$data = '';
			$data .= '<div class="forum-comment-bx"><figure><img src="'.url('/upload/profileimages/'.$commentDetail->user['avatar']).'" alt="user-sml-img1"></figure><div class="forum-txt"><h6>'.$commentDetail->user['name'].'<small><time class="timeago" datetime="'.date('Y-m-d', strtotime($commentDetail->created_at)).'T'.date('H:i:s', strtotime($commentDetail->created_at->diffForHumans())).'Z">'.$commentDetail->created_at->diffForHumans().'</time></small></h6><p>'.ucfirst($commentDetail->comment).'</p></div></div>';

			$result['html'] = $data;
			$result['count'] = count($allComments);
			echo json_encode($result);exit;
		}
	}

	
      

/*=============Get member list for invite friends===============*/

	public function getMemberList()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$memberList = UserRelation::where('relation','!=','0')->with('getuser')->where('user_id',$userId)->orderBy('id','desc')->get();
			$data = '';
			$data .= '<option value="">Select member....</option>';
			if(!empty($memberList))
			{
				foreach($memberList as $val)
				{
					if(count($val->getuser) == 0)
					{
						$data .= '<option value="'.$val['id'].'">'.$val['first_name'].' '.$val['last_name'].'</option>';
					}
				}
				
			}
			echo $data;
		}
	}

/*=============Get member email for invite friends===============*/

	public function getMemberEmail()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$memberEmail = UserRelation::where('id',$_GET['memberId'])->first();
			$email = $memberEmail->email_address;
			echo $email;
		}
	}

/*=============Invite friend request===============*/

	public function inviteMember(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$currentUserEmail = Auth::user()->email;
			$inviteMember = new InviteMember();
			$userDetail = UserRelation::where('id','=',$request->member_id)->first();

			$email = $request->user_email;
			$checkEmail = User::where('email', '=', $request->user_email)->get();
			if (count($checkEmail)) 

			{
			   	$result['success'] = false;
				$result['msg'] = 'You cannot enter your email';
			}
			else
			{
				UserRelation::where('id',$request->member_id)->update(['email_address'=>$request->user_email]);
				
				$inviteMember->user_id = $userId;
				$inviteMember->member_id = $request->member_id;
				$inviteMember->save();
				if($inviteMember->id != '' && $inviteMember->id != 0)
				{
					UserRelation::where('id',$request->member_id)->update(['email_address'=>$request->user_email]);
					$result['success'] = true;
					$result['msg'] = 'Member invited successfully';
					
   					$data = array('invited_by_id'=>$userId,'userRefId' => $userDetail->unique_id);



				    \Mail::send('invite-member', $data, function ($message) use($email){
		          
			            $message->subject('Family Network Invite');
			            $message->to($email);

		        	});
				}
				else
				{
					$result['success'] = true;
					$result['msg'] = 'Member not invited successfully';
				}
			}
			echo json_encode($result);exit;
		}
	}

	public function questions(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			if($request->isMethod('post'))
			{
				
		    	$story = new Story();

				$story->user_id = $userId;
				$story->title = $request->question;
				$story->excerpt = substr($request->user_answer,0,10);
				$story->description = $request->user_answer;
				if ($request->hasFile('user_photo')) 
			 	{
			 		$userPhoto = new UserPhoto();

			 		$user_photo = time().'.'.$request->file('user_photo')->getClientOriginalExtension();
		    		$request->file('user_photo')->move(public_path('upload/userphotos'), $user_photo);
					$userPhoto->user_id = $userId;
		    		$userPhoto->user_photo = $user_photo;
		    		$userPhoto->save();
		    		$story->user_photo_id = $userPhoto->id;
		    	}
				
				$story->save();
				if($story->id != '' && $story->id != 0)
				{
					$result['success'] = true;
					$result['msg'] = 'Answer submitted successfully';
				}
				else
				{
					$result['success'] = false;
					$result['msg'] = 'Answer not submitted successfully';
				}
				echo json_encode($result);exit;
				
			}
			$getQuestions = UserQuestion::orderBy('id','desc')->get();
			return view('user.questions',['questions'=>$getQuestions]);
		}
	}

/*==============Edit member profile view from tree node=============*/
	
	public function editProfile($id)
	{
		$memberDetail = UserRelation::where('id',$id)->first();
		return view('user.edit-profile',['memberDetail'=>$memberDetail]);
	}

/*==============Update edit member profile from tree node=============*/

	public function updateProfile(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			// if($request->email != '')
			// {
				// $checkEmail = Userrelation::where('email_address',$request->email)->where('id','!=',$request->editId)->get();
				// if(count($checkEmail) > 0)
				// {
					// $result['success'] = false;
					// $result['msg'] = 'Email already exist';
					// echo json_encode($result);exit;
				// }
				// else
				// {
					// $this->userEditDetail($request->all());
				// }
			// }
			// else
			// {
				$this->userEditDetail($request->all());
			//}
		}
		
	}
	public function userEditDetail($param)
	{
		$data = array(
			'gender' => $param['gender'],
			'first_name' => $param['first_name'],
			'last_name' => $param['last_name'],
			'email_address' => $param['email'],
			'borndate' => date('Y-m-d',strtotime($param['borndate'])),
			'bornplace' => $param['bornplace'],
			'live_dead' => $param['living'],
		);
		if (isset($param['profile_img'])) 
	 	{
	 		$profile_img = time().'.'.$param['profile_img']->getClientOriginalExtension();
    		$param['profile_img']->move(public_path('upload/profileimages'), $profile_img);
			$data['profile_image'] = $profile_img;
    	}
		$editId = UserRelation::where('id',$param['editId'])->update($data);
		if($editId == 1)
		{
			$result['success'] = true;
			$result['msg'] = 'Profile updated successfully';
			$result['url'] = '/tree';
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Profile not updated successfully';
		}
		echo json_encode($result);exit;
	}

/*==========User Bio==========*/	

	public function userBio()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$userBio = User::where('id',$userId)->first();
			return view('user.bio',['userBio'=>$userBio]);
		}
	}


/*===========Chat Modeule===========*/

	public function chat(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();



			if($request->isMethod('post'))
			{
				if($request->chat_id == '')
				{
					$conversation = new UserConversation();
					$conversation->from = $userId;
					$conversation->to = $request->reciever_id;
					$conversation->save();
					if($conversation->id != '' && $conversation->id != 0)
					{
						$notifyData = new UserNotification();
						$notifyData->user_id = $userId;
						$notifyData->sender_id = $request->reciever_id;
						$notifyData->model_name = 'Chat';
						$notifyData->notification = 'New conversation message';
						$notifyData->table_id = $conversation->id;
						$notifyData->save();
						
						$request['chat_id'] = $conversation->id;
						$result = $this->converstionMessage($request->all());
						
					}
					else
					{
						$result['success'] = false;
						$result['msg'] = 'Something went wrong!';
					}
				}
				else
				{
					$result = $this->converstionMessage($request->all());
				}
				return json_encode($result);exit;
			}

			$memberList = User::with(array('getmembers' => function($query) {
           						$query->where('status', '=','Accepted')->with('user');
	        				}))->with('parentUser')->where('id',$userId)->first();

			$userRel = User::where('id',$userId)->orderby('id','DESC')->first();
			$invitedId = explode(",",$userRel->invited_by_id);

			$invitedUser = array();
			foreach($invitedId as $val)
			{
				
				$invitedUser[] = User::where('id',$val)->orderby('id','DESC')->get();
			}

			$getConversation = UserConversation::where('from',$userId)->orWhere('to',$userId)->with('conversationmsg')->orderBy('created_at','ASC')->first();

			return view('user.chat',['memberList'=>$memberList,'getConversation'=>$getConversation,'invitedUser'=>$invitedUser]);
		}
	}
	public function converstionMessage($param)
	{
		$userId = Auth::id();
		$conversationMsg = new UserConversationMessage();
		$conversationMsg->chat_id = $param['chat_id'];
		$conversationMsg->sender_id = $userId;
		$conversationMsg->message = $param['message'];
		$conversationMsg->save();
		if($conversationMsg->id != '')
		{
			$html = '<div class="outgoing_msg">';
            $html .= '<div class="sent_msg"><p>'.$param['message'].'</p></div>';
			$html .= '<div class="outgoing_msg_img"><img src="'.url('/upload/profileimages/'.Auth::user()->avatar).'" alt="john"></div>';
            $html .= '</div>';

			$result['success'] = true;
			$result['chatId'] = $param['chat_id'];
			$result['html'] = $html;
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Something went wrong!';
		}
		return $result;
	}

/*=============Get conversation detail on click member===============*/

	public function getConversationDetail()
	{
		$memberId = $_GET['memberId'];
		$userId = Auth::id();
		$userDetail = User::where('id',$memberId)->first();
		$getConversation = UserConversation::where(['from' => $userId, 'to' => $memberId])->orWhere(['from' => $memberId, 'to' => $userId])->with('conversationmsg')->orderBy('created_at','ASC')->first();
		return view('user.user_conversation',['getConversation'=>$getConversation,'userDetail'=>$userDetail]);
	}

/*=============Group chat module===============*/

	public function groupChat(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			if($request->isMethod('post'))
			{
				$conversation = new GroupConversation();
				$conversation->group_id = $request->group_id;
				$conversation->user_id = $userId;
				$conversation->message = $request->message;
				$conversation->save();
				if($conversation->id != '' && $conversation->id != 0)
				{
					$html = '<div class="outgoing_msg">';
		            $html .= '<div class="sent_msg"><p>'.$request->message.'</p></div>';
					$html .= '<div class="outgoing_msg_img"><img src="'.url('/upload/profileimages/'.Auth::user()->avatar).'" alt="john"></div>';
		            $html .= '</div>';

					$result['groupId'] = $request->group_id;
					$result['html'] = $html;
					$result['success'] = true;
				}
				else
				{
					$result['success'] = false;
					$result['msg'] = 'Something went wrong!';
				}
				echo json_encode($result);exit;
			}

			$status = 'Accepted';

			$userGroup = UserGroup::where('user_id',$userId)->orWhereHas( 'members', function( $query ) use ( $userId){ $query->where('member_id',$userId); })->get();
			
			$groupConversation = UserGroup::select('user_groups.*')->leftJoin('group_members','group_members.group_id','=','user_groups.id')->where('user_groups.user_id',$userId)->orWhere(['group_members.member_id'=>$userId,'group_members.status'=>'"'.$status.'"'])->with(['groupconversation','userdetail'])->orderBy('user_groups.id','DESC')->first();

			return view('user.group-chat',['userGroup'=>$userGroup,'groupConversation'=>$groupConversation]);
		}
	}

/*=============Get conversation detail of group on click group===============*/

	public function getGroupConversationDetail()
	{
		$groupId = $_GET['groupId'];
		$groupDetail = UserGroup::where('id',$groupId)->first();
		$groupConversation = GroupConversation::where('group_id',$groupId)->with('userdetail')->orderBy('created_at','ASC')->get();
		return view('user.group-conversation',['groupConversation'=>$groupConversation,'groupDetail'=>$groupDetail]);
	}

/*=============Search Member for conversation===============*/

	public function searchMember()
	{
		$keyword = $_GET['keyword'];
		$userId = Auth::id();
		$memberList[] = User::with(array('getmembers' => function($query) use($keyword) {
			           	$query->where('status', '=','Accepted')->with(array('user' => function($user) use($keyword) {
			           	$user->where('name', 'Like','%' . $keyword . '%');
			           }));
			        }))->where('id',$userId)->first();

		
		$userRel = User::where('id',$userId)->orderby('id','DESC')->first();
		$invitedId = explode(",",$userRel->invited_by_id);
		$invitedUser = array();
		foreach($invitedId as $val)
		{
			$invitedUser[] = User::where('id',$val)->where('name', 'Like','%' . $keyword . '%')->groupBy('id')->orderby('id','DESC')->get();
		}

		return view('user.search-conversation-member',['memberList'=>$memberList,'invitedUser'=>$invitedUser]);
	}

/*=============Search Group for conversation===============*/

	public function searchGroup()
	{
		$keyword = $_GET['keyword'];
		$userId = Auth::id();
			$userGroup = UserGroup::where('user_id',$userId)->orWhereHas( 'members', function( $query ) use ( $userId){ $query->where('member_id',$userId); })->get();
		$memberList = UserGroup::with('groupMembers')->where('user_id',$userId)->get();
	}

	public function deleteStoryUpload()
	{
		$id = $_GET['id'];
		if($_GET['type'] == 'image')
		{
			UserPhoto::where('id',$id)->delete();
			Story::where('user_photo_id',$id)->delete();
		}
		else
		{
			UserVideo::where('id',$id)->delete();
			Story::where('user_video_id',$id)->delete();
		}
		echo 1;
	}

	public function paypalSubscription(Request $request)
	{
		$data = $request->all();
		$data['user_id'] = Auth::id();
		return view('pages.paypal-form',['data'=>$data]);
	}

}
