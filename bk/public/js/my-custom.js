jQuery(document).ready(function()
{
/*=============Show register modal when invited user click on link from email===========*/

	$(window).on('load',function(){
		var urlString = getUrlVars();
		alert(urlString);
		if(urlString == 'userRefId,invitedById')
		{
			user_sign_up(getUrlVars()["userRefId"],getUrlVars()["invitedById"]);
		}
		
	});

/*=========get url string for show register modal when invited user click on link from email========*/
	
	function getUrlVars()
	{
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}

/*=========Show register modal for normal user========*/

	jQuery('.user_sign_up').click(function(){
		user_sign_up();
	});

/*=================Common user sign up modal================*/

	function user_sign_up(userRefId,invitedById)
	{
		jQuery('#open_source').modal();
		jQuery('#open_source').find('.modal-title').empty().append('Create My Family Tree');
		jQuery('#open_source').find('.modal-body').empty().append('Processing...');
		
		jQuery.ajax({
			url: '/showuserregisterform',
			type: 'POST',
			data:{userRefId:userRefId,invitedById:invitedById},
			headers: {
			'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
			},
			success: function(result) {    
				jQuery('#open_source').find('.modal-body').empty().append(result);	
			} 
		});
	}
	
		
	var token=jQuery('meta[name=csrf-token]').attr("content");
	var time=jQuery('.timeago').length;
	if(time!=0)
	{	
		jQuery("time.timeago").timeago();
	}

	jQuery('#user_register').validate({
		rules: { 
			first_name: {
				required: true 
			},
			last_name: {
				required: true 
			},
			father_first_name: {
				required: true 
			},
			father_last_name: {
				required: true 
			},
			mother_first_name: {
				required: true 
			},
			mother_last_name: {
				required: true 
			} 
		},
		submitHandler: function(form) {
			
			var len=jQuery('input[name=gender]:checked').length;
			if(len==0)
			{
				toastr.error('Please select your gender');
				return false;	 
			}	
			jQuery('#user_register').find('input[type=submit]').attr('disabled',true);
			jQuery('#user_register').find('input[type=submit]').val('Processing...');
			
			jQuery.ajax({ 
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/registeruser',
				success: function(data) 
				{
					
					jQuery('#user_register').find('input[type=submit]').removeAttr('disabled');
					jQuery('#user_register').find('input[type=submit]').val('Get Started');
					
					//alert(data);
					//return false; 
					if(data==0)
					{
						toastr.error('Email already exists');
						return false;	
					}
					else
					{
						window.location.href="/dashboard";
					} 
				} 
			});
			
		}	 
		
	});	
	
	
	jQuery('#login_form').validate({
		rules: { 
			email: {
				required: true,
				email:true
			},
			password: {
				required: true 
			}
		},
		submitHandler: function(form) {
			
			jQuery.ajax({ 
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/loginuser',
				success: function(data) 
				{
					 
					
				} 
			});
			
		}	 
		
	});	
	
	jQuery('#my_account').validate({
		rules: { 
			name: {
				required: true
			},
			email: {
				required: true,
				email:true
			},
			dob: {
				required: true 
			}
		}
		
		, 
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/my-account',
				success: function(data) 
				{ 
					if(data=='1')
					{
						toastr.success('Changes Saved');		
						setTimeout(function(){
						  location.reload();
						}, 1500);
					}
				} 
			});
			
		}	 
		
	});	
	
	
	jQuery('#my_image').validate({
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/change_user_image_ajax',
				success: function(data) 
				{
					if(data=='1')
					{
						location.reload();		
					} 
				} 
			});
			
		}	 
		
	});	
	
	jQuery('#my_cover_image').validate({
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: '/change_user_cover_image_ajax',
				success: function(data)  
				{ 
					if(data=='1')
					{
						location.reload();		
					} 
				} 
			});
			
		}	 
		
	});	

/*================Add story Photo validation================*/

	jQuery('#add_photo').validate({
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: jQuery(form).action, 
				success: function(data)  
				{ 

					if(data!='' | data != 0)
					{
						
						toastr.success('Image uploaded successfully');		
						setTimeout(function(){
						  location.reload();
						}, 2000);	
					} 
				} 
			});
			
		}	 
		
	});	

/*================Add story Video validation================*/

	jQuery('#add_video').validate({
		
		submitHandler: function(form) {
			
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(), 
				url: jQuery(form).action, 
				success: function(data)  
				{ 

					if(data!='' && data != 0)
					{
						
						toastr.success('Video uploaded successfully');		
						setTimeout(function(){
						  location.reload();
						}, 2000);	
					} 
				} 
			});
			
		}	 
		
	});	

/*================Add member validation================*/

	jQuery('#add-member-detail').validate({
		rules: { 
			first_name: 
			{
				required: true,
			},
			last_name: 
			{
				required: true 
			}
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Add Member';

			commonAjaxSubmit(form,btnVal);
			
		}	 
		
	});	

/*================Create group validation================*/

	jQuery('#create-group').validate({
		rules: { 
			group_name: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Create Group';
			
			commonAjaxSubmit(form,btnVal);
			
		}	 
		
	});	

/*================Create forum validation================*/

	jQuery('#create-forum').validate({
		rules: { 
			title: 
			{
				required: true,
			},
			cat_id: 
			{
				required: true,
			},
			description: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Create';
			
			commonAjaxSubmit(form,btnVal);
			
		}	 
		
	});

/*================Add forum comment validation================*/

	jQuery('#forum_comment').validate({
		rules: { 
			forum_comment: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			jQuery(form).find('input[type=submit]').attr('disabled',true);
			jQuery(form).find('input[type=submit]').val('Processing...');
			jQuery.ajax({ 
				type: form.method,
				data: jQuery(form).serialize(), 
				url: form.action,
				success: function(data) 
				{
					if(data != '')
					{
						jQuery('.forum-comment-data').prepend(data);
						jQuery('#forum_comment').trigger('reset');
						jQuery(form).find('input[type=submit]').attr('disabled',false);
						jQuery(form).find('input[type=submit]').val('Submit');
						toastr.success('Comment added successfully');
					}
					else
					{
						toastr.error('Comment not added successfully');
					}
				} 
			});
			
		}	 
		
	});
/*================Add forum comment validation================*/

	jQuery('#user_invite_member').validate({
		rules: { 
			member_id: 
			{
				required: true,
			},
			user_email: 
			{
				required: true,
			},
		},
		submitHandler: function(form) 
		{
			var btnVal = 'Invite';
			commonAjaxSubmit(form,btnVal);
		}	 
	});
/*================question answer validation================*/

	
	jQuery(document).on('click','.answer-btn',function(){
		var formId = jQuery(this).parent('form').attr('id');
		jQuery('#'+formId).validate({
			rules: { 
				user_answer: 
				{
					required: true,
				},
			},
			submitHandler: function(form) 
			{
				var btnVal = 'Submit';
				commonAjaxSubmit(form,btnVal);
			}
		});
	});
/*================Create story validation================*/	
	
	setTimeout(function(){
		jQuery('#create_story').validate({
			rules: { 
				title: {
					required: true
				},
				content: {
					required: true
				}
			}, 
			submitHandler: function(form) {
				jQuery('#create_story').find('input[type=submit]').val('Processing...');
				jQuery('#create_story').find('input[type=submit]').attr('disabled',true);
				jQuery(form).ajaxSubmit({
					type: "POST",
					data: jQuery(form).serialize(), 
					url: '/create-story',
					success: function(data) 
					{ 
						
						if(data != 0)
						{
							jQuery('#create_story').find('input[type=submit]').val('Save');
							jQuery('#create_story').find('input[type=submit]').removeAttr('disabled');
							
							toastr.success('Your Story has been published');
							jQuery('#create_story').trigger('reset');
						}
						/*if(data!='0')
						{
							jQuery('#create_story').find('input[type=submit]').val('Save');
							jQuery('#create_story').find('input[type=submit]').removeAttr('disabled');
							
							toastr.success('Your Story has been published');
							jQuery('#create_story').trigger('reset');		 
						} */
					} 
				});
				
			}	 
			
		});	
	}, 500);
	
	
	
});	

function commonAjaxSubmit(form,btnVal)
{
	jQuery(form).find('input[type=submit]').attr('disabled',true);
	jQuery(form).find('input[type=submit]').val('Processing...');

	jQuery.ajax({ 
		type: form.method,
		data: new FormData(form), 
		url: form.action,
		processData: false,
        contentType: false,
		dataType:"json",
		success: function(data) 
		{
			jQuery(form).find('input[type=submit]').attr('disabled',false);
			jQuery(form).find('input[type=submit]').val(btnVal);
			
			if(data.success == true)
			{
				jQuery(form).trigger('reset');
				toastr.success(data.msg);
				setTimeout(function(){
				  location.reload();
				}, 2000);	
			}
			else
			{
				toastr.error(data.msg);
			}
		} 
	});
}

function mystory_pagination()
{
	var total_record=jQuery('#total_record').val();
	var offset=jQuery('#offset').val();
	jQuery('.load_more').html('Processing...');  
	jQuery.ajax({ 
		url: '/mystory_load_more',
		type: 'POST',
		data: "offset="+offset, 
		headers: {
		'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		}, 
		success: function(result) {    
			jQuery('.load_more').html('Load More');  
			offset=parseInt(offset)+1; 
			jQuery('#offset').val(offset);
			jQuery(result).insertAfter('.my_story:last');  
			
			var len=jQuery('.my_story').length;
			if(len==total_record)
			{
				jQuery('.load_more').hide();		
			}
			jQuery("time.timeago").timeago();		
		}   
	});  
	
	
}	 


function dashboard_story_pagination()
{
	var total_record=jQuery('#total_record').val();
	var offset=jQuery('#offset').val();
	jQuery('.load_more').html('Processing...');  
	jQuery.ajax({ 
		url: '/dashboard_story_load_more',
		type: 'POST',  
		data: "offset="+offset,  
		headers: {
		'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		}, 
		success: function(result) {    
			jQuery('.load_more').html('Load More');  
			offset=parseInt(offset)+1; 
			jQuery('#offset').val(offset);
			jQuery(result).insertAfter('.my_story:last');  
			
			var len=jQuery('.my_story').length;
			if(len==total_record)
			{
				jQuery('.load_more').hide();		
			}
			jQuery("time.timeago").timeago();		
		}   
	}); 
}	


function showMyImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {           
		var file = files[i];
		var imageType = /image.*/;     
		if (!file.type.match(imageType)) {
			alert('This File type not allowed here');
			jQuery('input[type=file]').val('');
			jQuery('#thumbnil').hide();
			return false;	 
		}           
		var img=document.getElementById("thumbnil");            
		img.file = file;    
		if(file.size > 30000)
		{
			alert('Please check file size');
			jQuery('input[type=file]').val('');
			jQuery('#thumbnil').hide();
			return false;	
		}
		var reader = new FileReader();
		reader.onload = (function(aImg) { 
			return function(e) { 
				aImg.src = e.target.result; 
				jQuery('#thumbnil').show();
			}; 
		})(img);
		reader.readAsDataURL(file);
		
		
		
	}    
}


function MyImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {           
		var file = files[i];
		var imageType = /image.*/;     
		if (!file.type.match(imageType)) {
			alert('This File type now allowed here');
			
			return false;	 
		}           
		
		if(file.size > 30000)
		{
			alert('Your file size should be less than 60kb');
			return false;	 
		}
		
		jQuery('#my_image').find('input[type=submit]').click();
		
		
	}    
}
function MyCoverImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {           
		var file = files[i];
		var imageType = /image.*/;     
		if (!file.type.match(imageType)) {
			alert('This File type now allowed here');
			
			return false;	 
		}           
		
		if(file.size > 30000)
		{
			alert('Your file size should be less than 60kb');
			return false;	 
		}
		
		jQuery('#my_cover_image').find('input[type=submit]').click();
}

