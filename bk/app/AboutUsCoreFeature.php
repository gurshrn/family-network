<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class AboutUsCoreFeature extends Model
{
    public function GetAboutUsCoreFeature()
	{
		$result = DB::table('about_us_core_features')->get();
		return $result;
	}	
}
