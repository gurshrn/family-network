<?php

namespace App\Http\Controllers;
use App\Models\UserGroup;
use App\Models\UserRelation;
use App\Models\GroupMember;
use App\Models\UserRelationParent;
use App\Models\UserRelationPartner;
use App\Models\User;
use App\Models\Story;
use App\Models\StoryMember;
use App\Models\UserPhoto;
use App\Models\UserVideo;
use App\Models\UserForum;
use App\Models\ForumComment;
use App\Models\InviteMember;
use App\Models\UserQuestion;
use App\Models\ForumCategory;
use Auth; 
use Mail;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

/*==============Dashboard=============*/

	public function dashboard()
	{
		if(Auth::user()) 
		{
			$userId=Auth::user()->id;
			$offset=0;
			$limit=1;
			
			$userDetail = User::where('id',$userId)->first();
			$userStory  = Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers'])->orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
			$totalRecords = count($userStory);
			
			$data=array('user_id'=>$userId,'UserDetail'=>$userDetail,'resultStory'=>$userStory,'total_record'=>$totalRecords);	
			return view('user.dashboard',$data);
		}
		else
		{
			return redirect('/');
		}
	}

	public function myStory()
	{
		if(Auth::user()) 
		{
			$userId=Auth::user()->id;
			$offset=0;
			$limit=1;
			$userDetail = User::where('id',$userId)->first();
			$userStory  = Story::where('user_id',$userId)->with(['userdetail','userphotos','uservideos','storymembers'])->orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
			$totalRecords = count($userStory);
			
			$data=array('userStory'=>$userStory,'userDetail'=>$userDetail,'totalRecords'=>$totalRecords);	
			return view('user.mystory',$data);
		}
	}



/*==============Add Story=============*/

	public function createStory(Request $request)
	{
		if(Auth::user()) 
		{
			$userId=Auth::user()->id;
			
			if($request->isMethod('post'))
			{
				$membersId = $request->input('member_list');
				if ($request->hasFile('user_photo')) 
			 	{
			 		$userPhoto = new UserPhoto();

			 		$user_photo = time().'.'.$request->file('user_photo')->getClientOriginalExtension();
		    		$request->file('user_photo')->move(public_path('upload/userphotos'), $user_photo);
					$userPhoto->user_id = $userId;
		    		$userPhoto->user_photo = $user_photo;
		    		$userPhoto->save();
		    	}
				
				if ($request->hasFile('user_video')) 
			 	{
			 		$userVideo = new UserVideo();

			 		$user_video = time().'.'.$request->file('user_video')->getClientOriginalExtension();
			 		$request->file('user_video')->move(public_path('upload/uservideos'), $user_video);
			 		$userVideo->user_id = $userId;
		    		$userVideo->user_video = $user_video;
		    		$userVideo->save();
		    	}
				
				$story = new Story();

				$story->user_id = $userId;
				$story->group_id = $request->group_id;
				$story->title = $request->title;
				$story->excerpt = $request->excerpt;
				$story->description = $request->description;
				if(isset($userPhoto->id))
				{
					$story->user_photo_id = $userPhoto->id;
				}
				if(isset($userVideo->id))
				{
					$story->user_video_id = $userVideo->id;
				}
				
				$story->save();

				if($membersId != '')
				{
					foreach($membersId as $memId)
					{
						$storyMember = new StoryMember();
						
						$storyMember->story_id = $story->id;
						$storyMember->member_id = $memId;
						$storyMember->save();
					}
				}
			}
			$userDetail = User::where('id',$userId)->first();
			$userRel = UserRelation::where('relation','!=','0')->where('user_id',$userId)->orderby('id','DESC')->get();
			$data=array('user_id'=>$userId,'UserDetail'=>$userDetail,'userRel'=>$userRel);
			return view('user.createstory',$data);
		}
	}

/*=============Add story photos===============*/

	public function addPhotos(Request $request)
	{
		$userId=Auth::user()->id;

		if($request->isMethod('post'))
		{
			if ($request->hasFile('user_photo')) 
		 	{
		 		$userPhoto = new UserPhoto();

		 		$user_photo = time().'.'.$request->file('user_photo')->getClientOriginalExtension();
        		$request->file('user_photo')->move(public_path('upload/userphotos'), $user_photo);
        		$userPhoto->user_id = $userId;
	    		$userPhoto->user_photo = $user_photo;
	    		$userPhoto->save();
	    		echo $userPhoto->id;
        	}
		}
		else
		{
			$userPhotos = UserPhoto::where('user_id',$userId)->get();
			$data = array('userPhotos'=>$userPhotos);
			return view('user.userphotos',$data);
		}
	}

/*=============Add story videos===============*/

	public function addVideos(Request $request)
	{

		$userId=Auth::user()->id;

		if($request->isMethod('post'))
		{
			if ($request->hasFile('user_video')) 
		 	{
		 		$userVideo = new UserVideo();

		 		$user_video = time().'.'.$request->file('user_video')->getClientOriginalExtension();
		 		$request->file('user_video')->move(public_path('upload/uservideos'), $user_video);
		 		$userVideo->user_id = $userId;
	    		$userVideo->user_video = $user_video;
	    		$userVideo->save();
	    		echo $userVideo->id;
			}
		}
		else
		{
			$userVideos = UserVideo::where('user_id',$userId)->get();
			$data = array('userVideos'=>$userVideos);
			return view('user.uservideos',$data);
		}
	}

/*=============Show User Tree=============*/

	public function userTree()
	{
		if(Auth::user()) 
		{
			$userId = Auth::user()->id;
			
			$rootarray = UserRelation::where('relation','0')->where('user_id',$userId)->get();
			$userTree  = UserRelation::where('relation','!=','0')->where('user_id',$userId)->get();
			
			$data=array('root_user'=>$rootarray,'UserTree'=>$userTree,'current_user'=>$userId);
			return view('user.tree',$data);	
		}		
	}

/*=============Add Members for tree=============*/

	public function addMember(Request $request)
	{
		
		$user_id = Auth::user()->id;
		
		$userRelation = new UserRelation();
		$userRelationParent = new UserRelationParent();
		$userRelationPartner = new UserRelationPartner();

		$relWithId = UserRelation::where('relation','0')->where('user_id',$user_id)->first();

		$memberType = lcfirst($request->member_type);
		
		$userRelation->user_id = $user_id;
		$userRelation->first_name = $request->first_name;
		$userRelation->last_name = $request->last_name;
		$userRelation->relation = $memberType;
		$userRelation->relation_with = $relWithId['id'];
		$userRelation->email_address = $request->email;
		$userRelation->borndate = $request->birth_date;
		$userRelation->bornplace = $request->birth_place;
		$userRelation->live_dead = $request->living;

		if ($request->hasFile('profile_image')) 
	 	{
	 		$user_photo = time().'.'.$request->file('profile_image')->getClientOriginalExtension();
    		$request->file('profile_image')->move(public_path('upload/profileimages'), $user_photo);
    		$userRelation->profile_image = $user_photo;
		}
        
        $userRelation->save();
		
		$rootId = $userRelation->id;

		if($rootId != '' && $rootId != 0)
		{
			if($memberType == 'mother' || $memberType == 'father')
			{
				$userDetail = UserRelation::where('id', $relWithId['id'])->first();
				$userParent = $userDetail->parent;
				if($userParent != '')
				{
					$parentId = $userParent.','.$rootId;
				}
				else
				{
					$parentId = $rootId;
				}
				UserRelation::where('id', $relWithId['id'])->update(['parent' => $parentId]);
			}
			if($memberType == 'daughter' || $memberType == 'son')
			{
				$userDetail = UserRelation::where('relation_with', $relWithId['id'])->where('relation','spouse')->first();
				$spouseId = $userDetail->id;

				if($spouseId == '')
				{
					UserRelation::where('id', $rootId)->update(['parent' => $relWithId['id']]);
				}
				else
				{

					$parentId = $spouseId.','.$relWithId['id'];
					UserRelation::where('id', $rootId)->update(['parent' => $parentId]);
				}
				
			}
			if($memberType == 'brother' || $memberType == 'sister')
			{
				$userDetail = UserRelation::where('id', $relWithId['id'])->first();
				$userParent = $userDetail->parent;
				

				if($userParent != '')
				{
					UserRelation::where('id', $rootId)->update(['parent' => $userParent]);
				}
			}

			$result['success'] = true;
			$result['msg'] = 'Member added successfully';
			$result['url'] = '/tree';
		}
		else
		{
			$result['success'] = false;
			$result['msg'] = 'Member not added successfully';
		}

		echo json_encode($result);exit;
	}  

/*============Show group view============*/

	public function showGroup()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$userGroups = UserGroup::where('user_id',$userId)->with('members')->orderby('id','DESC')->get();
			return view('user.group',['userGroups'=>$userGroups]);
		}
	}

/*============Create group============*/

	public function createGroup(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$group = new UserGroup();
			
			
			if($request->isMethod('post'))
			{
				$membersId = $request->input('member_list');


				$group->group_name = $request->input('group_name');
		        $group->description = $request->input('description');
		        $group->user_id = $userId;

				if ($request->hasFile('profile_image')) 
			 	{
			 		$profile_image = time().'.'.$request->file('profile_image')->getClientOriginalExtension();
			 		$request->file('profile_image')->move(public_path('upload/groupphotos'), $profile_image);
			 		$group->profile_image = $profile_image;
			 	}

			 	$group->save();

			 	$groupId = $group->id;

				if($groupId != 0 && $groupId != '')
				{
					foreach($membersId as $memId)
					{
						$groupMember = new GroupMember();
						
						$groupMember->group_id = $groupId;
						$groupMember->user_id = $userId;
						$groupMember->member_id = $memId;
						$groupMember->status = 'Invited';
						$groupMember->save();
					}

					$success = true;
					$msg = 'Group added successfully';

				}
				else
				{
					$success = false;
					$msg = 'Group not added successfully';
				}

				$result['success'] = $success;
				$result['msg'] = $msg;
				//echo json_encode($result);exit;
			}

			$userRel = UserRelation::where('relation','!=','0')->where('user_id',$userId)->orderby('id','DESC')->get();
			return view('user.create-group',['userRel'=>$userRel]);
			
			
		}
	}

/*============Group detail============*/

	public function groupDetail($id)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$groupDetail = UserGroup::where('id',$id)->with(['members','groupstory'])->first();
			return view('user.group-internal',['groupDetail'=>$groupDetail]);
		}
	}

/*=============Forum list view===============*/

	public function forum()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$forumDetail = UserForum::where('user_id',$userId)->with(['user','forumcategory'])->get();
			return view('user.forum',['forumDetail'=>$forumDetail]);
		}
	}

	public function forumDetail($id)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$forumDetail = UserForum::where('id',$id)->with(['user','forumcategory'])->first();
			$forumComment = ForumComment::where('forum_id',$id)->with('user')->orderBy('created_at','desc')->get();
			return view('user.forum-internal',['forumDetail'=>$forumDetail,'forumId'=>$id,'forumComment'=>$forumComment]);
		}
	}

/*=============Create forum===============*/

	public function createForum(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$forum = new UserForum();
			if($request->isMethod('post'))
			{
				$forum->user_id = $userId;
				$forum->cat_id = $request->cat_id;
				$forum->title = $request->title;
				$forum->description = $request->description;
				if ($request->hasFile('image')) 
			 	{
			 		$image = time().'.'.$request->file('image')->getClientOriginalExtension();
			 		$request->file('image')->move(public_path('upload/forum'), $image);
			 		$forum->image = $image;
			 	}
			 	$forum->save();
			 	if($forum->id != '' && $forum->id != 0)
			 	{
			 		$success = true;
					$msg = 'Forum created successfully';
				}
			 	else
			 	{
			 		$success = false;
					$msg = 'Forum not created successfully';
				}
			 	$result['success'] = $success;
				$result['msg'] = $msg;
				echo json_encode($result);exit;
			}
			$forumCat = ForumCategory::orderBy('id','desc')->get();
			return view('user.create-forum',['forumCat'=>$forumCat]);
		}
	}

/*=============Add forum comment===============*/

	public function addForumComment(Request $request)
	{
		$userId = Auth::id();
		$comment = new ForumComment();
		$comment->user_id = $userId;
		$comment->forum_id = $request->forum_id;
		$comment->comment = $request->forum_comment;
		$comment->save();
		if($comment->id != '' && $comment->id != 0)
		{
			$commentDetail = ForumComment::where('id',$comment->id)->with('user')->first();
			$data = '';
			$data .= '<div class="forum-comment-bx"><figure><img src="'.url('/upload/profileimages/'.$commentDetail->user['avatar']).'" alt="user-sml-img1"></figure><div class="forum-txt"><h6>'.$commentDetail->user['name'].'<small><time class="timeago" datetime="'.date('Y-m-d', strtotime($commentDetail->created_at)).'T'.date('H:i:s', strtotime($commentDetail->created_at)).'Z">'.date('F j, Y, g:i a', strtotime($commentDetail->created_at)).'</time></small></h6><p>'.ucfirst($commentDetail->comment).'</p></div></div>';
			echo $data;
		}
	}

/*=============Get member list for invite friends===============*/

	public function getMemberList()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$memberList = UserRelation::where('relation','!=','0')->with('getuser')->where('user_id',$userId)->orderBy('id','desc')->get();
			$data = '';
			$data .= '<option value="">Select member....</option>';
			if(!empty($memberList))
			{
				foreach($memberList as $val)
				{
					if(count($val->getuser) == 0)
					{
						$data .= '<option value="'.$val['id'].'">'.$val['first_name'].' '.$val['last_name'].'</option>';
					}
				}
				
			}
			echo $data;
		}
	}

/*=============Get member email for invite friends===============*/

	public function getMemberEmail()
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$memberEmail = UserRelation::where('id',$_GET['memberId'])->first();
			$email = $memberEmail->email_address;
			echo $email;
		}
	}

/*=============Invite friend request===============*/

	public function inviteMember(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			$inviteMember = new InviteMember();
			if (UserRelation::where('email_address', '=', $request->user_email)->where('id','!=',$request->member_id)->count() > 0) 
			{
			   	$result['success'] = false;
				$result['msg'] = 'Email already exist';
			}
			else
			{
				$inviteMember->user_id = $userId;
				$inviteMember->member_id = $request->member_id;
				$inviteMember->save();
				if($inviteMember->id != '' && $inviteMember->id != 0)
				{
					UserRelation::where('id',$request->member_id)->update(['email_address'=>$request->user_email]);
					$result['success'] = true;
					$result['msg'] = 'Member invited successfully';
					$data = array('name'=>"Virat Gandhi");
   
				      Mail::send(['text'=>'mail'], $data, function($message) {
				         $message->to('gurjeevan.kaur@imarkinfotech.com', 'Tutorials Point')->subject
				            ('Laravel Basic Testing Mail');
				     });
				      echo "Basic Email Sent. Check your inbox.";
				}
				else
				{
					$result['success'] = true;
					$result['msg'] = 'Member not invited successfully';
				}
			}
			echo json_encode($result);exit;
		}
	}

	public function questions(Request $request)
	{
		if(Auth::user()) 
		{
			$userId = Auth::id();
			if($request->isMethod('post'))
			{
				if ($request->hasFile('user_photo')) 
			 	{
			 		$userPhoto = new UserPhoto();

			 		$user_photo = time().'.'.$request->file('user_photo')->getClientOriginalExtension();
		    		$request->file('user_photo')->move(public_path('upload/userphotos'), $user_photo);
					$userPhoto->user_id = $userId;
		    		$userPhoto->user_photo = $user_photo;
		    		$userPhoto->save();
		    	}
		    	$story = new Story();

				$story->user_id = $userId;
				$story->title = $request->question;
				$story->excerpt = substr($request->user_answer,0,10);
				$story->description = $request->user_answer;
				$story->user_photo_id = $userPhoto->id;
				$story->save();
				if($story->id != '' && $story->id != 0)
				{
					$result['success'] = true;
					$result['msg'] = 'Answer submitted successfully';
				}
				else
				{
					$result['success'] = false;
					$result['msg'] = 'Answer not submitted successfully';
				}
				echo json_encode($result);exit;
				
			}
			$getQuestions = UserQuestion::orderBy('id','desc')->get();
			return view('user.questions',['questions'=>$getQuestions]);
		}
		
	}
}
