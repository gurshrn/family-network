<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRelation;
use App\Story;
use Illuminate\Support\Facades\Hash;
use Auth; 
class UserController extends Controller
{
    
/*=============Show Register view===============*/

    public function ShowUserRegisterForm(Request $request)
	{
		if(!empty($request->all()))
		{

			$getMemberDetail = UserRelation::where('unique_id',$request->userRefId)->first();

			$data = array(
				'email'=>$getMemberDetail->email_address,
				'first_name'=>$getMemberDetail->first_name,
				'last_name'=>$getMemberDetail->last_name,
				'invitedById'=>$request->invitedById,
				'memberId'=>$request->userRefId,
			);
		}
		else
		{
			$data =array();
		}
		return view('user.registeruser',['memberDetail'=>$data]);  
	}

/*=============Register User===============*/

	public function registerUser(Request $request)
	{

		$userobj=new User();
		
		$email=$request->email;
		$first_name=$request->first_name;
		$last_name=$request->last_name;
		$name=$first_name.' '.$last_name;

		$email_exist=User::where('email',$email)->count();
		
		if($email_exist > 0) 
		{
			echo 0; 	 
			die;
		} 
		else 
		{
			$userobj->name = $name;
			$userobj->email = $email;
			$userobj->dob = $request->input('dob');
			$userobj->password = Hash::make('password');
			$userobj->gender = $request->input('gender');
			$userobj->role_id = 2;
			$userobj->invited_by_id = $request->input('invitedById');

			$userobj->save();

			$rootUser = new UserRelation();

			$rootUser->user_id = $userobj->id;
			$rootUser->first_name = $first_name;
			$rootUser->last_name = $last_name;
			$rootUser->relation = 0;
			$rootUser->relation_with = 0;
			$rootUser->live_dead = 'Living';
			$rootUser->email_address = $email;
			
			
			$rootUser->save();

			$fatherRel = new UserRelation();

			$fatherRel->user_id = $userobj->id;
			$fatherRel->first_name = $request->father_first_name;
			$fatherRel->last_name = $request->father_last_name;
			$fatherRel->relation = 'Father';
			$fatherRel->relation_with = $rootUser->id;

			$fatherRel->save();

			$motherRel = new UserRelation();

			$motherRel->user_id = $userobj->id;
			$motherRel->first_name = $request->mother_first_name;
			$motherRel->last_name = $request->mother_last_name;
			$motherRel->relation = 'Mother';
			$motherRel->relation_with = $rootUser->id;
			
			$motherRel->save();

			$parentsId = $fatherRel->id.','.$motherRel->id;

			UserRelation::where('id', $rootUser->id)->update(['parent' => $parentsId]);

			Auth::loginUsingId($userobj->id);

			echo 1;
		}  
	}

/*==============User my account============*/
	
	public function myAccount(Request $request)
	{
		$userId = Auth::user()->id;	

		if($request->isMethod('post'))
		{
			$array=array(
				'name'=>$request->name,	
				'dob'=>$request->dob,	
				'gender'=>$request->gender	
			); 
			if ($request->hasFile('profile_img'))
			{ 
				$profileImg = time().'.'.$request->profile_img->getClientOriginalExtension();
				$request->profile_img->move(public_path('upload/profileimages'), $profileImg);
				$array['avatar'] = $profileImg;
			} 
			User::where('id', $userId)->update($array);
			echo "1";
		} 
		else
		{
			$array = User::where('id',$userId)->first();
			$data  = array('user_id'=>$userId,'UserDetail'=>$array);
			return view('user.myaccount',$data);
		}
	}	 

	public function myStory()
	{

		$offset=0;
		$limit=1;
		$obj=new Story(); 
		$objStory=new Story(); 
		$user=Auth::user();
		$userId = Auth::id();	
		$array=$obj->getUserDetail($userId);
		$resultStory=$objStory->GetUserStory($userId,$offset,$limit);

		$result=json_decode($resultStory);

		$data=array('user_id'=>$userId,'UserDetail'=>$array,'resultStory'=>$result->result,'total_record'=>$result->total_record);	
		return view('user.mystory',$data);		
	}	 
	
	
	public function storydetail($story_id)
	{
		$obj=new Story(); 
		$objStory=new Story(); 
		$user=Auth::user();
		$userId = Auth::id();	
		$array=$obj->getUserDetail($userId);
		$resultStory=$objStory->GetStoryDetailById($story_id);
		
		$result=json_decode($resultStory);

		$data=array('user_id'=>$userId,'UserDetail'=>$array,'resultStory'=>$result->result,'total_record'=>$result->total_record);	
		return view('user.storydetail',$data);			 	
	}	






	
	
}
