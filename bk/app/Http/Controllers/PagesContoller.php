<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Support\Facades\View;
use App\Traits\Blocks;
use Illuminate\Http\Request;
use Auth;

class PagesContoller extends Controller
{
    use Blocks;

    /**
     * Fetch all pages and their associated blocks
     *
     * @param string $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPage($slug = 'home')
    {
		$page = Page::where('slug', '=', $slug)->firstOrFail();
        $blocks = $page->blocks()
            ->where('is_hidden', '=', '0')
            ->orderBy('order', 'asc')
            ->get()
            ->map(function ($block) {
                return (object)[
                    'id' => $block->id,
                    'page_id' => $block->page_id,
                    'updated_at' => $block->updated_at,
                    'cache_ttl' => $block->cache_ttl,
                    'template' => $block->template()->template,
                    'data' => $block->cachedData,
                    'path' => $block->path,
                    'type' => $block->type,
                ];
            });

        // Override standard body content, with page block content
        $page['block_content'] = view('pages.blocks', [
            'page' => $page,
            'blocks' => $this->prepareEachBlock($blocks),
        ]);
        
        // Check that the page Layout and its View exists
        
		
		
		if (empty($page->layout)) {
            $page->layout = 'index'; 
        }
        if (!View::exists("pages.{$page->layout}")) {
             $page->layout = "pages.{$page->layout}";
        }
		
		if ($slug=="how-it-works") 
		{
           $page->layout = "how-it-works";
        }
		else if ($slug=="about-us") 
		{
           $page->layout = "about-us";
        }
		else if ($slug=="faq") 
		{
           $page->layout = "faq";
        }
		else if ($slug=="pricing") 
		{
           $page->layout = "pricing";
        }
		
		
        return view('pages.'.$page->layout,[
            'page' => $page,
            'layout' => $page->layout,
        ]);
    }
	
	
	
	
}
