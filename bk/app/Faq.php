<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;
use DB;

class Faq extends Model
{
    public function GetFaq()
	{
		$result = DB::table('faqs')->get();
		return $result;
	}	
}
