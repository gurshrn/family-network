<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserForum extends Model
{
    public function user()
    {
    	return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function forumcategory()
    {
    	return $this->belongsTo('App\Models\ForumCategory','cat_id','id');
    }

}
