<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRelation extends Model
{
    public function groupMembers()
    {
    	return $this->hasMany('App\Models\GroupMember','group_id');
    }
    public function getuser()
    {
    	return $this->hasMany('App\Models\User','email','email_address');
    }
   
}
