<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    public function userrelation()
    {
    	return $this->belongsTo('App\Models\UserRelation','member_id');
    }
}
