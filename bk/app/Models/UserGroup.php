<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    public function members()
    {
    	return $this->hasMany('App\Models\GroupMember','group_id','id');
    }
    public function groupstory()
    {
    	return $this->hasMany('App\Models\Story','group_id','id');
    }
}
