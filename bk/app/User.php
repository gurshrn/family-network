<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
class User extends \TCG\Voyager\Models\User
{
    use Notifiable;      

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','dob','gender','register_from','provider', 'provider_id'
    ]; 

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	
	public function email_exist($email)
	{
		return $result = DB::table('users')->where('email', $email)->get()->count();
		
	}	
	
	public function getUserDetail($user_id)
	{
		$result = DB::table('users')->where('id', $user_id)->get();
		return $result[0];
		
	}

	public function getUserRelList($userId)
	{
		return $result = DB::table('user_relations')->where('level','!=', 0)->where('user_id',$userId)->get();
	}

	public function getUserRelId($userId)
	{
		return $result = DB::table('user_relations')->where('relation', 0)->where('level',0)->where('user_id',$userId)->first();
	}	
	
	
	public function createrootuser($rootUser)
	{
		return DB::table('user_relations')->insertGetId($rootUser);
	}	
	
	public function createrelation($relation,$array,$root_id)
	{
		if($relation=='father')
		{
			$father_rel_id=DB::table('user_relations')->insertGetId($array);	
			$relation=array(
				'user_id'=>$root_id,
				'rel_id'=>$father_rel_id
			);
			
			DB::table('user_relation_parents')->insertGetId($relation);
		}
		else if($relation=='mother')
		{
			$mother_rel_id=DB::table('user_relations')->insertGetId($array);	
			$relation=array(
				'user_id'=>$root_id,
				'rel_id'=>$mother_rel_id 
			); 
			
			DB::table('user_relation_parents')->insertGetId($relation);
		} 
	}	

	public function createMemberRelation($param)
	{
		DB::table('user_relation_parents')->insertGetId($param);
	}
	
	public function getTreeRoot($userId)
	{
		$array=array(
			'user_id'=>$userId, 
			'relation'=>'0'
		);
		return $result = DB::table('user_relations')->where($array)->get();	
	}

	public function getUserTree($userId)
	{
		return $result = DB::table('user_relations')->where('user_id', '=' , $userId)->where('relation', '!=' , '0')->get(); 	
	}
	
	public function getUserParents($userIdd)
	{
		$parent_id='';
		$id='';
		$result = DB::table('user_relation_parents')->where('user_id', '=' , $userIdd)->get(); 	
		$count = DB::table('user_relation_parents')->where('user_id', '=' , $userIdd)->count(); 	
		if($count > 0) {
		foreach($result as $row)
		{
			$id .=$row->rel_id .',';		
		}

			return $parent_id=substr($id,0,-1);	
		}
		else 
		{
			return $parent_id;	
		} 
		
				
	}
	
	public function getSpouse($idd)
	{

		$spouse_id='';
		$id='';
		$result = DB::table('user_relations')->where('id', '=' , $idd)->where('relation','spouse')->get(); 	
		$count = DB::table('user_relations')->where('id', '=' , $idd)->where('relation','spouse')->count();
		
		if($count > 0) {
		foreach($result as $row)
		{
			$id .=$row->relation_with .',';		
		}


			return $spouse_id=substr($id,0,-1);	
		}
		else 
		{
			return $spouse_id;	
		}  	

	}
	
	public function getUserPartner($userIdd)
	{
		$parent_id='';
		$id='';
		$result = DB::table('user_relation_parents')->distinct('user_id')->where('rel_id', '=' , $userIdd)->pluck('user_id'); $count =DB::table('user_relation_parents')->distinct('user_id')->where('rel_id', '=' , $userIdd)->count(); 	
		if($count > 0) {
		foreach($result as $row)
		{
			$id .=$row .',';		
		}
			return $parent_id=substr($id,0,-1);	
		}
		else 
		{
			return $parent_id;	
		}  
		
				
	}
	
	public function UpdateUserAccount($array,$user_id)
	{
		DB::table('users')
                ->where('id', $user_id)
                ->update($array);	
				
	}	

/*=============create story==============*/	

	public function CreateUserStory($array)
	{
		return DB::table('stories')->insertGetId($array);
	}	

	public function uploadUserData($param,$tablename)
	{

		return DB::table($tablename)->insertGetId($param);
	}

	public function getUserUploadData($userId,$tablename)
	{
		return $result = DB::table($tablename)->where('user_id',$userId)->get();
	}

/*=============add group==============*/

	public function addGroup($param)
	{
		return DB::table('user_group')->insertGetId($param);
	}

/*=============add group member==============*/
	
	public function addGroupMember($param)
	{
		return DB::table('group_members')->insertGetId($param);
	}

/*=============get group list==============*/

	public function getUserGroupList($userId)
	{
		$result = DB::table('user_group')->where('user_id', '=' , $userId)->get();
		return $result;	
	}
	
	
}
