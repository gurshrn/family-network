@extends('layouts.default')
@section('page_title')
Pricing
@endsection 
@section('content')
<section class="about_page faq_page">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Pricing</h2>
				<div class="pricing_main">
				 <div class="pricing_bx">
					<div class="pricing_heading">
					<div class="pricing_heading_inr">
					 <h2>Basic</h2>
						<h5>Free</h5>
						</div>
					 </div><!-- pricing_heading end -->
					 <div class="pricing_cntnt_bx">
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Profile Page</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Family Tree Builder</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>View family member profiles and stories</span></p>
						 <p><img src="images/cross_icn.svg" alt="grn_tck"><span>Write stories, upload and store video, photos and audio</span></p>
						 <p><img src="images/cross_icn.svg" alt="grn_tck"><span>Weekly Inspiration Question</span></p>
						 <p><img src="images/cross_icn.svg" alt="grn_tck"><span>Forums</span></p>
						 <a href="#" title="" class="strt_here">Start Here</a>
					 </div>
					</div>
					
					<div class="pricing_bx">
					<div class="pricing_heading">
					<div class="pricing_heading_inr">
					 <h2>Standard</h2>
						<h5>1 account</h5>
						<h2>$5 <sup>/mth</sup></h2>
						</div>
					 </div><!-- pricing_heading end -->
					 <div class="pricing_cntnt_bx">
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Profile Page</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Family Tree Builder</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>View family member profiles and stories</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Write stories, upload and store video, photos and audio</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Weekly Inspiration Question</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Forums</span></p>
						  <a href="#" title="" class="strt_here">Start Here</a>
					 </div>
					</div>
					<div class="pricing_bx">
					<div class="pricing_heading">
					<div class="pricing_heading_inr">
					 <h2>Family</h2>
						<h5>5 account</h5>
						<h2>$199 <sup>/yr</sup></h2>
						</div>
					 </div><!-- pricing_heading end -->
					 <div class="pricing_cntnt_bx">
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Profile Page</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Family Tree Builder</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>View family member profiles and stories</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Write stories, upload and store video, photos and audio</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Weekly Inspiration Question</span></p>
						 <p><img src="images/grn_tck.svg" alt="grn_tck"><span>Forums</span></p>
						  <a href="#" title="" class="strt_here">Start Here</a>
					 </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="start_tell">
 <div class="container">
	 <h2>Start telling your stories today.</h2>
	 <h3>Get a 30 day Free Trial today!</h3>
	 <a href="#" class="started_btn eff-5">Start Here</a>
 </div>
</section>
@endsection
    