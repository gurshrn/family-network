@extends('layouts.default') 
@section('page_title') Faq @endsection 
@section('content')
<?php
$result=(new App\Faq)->GetFaq();
?>

<section class="about_page faq_page">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>FAQs</h2>
				<?php
				if(!empty($result)) {
				?>
				<div class="panel-group" id="accordion">
					<?php
					$i=1;
					foreach($result as $row) {
					?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								
                                <a data-toggle="collapse" data-parent="#accordion" href="#acrdn<?php echo $i; ?>" class="collapsed">
                               <?php echo $row->title; ?>

                                  </a>
                            </h4> </div>
						<div id="acrdn<?php echo $i; ?>" class="panel-collapse collapse">
							<div class="panel-body">
								<?php echo $row->content; ?>
							</div>
						</div>
					</div>
					<?php 
					$i++;
					} 
					?>
					
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section> @endsection