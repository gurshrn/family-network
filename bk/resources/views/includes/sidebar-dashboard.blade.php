<div id="mySidenav" class="sidenav MyClass">
	<div class="block menu-icon" id="tg" onclick="openNav()">
		<div class="cta">
			<div class="toggle-btn type10"></div>
		</div>
	</div>
	<?php
	
	
	?>
	 
	<ul class="sidebar-mnu"><li>
	<a class="{{ (Request::path()=='dashboard' ? 'active' : '') }}" href="{{ url('/dashboard') }}">
	
		<svg>
			<use xlink:href="#menu1"></use>
		</svg><span>Home</span></a>
		</li>
		<li>
	<a href="{{ url('/my-story') }}" class="{{ (Request::path()=='my-story' ? 'active' : '') }}" title="">
		<svg> 
			<use xlink:href="#menu2"></use>
		</svg><span>My Story</span></a></li>
		<li>
	<a href="{{url('/tree')}}">
		<svg>
			<use xlink:href="#menu3"></use>
		</svg><span>Tree</span></a></li>
		<li> 
			<a href="chat.html">
				<svg>
					<use xlink:href="#menu4"></use>
				</svg>
				<span>Inbox</span>
			</a>
		</li>
		<li class="has-children">
			<a href="{{url('/forum')}}">
				<svg>
					<use xlink:href="#menu5"></use>
				</svg><span>Forum</span>
			
			</a>
			<span class="drop-down"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
			<ul class="dropdown-menu1">
			<li><a href="{{url('/create-forum')}}" title="">Create Forum</a></li>
			</ul>
		</li>
		<li class="has-children">
			<a href="{{ url('/group') }}"> 
				<svg>
					<use xlink:href="#menu6"></use>
				</svg>
				<span>Group</span>
			</a>
			<span class="drop-down"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
			<ul class="dropdown-menu1">
				<li>
					<a href="{{ url('/create-group') }}" title="">Create Group</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="{{url('/questions')}}">
				<svg>
					<use xlink:href="#menu7"></use>
				</svg>
				<span>Questions</span>
			</a>
		</li>
		<li> 
			<a href="javascript:void(0);" id="add-user-member">
				<svg>
					<use xlink:href="#menu4"></use>
				</svg>
				<span>Add Member</span>
			</a>
		</li>
		<li>
			<a href="#">
				<svg>
					<use xlink:href="#menu8"></use>
				</svg>
				<span>Settings</span>
			</a>
		</li>
</ul>
</div>