<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ setting('site.title') }} - @yield('page_title')</title>
	<link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/x-icon">
	<link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/animate.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/toastr.css') }}" rel="stylesheet" type="text/css">
		
</head>
<body>
<div id="preloader"> 
	<div class="wrapper">
		<div class="cssload-loader"></div>
	</div>
</div>
<header>  
	<div class="container">
		<nav class="navbar navbar-expand-lg">
			<a class="navbar-brand" href="{{ url('/') }}"><img src="{{ imageUrl(setting('site.logo'), 256, null, ['crop' => false]) }}" alt="logo"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
			<div class="collapse navbar-collapse">
				
				{!! menu('Top Menu','mymenu') !!} 
				
				@include('includes.loginform')
			</div>
		</nav>
	</div>
</header>