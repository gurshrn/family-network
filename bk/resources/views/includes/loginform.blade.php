<div class="login_form">
    @if (!Auth::check())
	<form method="POST" action="{{ route('login') }}">
	@csrf
      <p>
        <input id="email" type="email" class="form_control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter Your Email" required>
		@if ($errors->has('email'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('email') }}</strong>
			</span>
		@endif
       
		<input id="password" placeholder="Password" type="password" class="form_control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
		@if ($errors->has('password'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('password') }}</strong>
			</span>
		@endif
      </p> 
      <div class="login_sbmt eff-5">
         <input type="submit" value="login" class="login">
      </div>
    </form>
	<div class="account-wrap">
		<div class="account-item clearfix js-item-menu">
			<div class="content">
				<a class="js-acc-btn" href="#"></a>
			</div>
			<div class="account-dropdown js-dropdown">

				<div class="account-dropdown__body">
					<div class="account-dropdown__item">
						<a href="#" title=""><span><i class="fa fa-facebook" aria-hidden="true" target="_blank"></i></span>Login with Facebook</a>
					</div>
					<div class="account-dropdown__item">
						<a href="#" title=""><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>Login with Gmail</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	
	
	
	@else
	<div class="user_log_in">
		<p>Welcome, <span>{{ Auth::user()->name }}</span></p>
		<a href="{{url('/logout')}}" class="open_source_btn">Logout</a> 
	</div> 
	@endif
</div>
