<!-- story_sec end -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 ftr_main">
					<p>Copyright &copy; <?php echo date('Y'); ?> Yearloom. Powered By - <a href="http://imarkinfotech.com" target="_blank">iMark Infotech</a></p>
					{!! menu('Footer Menu','footer_menu') !!} 
					<div class="ftr_social">
						<ul>
							<li><a href="#" title="" target="_blank"><i class="fa fa-facebook" aria-hidden="true" target="_blank"></i></a></li>
							<li><a href="#" title="" target="_blank"><i class="fa fa-twitter" aria-hidden="true" target="_blank"></i></a></li>
							<li><a href="#" title="" target="_blank"><i class="fa fa-linkedin" aria-hidden="true" target="_blank"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- footer end -->
	@include('includes.modal')
	<!-- jQuery (necessary for JavaScript plugins) -->
	
	<script type="text/javascript" src="{{ URL::asset('js/jquery3.3.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/popper.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/wow.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/my-custom.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/toastr.js') }}"></script>
	 
	  
</body>

</html>