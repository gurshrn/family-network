<section class="banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6">
				<figure class="wow zoomIn" data-wow-delay="0.2s" data-wow-duration="1000ms"><img src="{{ imageUrl($blockData->image, 701, null, ['crop' => false]) }}" alt="family_img"></figure>
			</div>
			<div class="col-sm-12 col-md-6 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
				<h1>{{ $blockData->title ?? '' }}</h1> <a href="{{ $blockData->link ?? '' }}" class="started_btn eff-5">{{ $blockData->button_text ?? 'Learn More' }}</a> </div>
		</div> 
	</div>
</section>