<div class="feature_btm">
    <div class="feature_btm_bx" style="background:url({{ imageUrl($blockData->image_1) }}) no-repeat;"> <a href="{{ $blockData->link_1 ?? '' }}" title="">{{ $blockData->title_1 ?? '' }}</a> </div>
    <div class="feature_btm_bx" style="background:url({{ imageUrl($blockData->image_2) }}) no-repeat;"> <a href="{{ $blockData->link_2 ?? '' }}" title="">{{ $blockData->title_2 ?? '' }}</a> </div>
</div>
<!-- TWO CARD BLOCK -->
