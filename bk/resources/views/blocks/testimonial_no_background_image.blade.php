@php
$result=(new App\Testimonial)->GetTestimonials();
@endphp
@if(!empty($result)) {
<section class="testimonial_sec">
	<div class="container">
		<div id="home_testi" class="owl-carousel owl-theme">
			@foreach($result as $value)
			<div class="item">
				<div class="testi_bx">
					<figure><img src="{{ imageUrl($value->image, 116, null, ['crop' => false]) }}" alt="john_img"></figure>
					{!! $value->content !!}
					<h4>{{$value->title}}</h4> </div>
			</div>
			@endforeach
		</div>
	</div>   
</section>  
@endif