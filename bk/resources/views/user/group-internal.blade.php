@extends('layouts.dashboard')
@section('page_title')
Group
@endsection
@section('content')

	<div class="sidebar-rght grp-intrn">

		<div class="dashboard-wrapper pd_none">

			<div class="grp-intrn-main">

				<div class="forum-bx">

					<figure>
						<img src="{{url('/upload/groupphotos/'.$groupDetail->profile_image)}}" alt="forum-img">
					</figure>

					<div class="forum-cntnt">

						<h2>{{ucfirst($groupDetail['group_name'])}}</h2>

						<p>{{ucfirst($groupDetail['description'])}}</p>

						<div class="grp-btn"> 

							<a href="#" title="" class="btn-white" data-toggle="modal" data-target="#group-members-modal">View All Member</a> 

							<a href="javascript:void(0)" title="" class="btn-white" data-toggle="modal" data-target="#addmember_modal">Add Member</a> 

						</div>

					</div>

				</div>

			</div>

			<div class="share-post"> <a href="{{url('/add-group-story?id='.$groupDetail['id'])}}" title="" class="share-post-sctn">Share a New Story</a>

				<figure>

					<a href="create-post.html" title=""><img src="{{url('images/camera_icn.svg')}}" alt="camera_icn"></a>

				</figure>

			</div>

			<div class="post-main">

				@if($groupDetail->groupstory)
				@foreach($groupDetail->groupstory as $story)

					<div class="post-main-bx">

						<div class="post-info-sctn">

							<div class="post-info-sctn-lft">

								<figure>
									<img src="{{url('/upload/profileimages/'.$story->userdetail['avatar'])}}" alt="user_img">
								</figure>

								<h2>{{$story->userdetail['name']}}</h2> 
							</div>

							<div class="post-info-sctn-rght">

								<div class="date-sctn"> 
									<span>3 Hours Ago</span> 
									<a href="javacript:void();" title="" class="dot-sctn">
										<span></span>
										<span></span>
										<span></span>
									</a>

									<ul class="view_links1" style="display: none;">

										<li>

											<a href="#" title="" id="crt_edit">

												<svg>

													<use xlink:href="#edit_img"></use>

												</svg><span>Edit</span></a>

										</li>

										<li>

											<a href="#" title="" id="crt_del">

												<svg>

													<use xlink:href="#del_img"></use>

												</svg><span>Delete</span></a>

										</li>

										<li>

											<a href="#" title="" id="crt_hide">

												<svg>

													<use xlink:href="#hide_img"></use>

												</svg><span>Hide</span></a>

										</li>

									</ul>

								</div>

							</div>

						</div>

						<h1><a href="#" title="">{{$story->title}}</a></h1>

						<div class="post-info-cntnt">

							<p>{{$story->excerpt}}... <a href="my-story-inner.html" title="" class="opn_stry">Open Story</a></p>

							<figure><img src="{{url('upload/timeline-full.jpg')}}" alt="timeline-full"></figure>

							<ul class="post-info-cmnt">

								<li><a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span>Like</span></a></li>

								<li><a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i><span>Comment</span></a></li>

							</ul>

						</div>

					</div>

				@endforeach
				@endif

			</div>

		</div>

	</div>

@endsection
	<div class="modal fade" id="group-members-modal" role="dialog">
	    <div class="modal-dialog">
	    
	      <div class="modal-content">

	      	<form id="add-relation" method="post" action="{{'/add-member'}}">
	      		@csrf
	      		<input type="hidden" name="relation_type" class="relation_type">
	      	</form>
	        
	        <div class="modal-body add-member-body">

	        	@if($groupDetail)
	        	@foreach($groupDetail->members as $val)
	        		<a href="javascript:void(0)" data-attr="Mother" class="select-member"><p>{{$val->userrelation->first_name.''.$val->userrelation->last_name}}</p></a>

	        	@endforeach
	        	@endif
	        	
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div>
	      
	    </div>
  	</div>