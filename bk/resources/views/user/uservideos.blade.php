@extends('layouts.dashboard')
@section('page_title')
Photos 
@endsection
@section('content')

	<div class="sidebar-rght story-page">

		@include('user.user_banner_section')

		<div class="dashboard-wrapper photo-wrapper">

			<div class="photo-page video-page">

				<h2>Videos</h2>

				<form class="add-create" id="add_video" action="{{ url('/videos') }}" method="post" enctype="multipart/form-data">
							
					@csrf 

					<div class="browse-section">

						<div class="custom-file">

							<input type="file" name="user_video" class="custom-file-input" id="customFile">

							<label class="form_control" for="customFile">Upload Videos</label>

						</div>

						<div class="photo-sbmt">

							<input type="submit" value="Upload" class="post-btn"> </div>

					</div>

				</form>

				<div class="photo-list">

					

					<div class="photo-bx">

						<!--Modal: Name-->

						<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

							<div class="modal-dialog" role="document">

								<!--Content-->

								<div class="modal-content">

									<div class="modal-header">

									<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>

									</div>

									<!--Body-->

									<div class="modal-body mb-0 p-0">

										<div class="embed-responsive embed-responsive-16by9 z-depth-1-half">

											<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/wTcNtgA6gHs?enablejsapi=1&amp;origin=https%3A%2F%2Fmdbootstrap.com" allowfullscreen="" data-gtm-yt-inspected-2340190_699="true" id="864247770" data-gtm-yt-inspected-2340190_908="true"></iframe>

										</div>

									</div>
								</div>
							</div>
						</div>

						<!--Modal: Name-->

						<div class="video-thumb">

						<figure>

							<a><img class="img-fluid z-depth-1" src="images/photo-img.jpg" alt="photo-img"></a>

						</figure>

						<div class="video-overlay"> <a href="#" title="" alt="video" data-toggle="modal" data-target="#modal1"><span><i class="fa fa-play" aria-hidden="true"></i></span></a> </div></div><h4><a href="#" title="">Lorem Ipsum dolor</a></h4>

					</div>
					

				</div>
			</div>
		</div>

	</div>

@endsection