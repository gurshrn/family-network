@extends('layouts.dashboard')
@section('page_title')
My Story 
@endsection
@section('content')
<div class="sidebar-rght story-page">
	@include('user.user_banner_section')  
	<div class="dashboard-wrapper">
	
		<div class="post-main">
			<?php
			/* echo "<pre>";
				print_r($resultStory);
			echo "</pre>";  */
			?> 
			@if (!empty($resultStory))
			@foreach($resultStory as $story)
			<div class="post-main-bx my_story">
				<div class="post-info-sctn">
					<div class="post-info-sctn-lft">
						<figure>
							<img src="{{asset('storage/').'/'.(empty(Auth::user()->avatar)?Config::get('voyager.user.default_avatar'):Auth::user()->avatar)}}" alt="user_img">
						</figure>
						<h2>{{Auth::user()->name}}</h2> 
					</div>
					<div class="post-info-sctn-rght">
						<div class="date-sctn"> 
							<span>
								<time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($story->created_at)); ?>T<?php echo date('H:i:s', strtotime($story->created_at)); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($story->created_at)); ?></time>
							</span> 
						</div> 
					</div>
				</div>
				<h1>
					<a href="#" title="">{{ $story->title }}</a>
				</h1>
				
				<div class="post-info-cntnt">
					
					{!! $story->description !!}
					
				</div>
				@if($story->user_photo != '')
					<figure><img src="{{ URL::asset('upload/userphotos/'.$story->user_photo) }}" alt="timeline-full"></figure>
				@endif
				@if($story->user_video != '')
					<video width="320" height="240" autoplay>
					  	<source src="{{ URL::asset('upload/uservideos/'.$story->user_video) }}" >
					</video>
					
				@endif
			</div>
			@endforeach
					
			@else 
				<div class="alert alert-danger">No Story Found</div>
			@endif 
			<!--post-main-bx end -->
		</div>
		<!--post-main end -->
	</div>
</div>
@endsection