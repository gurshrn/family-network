@extends('layouts.dashboard')
@section('page_title')
My Story 
@endsection
@section('content')
<div class="sidebar-rght story-page">
	@include('user.user_banner_section')  
	<div class="dashboard-wrapper">
	
		<div class="post-main">
			
			@if (!empty($userStory))
			@foreach($userStory as $story)

			<div class="post-main-bx my_story">
				<div class="post-info-sctn">
					<div class="post-info-sctn-lft">

						<figure>
							<img src="{{url('upload/profileimages/'.$story->userdetail['avatar'])}}" alt="user_img">
						</figure>

						<h2>{{$story->userdetail['name']}}</h2> 
					</div>

					<div class="post-info-sctn-rght">
						<div class="date-sctn"> 
							<span>
								<time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($story['created_at'])); ?>T<?php echo date('H:i:s', strtotime($story['created_at'])); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($story['created_at'])); ?>
									
								</time>
							</span> 
						
						<a href="javacript:void();" title="" class="dot-sctn"><span></span><span></span><span></span></a>
								<ul class="view_links1" style="display: none;">
									<li>
										<a href="#" title="" id="crt_edit">
											<svg>
												<use xlink:href="#edit_img"></use>
											</svg><span>Edit</span></a>
									</li>
									<li>
										<a href="#" title="" id="crt_del">
											<svg>
												<use xlink:href="#del_img"></use>
											</svg><span>Delete</span></a>
									</li>
									<li>
										<a href="#" title="" id="crt_hide">
											<svg>
												<use xlink:href="#hide_img"></use>
											</svg><span>Hide</span></a>
									</li>
								</ul>
						
						
						</div> 
					</div>
				</div>
				<h1>
					<a href="#" title="">{{ $story['title'] }}</a>
				</h1>
				<div class="post-info-cntnt">
					<p>{{ $story['excerpt'] }}</p>
					<p>
						<a href="{{ url('/story/') }}/{{ $story['id']  }}" title="" class="opn_stry">Open Story</a>
					</p>

					@if($story->userphotos['user_photo'] != '')
						<figure>
							<img src="upload/userphotos/{{$story->userphotos['user_photo']}}" alt="timeline-full">
						</figure>
					@endif
					 
					<ul class="post-info-cmnt"> 
						<li>
							<a href="">
								<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
								<span>Like</span>
							</a>
						</li>
						<li>
							<a href="">
								<i class="fa fa-commenting-o" aria-hidden="true"></i>
								<span>Comment</span>
							</a>
						</li>
					</ul>
					
				</div>
			</div>
			@endforeach
			
			<input type="hidden" id="total_record" value="{{ $totalRecords }}">
			<input type="hidden" id="offset" value="2">  
			<a href="javascript:void(0);" class="load_more" onclick="mystory_pagination();">Load More</a>
			@else 
				<div class="alert alert-danger">No Story Found</div>
			@endif 
			<!--post-main-bx end -->
		</div>
		<!--post-main end -->
	</div>
</div>
@endsection