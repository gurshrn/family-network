@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection 
@section('content')

	<div class="sidebar-rght story-page">

		<div class="dashboard-wrapper dashboard-full">

			<div class="forum-main">

				<div class="forum-bx">

					<figure><img src="{{url('/upload/forum/'.$forumDetail->image)}}" alt="forum-img"></figure>

					<div class="forum-cntnt">

						<h2>
							<a href="#" title="">{{$forumDetail->forumcategory->category_name}}</a>
							<small>
								<span>
									{{'Last updated by '.$forumDetail->user['name']}}
									<time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($forumDetail->created_at)); ?>T<?php echo date('H:i:s', strtotime($forumDetail->created_at)); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($forumDetail->created_at)); ?>
									</time>
								</span> 
							</small>
						</h2>

						<h6>{{$forumDetail->title}}</h6>

						<p>{{$forumDetail->description}}</p>

						<div class="forum-cmnt-sctn"> 
							<i class="fa fa-commenting-o" aria-hidden="true"></i>
							<span>{{count($forumComment)}} 
									@if(count($forumComment) == 1)
										{{'Comment'}}
									@else
										{{'Comments'}}
									@endif
							</span> 
							
						</div>

						<div class="forum-comment">

							<form id="forum_comment" action="{{url('add-forum-comment')}}" method="post">

								@csrf

								<input type="hidden" name="forum_id" value="{{$forumId}}">

								<p class="forum-full">

									<input type="text" placeholder="Write Your Comment" name="forum_comment" class="form_control">

								</p>
								<p>

									<input type="submit" value="Submit" class="post-btn add-submit-btn">

								</p>
							</form>
						</div>
					</div>
				</div>

				<div class="forum-comment-sctn">

					<h2>Comments</h2>

					<div class="forum-comment-main forum-comment-data">

						@if($forumComment)
						@foreach($forumComment as $comm)

							<div class="forum-comment-bx">

								<figure><img src="{{url('/upload/profileimages/'.$comm->user['avatar'])}}" alt="user-sml-img1"></figure>

								<div class="forum-txt">

									<h6>{{$comm->user['name']}}
										<small>
											<time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($comm['created_at'])); ?>T<?php echo date('H:i:s', strtotime($comm['created_at'])); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($comm['created_at'])); ?>
											</time>
										</small>
									</h6>

									<p>{{ucfirst($comm['comment'])}}</p>

								</div>
							</div>

						@endforeach
						@endif

					</div>
				</div>		
			</div>
		</div>
	</div>
@endsection