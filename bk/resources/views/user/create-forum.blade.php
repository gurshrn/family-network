@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection 
@section('content')

	<div class="sidebar-rght story-page">

		<div class="dashboard-wrapper dashboard-full">

			<div class="create-forum">

				<h2>Create Forum</h2>
				
				<form class="post-forum" id="create-forum" action="{{URL('/create-forum')}}" method="post" enctype="multipart/form-data">

					@csrf
				
					<p>
						<input type="text" placeholder="Title" name="title" class="form_control">

					</p>
					<p class="slct">

						<select class="placeholder1 form_control" name="cat_id">
							<option value="">Select Category</option>
							@if($forumCat)
							@foreach($forumCat as $cat)
								<?php echo '<option value="'.$cat['id'].'">'.$cat['category_name'].'</option>'; ?>
							@endforeach
							@endif
						</select>
					</p>
					<div class="input-group form_control"> 
						<span class="input-group-btn">
							<div class="btn1 custom-file-uploader">

			                	<input type="file" name="image" onchange="this.form.filename.value = this.files.length ? this.files[0].name : ''" />

			                		Upload Image

			              	</div>
						</span>

						<input type="text" name="filename" class="txtsctn" readonly>
						<img src="images/attach.svg" alt="attach" class="attch-file"> 
					</div>
					<p>
						<textarea class="form_control" placeholder="Description" name="description"></textarea>
					</p>

					<p>
						<input type="submit" value="Create" class="post-btn add-submit-btn">
					</p>
				</form>
			</div>
		</div>
	</div>
@endsection