<script type="text/javascript" src="{{ URL::asset('js/my-custom.js') }}"></script>
	<form name="user_register" id="user_register" action="" method="post">

		<?php 
			if(!empty($memberDetail))
			{
				$first_name = $memberDetail['first_name'];
				$last_name = $memberDetail['last_name'];
				$email = $memberDetail['email'];
				$invitedById = $memberDetail['invitedById'];
			}
			else
			{
				$first_name = '';
				$last_name = '';
				$email = '';
				$invitedById = '';
				$uniqueId = '';
			}
		?>

	@csrf

	<input type="hidden" value="{{$invitedById}}" name="invitedById">
	<p>
		<label>Sign up with Email</label>
	</p>
	<div class="radio_list">

		<label class="lt_0" for="gender_male">
			<input type="radio" id="gender_male" value="male" name="gender">
			<span class="circle"></span>Male
		</label>

		<label for="gender_female">
			<input type="radio" id="gender_female" value="female" name="gender">
			<span class="circle"></span>Female
		</label>

	</div>
	<div class="wdth_full">

		<p>
			<input type="text" name="first_name" placeholder="First Name" class="form_control cptl" value="{{$first_name}}">
		</p>

		<p>
			<input type="text" name="last_name" placeholder="Last Name" class="form_control cptl" value="{{$last_name}}"> 
		</p>
	</div>
	<div class="wdth_full">

		<p>
			<input type="email" name="email" placeholder="Email Address" class="form_control" value="{{$email}}">
		</p>

		<p class="slct">
			<select class="placeholder1 form_control" name="dob">
				<option value="">Year of Birth</option>
				<?php
				for($i=1980; $i<=date('Y'); $i++ ) {
				?>	
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php 
				}
				?>
			</select> 
		</p>
	</div>
	<div class="wdth_med">
		<label>My Father</label>
	<div class="wdth_full">
	
		<p>
		<input type="text" placeholder="First Name" name="father_first_name" class="form_control cptl"> </p>
	<p>
		<input type="text" placeholder="Last Name"  name="father_last_name" class="form_control cptl"> </p>
	</div>	
	</div>
	<div class="wdth_med">
		<label>My Mother</label>
		<div class="wdth_full">
			<p><input type="text" placeholder="First Name" name="mother_first_name" class="form_control cptl"> </p>
			<p><input type="text" placeholder="Last Name" name="mother_last_name" class="form_control cptl"> </p>
		</div>	
	</div> 
	<div class="loader_btn started_btn eff-5">
		<input type="submit" class="sbmt_btn"value="Get Started">
	</div>
	@if(empty($memberDetail))
	
		<div class="separtor_sctn">
			<label>or</label>
		</div>
		<div class="login_selection">
		 
			<a href="{{ url('/auth/facebook') }}" class="btn btn-facebook"><span><i class="fa fa-facebook"></i></span>Facebook</a>
			<a href="{{ url('/auth/google') }}" class="btn btn-facebook"><span><i class="fa fa-google"></i></span>Google</a>
			 
		</div>

	@endif

</form>