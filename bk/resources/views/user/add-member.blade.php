@extends('layouts.dashboard')
@section('page_title')
Create Group
@endsection
@section('content')

	<div class="sidebar-rght story-page">

		<div class="dashboard-wrapper dashboard-full">

			<div class="create-forum add-member">

				<h2>Add <span>{{$relation}}</span></h2>

				<form id="add-member-detail" action="{{URL('/create-add-member')}}" method="post" enctype="multipart/form-data">

					@csrf

					<input type="hidden" name="member_type" class="member_type" value="{{$relation}}">

					<div class="radio_list">

						<label class="lt_0" for="male">

							<input type="radio" checked="" id="male" name="gender" value="Male"> <span class="circle"></span>Male</label>

						<label for="female">

							<input type="radio" id="female" name="gender" value="Female"> <span class="circle"></span>Female</label>

						<label for="unknown">

							<input type="radio" id="unknown" name="gender" value="Unknown"> <span class="circle"></span>Unknown</label>

					</div>

					<div class="inpt-med1">

						<p>

							<input type="text" placeholder="First Name" name="first_name" class="form_control cptl"> 
						</p>

						<p>

							<input type="text" placeholder="Last Name" name="last_name" class="form_control cptl"> 
						</p>

					</div>

					<div class="inpt-med1">

						<p>

							<input type="text" placeholder="Birth Date" name="birth_date" class="form_control" id="datepicker-place"> </p>

						<p>

							<input type="text" placeholder="Birth Place" name="birth_place" class="form_control"> </p>

					</div>

					<div class="radio_list">

						<label class="lt_0" for="living">

							<input type="radio" checked="" id="living" name="living" value="Living"> <span class="circle"></span>Living</label>

						<label for="deceased">

							<input type="radio" id="deceased" name="living" value="Deceased"> <span class="circle"></span>Deceased</label>

					

					</div>

					<div class="inpt-med1">

						<p>

							<input type="email" placeholder="Email address" name="email" class="form_control"> 
						</p>

					</div>

					<div class="input-group form_control"> 
						<span class="input-group-btn">
							<div class="btn1 custom-file-uploader">

			                	<input type="file" name="profile_image" onchange="this.form.filename.value = this.files.length ? this.files[0].name : ''" />

			                		Upload Image

			              	</div>
						</span>

						<input type="text" name="filename" class="txtsctn" readonly>
						<img src="images/attach.svg" alt="attach" class="attch-file"> 
					</div>


					<div class="tp-24">

						<input type="submit" value="Save" class="post-btn add-submit-btn"> 	
						<a href="{{url('/tree')}}">
							<input type="button" value="Cancel" class="post-btn post-btn-brdr">
						</a>
					</div>

				</form>

			</div>

		</div>

	</div>

@endsection

