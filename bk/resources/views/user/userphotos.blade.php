@extends('layouts.dashboard')
@section('page_title')
Photos 
@endsection
@section('content')
	<div class="sidebar-rght story-page">
				
		@include('user.user_banner_section')

				<div class="dashboard-wrapper photo-wrapper">
					<div class="photo-page">
						
						<h2>Photos</h2>

						<form class="add-create" id="add_photo" action="{{ url('/photos') }}" method="post" enctype="multipart/form-data">
							
							@csrf 
							
							<div class="browse-section">
								<div class="custom-file">
									<input type="file" name="user_photo" class="custom-file-input" id="customFile">
									<label class="form_control" for="customFile">Upload Photos</label>
								</div>
								<div class="photo-sbmt">
									<input type="submit" value="Upload" class="post-btn">
								</div>
							</div>
						</form>
						<div class="photo-list">

							@if (!empty($userPhotos))
							@foreach($userPhotos as $photos)

								<div class="photo-bx">
									<figure><img src="{{url('/upload/userphotos/'.$photos->user_photo)}}" alt="photo-img">
										
										<div class="photo-overlay">
											<a href="#" title=""><svg><use xlink:href="#del_img"></use></svg></a>
										</div>
										
									</figure>
								</div><!-- photo-bx end -->

							@endforeach
							@endif
							
						</div><!-- photo-list end -->
					</div>
					<!--bio-page end -->
				</div>
			</div>
}
}
@endsection