<?php
/* echo "<pre>";
	print_r($resultStory);
echo "</pre>";  */
?> 
@foreach($resultStory as $story)

<div class="post-main-bx my_story">
	<div class="post-info-sctn">
		<div class="post-info-sctn-lft">
			<figure><img src="{{asset('storage/').'/'.(empty(Auth::user()->avatar)?Config::get('voyager.user.default_avatar'):Auth::user()->avatar)}}" alt="user_img"></figure>
			<h2>{{Auth::user()->name}}</h2> </div>
		<div class="post-info-sctn-rght">
			<div class="date-sctn"> <span><time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($story->created_at)); ?>T<?php echo date('H:i:s', strtotime($story->created_at)); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($story->created_at)); ?></time></span> 
			</div> 
		</div>
	</div><h1><a href="#" title="">{{ $story->title }}</a></h1>
	<div class="post-info-cntnt">
		<p>{{ $story->excerpt }}</p>
		<p><a href="{{ url('/story/') }}/{{ $story->id  }}" title="" class="opn_stry">Open Story</a></p>
		@if($story->user_photo != '')
			<figure><img src="upload/userphotos/{{$story->user_photo}}" alt="timeline-full"></figure>
		@endif
		
		<ul class="post-info-cmnt">
			<li><a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span>Like</span></a></li>
			<li><a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i><span>Comment</span></a></li>
		</ul>
		
	</div>
</div>
@endforeach

<!--post-main-bx end -->
			