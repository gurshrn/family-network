@extends('layouts.dashboard')
@section('page_title')
Questions
@endsection 
@section('content')

	<div class="sidebar-rght story-page">
		<div class="dashboard-wrapper dashboard-full">
			<div class="question-page">

				<h4>We have a lot of questions that you’ve probably never been asked before. <br> Use these questions as inspiration to get started and you can also write your own.</h4>

				<div class="question-page-main">
					<div class="accordion" id="accordionExample">

						@if($questions)
						<?php $i = 0 ; ?>
						@foreach($questions as $val)
							<div class="card">
								<div class="card-header" id="headingOne">
									<h6>
										<a href="" data-toggle="collapse" data-target="#collapseOne{{$i}}" aria-expanded="true" aria-controls="collapseOne">{{$val['question']}}</a>
									</h6> 
								</div>
								<div id="collapseOne{{$i}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
									<div class="card-body">
										
										<form  id="user_answers{{$i}}"  action="{{'/questions'}}" method="post" enctype="multipart/form-data">

											@csrf

											<input type="hidden" name="question" value="{{$val['question']}}">

											<p>
												<textarea  class="form_control" name="user_answer" placeholder="Answer" ></textarea>
											</p>

											<div class="input-group form_control"> 
												<span class="input-group-btn">
													<div class="btn1 custom-file-uploader">

	                									<input type="file" name="user_photo" onchange="this.form.filename.value = this.files.length ? this.files[0].name : ''" />

													</div>
												</span>
												
												<input type="text" name="filename" class="txtsctn" placeholder="Reply">
												<img src="images/attach.svg" alt="attach" class="attch-file"> 
											</div>
											
											<input type="submit" value="Submit" class="post-btn answer-btn"> 
										</form>
									</div>
								</div>
							</div>
						<?php	$i++; ?>
						@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection