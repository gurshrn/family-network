<script type="text/javascript" src="{{ URL::asset('js/my-custom.js') }}"></script>
<form id="add-member-detail" action="{{URL('/add-member')}}" method="post">
	
	@csrf
	
	<p>
		<label>Add <span class="add_member_type"></span></label>
	</p>

	<input type="hidden" name="member_type" class="member_type">
	
	<div class="radio_list">

		<label class="lt_0" for="gender_male">
			<input type="radio" id="gender_male" value="male" name="gender">
			<span class="circle"></span>Male
		</label>

		<label for="gender_female">
			<input type="radio" id="gender_female" value="female" name="gender">
			<span class="circle"></span>Female
		</label>
	</div>

	<div class="wdth_full">
		<p>
			<input type="text" name="first_name" placeholder="First Name" class="form_control cptl"> </p>
		<p>
		<input type="text" name="last_name" placeholder="Last Name" class="form_control cptl"> </p>
	</div>

	<div class="wdth_full">
		<p>
			<input type="email" name="email" placeholder="Email Address" class="form_control">
		</p>
		<p class="slct">
			<select class="placeholder1 form_control" name="dob">
				
				<option value="">Year of Birth</option>
				
				<?php for($i=1980; $i<=date('Y'); $i++ ) { ?>	
					
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>

				<?php } ?>
			</select> 
		</p>
	</div>
	
	<div class="loader_btn started_btn eff-5">
		<input type="submit" class="sbmt_btn add-submit-btn"value="Add Member">
	</div>
	
</form>