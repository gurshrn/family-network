<div class="timeline-sctn">
 <figure><img class="timeline-pic" src="{{asset('storage/').'/'.(empty(Auth::user()->cover_img)?'jtg1Mkh96mhsbi4cfOA0fprkbLpROZjZ6O5sDJsn.jpeg':Auth::user()->cover_img)}}" alt="timeline-img"></figure>
 <div class="p-image">
	<img src="{{ URL::asset('images/camera-img.svg') }}" alt="camera-img">
	<form name="my_cover_image" id="my_cover_image" action="" method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="ChangeUserCoverImage">
		@csrf  
		<input type="file" name="profile_img" onchange="MyCoverImage(this)">
		<input type="submit" name="submit" style="display:none;">  
	</form>   
</div>
</div>
<div class="timeline-image">
	<div class="timeline-lft-main">
	<div class="timeline-lft">
	
	 <figure>
		<img class="profile-pic" src="{{asset('storage/').'/'.(empty(Auth::user()->avatar)?Config::get('voyager.user.default_avatar'):Auth::user()->avatar)}}" alt="profile-img">
	 </figure>   
	 <div class="p-image">
		<figure>
			<img src="{{ URL::asset('images/camera-img.svg') }}" alt="camera-small-img">
		</figure> 
		<form name="my_image" id="my_image" action="" method="post" enctype="multipart/form-data">
			<input type="hidden" name="action" value="ChangeUserImage">
			@csrf 
			<input type="file" name="profile_img" onchange="MyImage(this)">
			<input type="submit" name="submit" style="display:none;">  
		</form> 
	 </div>
	</div>
	<div class="timeline-name"><h2>{{Auth::user()->name}}</h2></div>
		<ul>
		 <li><a href="bio.html" title="">Bio</a></li> 
			<li class="{{ (Request::path()=='my-story' ? 'current-nav' : '') }}">
				<a href="{{ url('/my-story') }}" title="">Stories</a>
			</li>  
			<li><a href="{{url('/photos')}}" title="">Photos</a></li> 
			<li><a href="{{url('/videos')}}" title="">Videos</a></li> 
		</ul>
	</div>
</div>