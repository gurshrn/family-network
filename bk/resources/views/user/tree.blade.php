@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection 
@section('content')

<div class="sidebar-rght story-page">
	<div class="dashboard-wrapper dashboard-full">
		<div class="tree-main">
			<?php
			//$current_user;
			//$result=(new App\User)->getUserParents($current_user);
			
			?>
			<div id="basicdiagram" style="width: 100%; height: 480px; border-style: dotted; border-width: 1px;" />
		</div>
	</div>
</div>

<?php //echo "<pre>";print_r($root_user);die;?>

@endsection

@section('dashboardTree')
    <?php 
        if($root_user[0]->profile_image != '')
        {
            $rootUserImage = url('upload/profileimages/'.$root_user[0]->profile_image);
        }
        else
        {
            $rootUserImage = url('upload/profileimages/father.png');
        }
    ?>

	<script>
        var options = new primitives.famdiagram.Config();
        var items = [
			{ "id":  <?php echo $root_user[0]->id; ?>, "title": "<?php echo $root_user[0]->first_name; ?>", "spouses" : [], "parents": [<?php echo $root_user[0]->parent; ?>], image: "<?php echo $rootUserImage;?>",description:"<div class='overlay-cntnt'><p><label>Nick Name</label><span>Vicky</span></p><p><label>DOB</label><span>26 - 12 - 1994 </span></p><p><label>Bio</label><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span></p><p><a href='#' class='post-btn'>View Profile</a></p></div>"},

		<?php 
            foreach($UserTree as $row) 
            { 
                if($row->profile_image != '')
                {
                    $userImage = url('upload/profileimages/'.$row->profile_image);
                }
                else
                {
                    $userImage = url('upload/profileimages/father.png');
                }
        ?>
			
			{ "id":  <?php echo $row->id; ?>, "title": "<?php echo $row->first_name .' '.$row->last_name; ?>", "spouses" : [<?php echo $result=(new App\User)->getSpouse($row->id);?>],  "parents": [<?php echo $row->parent; ?>], image: "<?php echo $userImage;?>" ,description:"<div class='overlay-cntnt'><p><label>Nick Name</label><span>Vicky</span></p><p><label>DOB</label><span>26 - 12 - 1994 </span></p><p><label>Bio</label><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span></p><p><a href='#' class='post-btn'>View Profile</a></p></div>"},
		
		<?php } ?> 
		];

		/*var items = [
            { id: 0, spouses: [1], title: "Mary Spencer", image: "../images/photos/m.png" },
            { id: 1, spouses: [0], title: "David Kirby", image: "../images/photos/d.png" },
            { id: 10, spouses: [], title: "Mary Spencer",image: "../images/photos/m.png" },
            { id: 11, spouses: [10], title: "David Kirby", image: "../images/photos/d.png" },
            
            { id: 2,parents: [0,1], spouses: [], title: "Mary Spencer", image: "../images/photos/m.png" },
            { id: 3, parents: [10,11],spouses: [2], title: "David Kirby",  image: "../images/photos/d.png" },
            { id: 4, parents: [2, 3], title: "Brad Williams", image: "../images/photos/b.png" },
            { id: 5, parents: [2, 3], spouses: [ 7], title: "Mike Kirby", image: "../images/photos/m.png"},
            { id: 7, title: "Sara Kemp", image: "../images/photos/s.png" },
            { id: 8, parents: [7,5], title: "Leon Kemp",image: "../images/photos/l.png" }
        ];*/

        options.pageFitMode = primitives.common.PageFitMode.None;
        options.items = items;
        options.cursorItem = 2;
        options.linesWidth = 6;
        options.linesColor = "#ddd";
        options.hasSelectorCheckbox = primitives.common.Enabled.False;
        options.normalLevelShift = 20;
        options.dotLevelShift = 20;
        options.lineLevelShift = 20;
        options.normalItemsInterval = 10;
        options.dotItemsInterval = 10;
        options.lineItemsInterval = 10;
        options.arrowsDirection = primitives.common.GroupByType.Parents;
        options.showExtraArrows = true;
        options.shapeType =primitives.common.ShapeType.Circle;
        jQuery("#basicdiagram").famDiagram(options);
        
	</script>
@stop

    