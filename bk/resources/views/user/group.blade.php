@extends('layouts.dashboard')
@section('page_title')
Group
@endsection
@section('content')

	<div class="sidebar-rght story-page">

		<div class="dashboard-wrapper dashboard-full">

			<div class="group-main">

				<div class="group-create">
					<a href="{{url('/create-group')}}" title="" class="post-btn create-grp">Create Group</a>
				</div>

				<ul class="nav nav-tabs" id="myTab" role="tablist">

					<li class="nav-item"> 
						<a class="nav-link active" id="grp-tab" data-toggle="tab" href="#grp" role="tab" aria-controls="grp" aria-selected="true">My Group</a> 
					</li>

					<li class="nav-item"> 
						<a class="nav-link" id="followgrp-tab" data-toggle="tab" href="#followgrp" role="tab" aria-controls="followgrp" aria-selected="false">Followed Group</a> 
					</li>

				</ul>

				<div class="tab-content" id="myTabContent">

					<div class="tab-pane fade show active" id="grp" role="tabpanel" aria-labelledby="grp-tab">

						@if($userGroups)
						@foreach($userGroups as $val)

							<div class="mygroup">

								<div class="mygroup-bx">

									<a href="{{url('/group-detail/'.$val->id)}}">
										<figure>
											<img src="{{url('/upload/groupphotos/'.$val->profile_image)}}" alt="group-img">
										</figure>

										<h5>{{$val->group_name}}</h5>

										<p>{{count($val->members)}} Member</p>
									</a>

								</div>
							</div>

						@endforeach
						@endif
				</div>

					<div class="tab-pane fade" id="followgrp" role="tabpanel" aria-labelledby="followgrp-tab">

						<div class="mygroup">

						 <div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						<div class="mygroup-bx">

							 <figure><img src="images/group-img.jpg" alt="group-img"></figure>

							 <h5>Group Name</h5>

							 <p>100 Member</p>

						 </div><!-- mygroup-bx end -->

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection

		