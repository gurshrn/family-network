@extends('layouts.dashboard')
@section('page_title')
Dashboard
@endsection 
@section('content')

	<div class="sidebar-rght story-page">

		<div class="dashboard-wrapper dashboard-full">

		 	<div class="forum-main">

		 		@if($forumDetail)
		 		@foreach($forumDetail as $val)

		 			<div class="forum-bx">

					 	<figure>
					  		<img src="{{url('/upload/forum/'.$val['image'])}}" alt="forum-img">
					  	</figure>

						<div class="forum-cntnt">

							<h2>
								<a href="{{url('/forum-detail/'.$val->id)}}" title="">{{$val->forumcategory['category_name']}}</a>
								<small>
									<div class="date-sctn"> 
										<span>
											{{'Last updated by '.$val->user['name']}}
											<time class="timeago" datetime="<?php echo date('Y-m-d', strtotime($val['created_at'])); ?>T<?php echo date('H:i:s', strtotime($val['created_at'])); ?>Z"><?php echo date('F j, Y, g:i a', strtotime($val['created_at'])); ?>
											</time>
										</span> 
							</div></small>
							</h2>

							<h6>{{$val['title']}}</h6>

							<p>{{$val['description']}}</p>

							<div class="forum-cmnt-sctn">
								<i class="fa fa-commenting-o" aria-hidden="true"></i>
								<span>100 Comment</span>
							</div>

						</div>
					</div>

				@endforeach
				@endif

			</div>
		</div>
	</div>
@endsection
	