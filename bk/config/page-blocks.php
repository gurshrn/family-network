<?php

/*
|--------------------------------------------------------------------------
| Page Blocks
|--------------------------------------------------------------------------
|
| This configuration file is used to display page blocks and their content,
| where each key is a block and each property is specific to that block.
|
*/
$blocks = array();

// The 'global' fields we'll use on multiple blocks
$spacesField = [
    'field' => 'spaces',
    'display_name' => 'Add Vertical Space',
    'partial' => 'voyager::formfields.select_dropdown',
    'required' => 0,
    'options' => [
        'Bottom',
        'Top',
        'Top & Bottom',
        'None',
    ],
    'placeholder' => 0,
];

$animationsField = [
    'field' => 'animate',
    'display_name' => 'Animate this block (in)?',
    'partial' => 'voyager::formfields.checkbox',
    'placeholder' => 'on',
    'required' => 0,
];

/**
 * (Column'd) Content Block
 * - Can be used for standard WYSIWYG content
 */
$columns = array(
    'content_one_column',
    'content_two_columns',
    'content_three_columns',
    'content_four_columns',
);

foreach ($columns as $i => $block) {
    $numCols = $i + 1;

    $blocks[$block] = [
        'name' => "Content - {$numCols} Column/s",
        'template' => 'blocks.' . $block,
    ];
    for ($col = 1; $col <= $numCols; $col++) {
        $blocks[$block]['fields']["html_content_{$col}"] = [
            'field' => "html_content_{$col}",
            'display_name' => "Column {$col} content",
            'partial' => 'voyager::formfields.rich_text_box',
            'required' => 0,
            'placeholder' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.</p>',
        ];
    }
    $blocks[$block]['fields']['spaces'] = $spacesField;
    $blocks[$block]['fields']['animate'] = $animationsField;
}

/**
 * (Column'd) Cards Block
 */
$columns = array(
    'cards_one_column',
    'cards_two_columns',
    'cards_three_columns',
);

foreach ($columns as $i => $block) {
    $numCols = $i + 1;

    $blocks[$block] = [
        'name' => "Cards - {$numCols} Column/s",
        'template' => 'blocks.' . $block,
    ];
    for ($col = 1; $col <= $numCols; $col++) {
        $blocks[$block]['fields']["image_{$col}"] = [
            'field' => "image_{$col}",
            'display_name' => "Column {$col}: Image",
            'partial' => 'voyager::formfields.image',
            'required' => 0,
        ];
        if ($numCols === 1) {
            $blocks[$block]['fields']["image_position_{$col}"] = [
                'field' => "image_position_{$col}",
                'display_name' => "Position of Column {$col}: Image",
                'partial' => 'voyager::formfields.select_dropdown',
                'required' => 0,
                'options' => [
                    'Left',
                    'Right',
                ],
                'placeholder' => 0,
            ];
        }
        $blocks[$block]['fields']["br_{$col}_1"] = [
            'field' => "br_{$col}_1",
            'display_name' => "Line break",
            'partial' => 'break',
        ];
        $blocks[$block]['fields']["title_{$col}"] = [
            'field' => "title_{$col}",
            'display_name' => "Column {$col}: Title",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Changing the World!',
        ];
        $blocks[$block]['fields']["content_{$col}"] = [
            'field' => "content_{$col}",
            'display_name' => "Column {$col}: Content",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.',
        ];
        $blocks[$block]['fields']["br_{$col}_2"] = [
            'field' => "br_{$col}_2",
            'display_name' => "Line break",
            'partial' => 'break',
        ];
        $blocks[$block]['fields']["button_text_{$col}"] = [
            'field' => "button_text_{$col}",
            'display_name' => "Button Column {$col}: Text",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Learn More',
        ];
        $blocks[$block]['fields']["link_{$col}"] = [
            'field' => "link_{$col}",
            'display_name' => "Column {$col}: Link",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#',
        ];
        $blocks[$block]['fields']["br_{$col}_3"] = [
            'field' => "br_{$col}_3",
            'display_name' => "Line break",
            'partial' => 'break',
        ];
    }
    $blocks[$block]['fields']['spaces'] = $spacesField;
    $blocks[$block]['fields']['animate'] = $animationsField;
}

   
$blocks['image_content'] = [
    'name' => 'Image Content Block',
    'template' => 'blocks.image_content',
    'fields'=>
        ["html_content"=> [
            'field' => "html_content",
            'display_name' => "Column content",
            'partial' => 'voyager::formfields.rich_text_box',
            'required' => 0,
            'placeholder' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.</p>',
            ],
        "image" => [
            'field' => "image",
            'display_name' => "Image",
            'partial' => 'voyager::formfields.image',
            'required' => 0,
            ],
        "image_position" => [
            'field' => "image_position",
            'display_name' => "Position of Image",
            'partial' => 'voyager::formfields.select_dropdown',
            'required' => 0,
            'options' => [
                'Left',
                'Right',
            ],
            'placeholder' => 0,
            ],
        "br_1" => [
            'field' => "br_1",
            'display_name' => "Line break",
            'partial' => 'break',
        ],
        "title" => [
            'field' => "title",
            'display_name' => "Title",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Changing the World!',
        ],
        'animate' => $animationsField,
        "br_2" => [
            'field' => "br_2",
            'display_name' => "Line break",
            'partial' => 'break',
        ],
        "button_text" => [
            'field' => "button_text",
            'display_name' => "Button Text",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Learn More',
        ],
        "link_2" => [
            'field' => "link",
            'display_name' => "Link",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#',
        ],
       
        //'spaces' => $spacesField,
        
    ]
];

  
$blocks['home_banner'] = [
    'name' => 'Home Page Banner',
    'template' => 'blocks.home_banner',
    'fields'=>
        ["html_content"=> [
            'field' => "html_content",
            'display_name' => "Column content",
            'partial' => 'voyager::formfields.rich_text_box',
            'required' => 0,
            'placeholder' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.</p>',
            ],
        "image" => [
            'field' => "image",
            'display_name' => "Image",
            'partial' => 'voyager::formfields.image',
            'required' => 0,
            ],
        
        "br_1" => [
            'field' => "br_1",
            'display_name' => "Line break",
            'partial' => 'break',
        ],
        "title" => [
            'field' => "title",
            'display_name' => "Title",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Changing the World!',
        ],
        "br_2" => [
            'field' => "br_2",
            'display_name' => "Line break",
            'partial' => 'break',
        ],
        "button_text" => [
            'field' => "button_text",
            'display_name' => "Button Text",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Learn More',
        ],
        "link_2" => [
            'field' => "link",
            'display_name' => "Link",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#',
        ],
         
        //'spaces' => $spacesField,
        
    ]
];


$blocks['home_story_1'] = [ 
    'name' => 'Home Story Section 1',
    'template' => 'blocks.home_story_1',
    'fields'=>
        ["html_content"=> [
            'field' => "html_content",
            'display_name' => "Column content",
            'partial' => 'voyager::formfields.rich_text_box',
            'required' => 0,
            'placeholder' => 'Enter Your Story here...',
            ],
        "image" => [
            'field' => "image",
            'display_name' => "Image",
            'partial' => 'voyager::formfields.image',
            'required' => 0,
            ],
		"image_position" => [
            'field' => "image_position",
            'display_name' => "Position of Image",
            'partial' => 'voyager::formfields.select_dropdown',
            'required' => 0,
            'options' => [
                'Left',
                'Right',
            ],
            'placeholder' => 0,
            ],	
        "title" => [
            'field' => "title",
            'display_name' => "Title",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Changing the World!',
        ],
		
		"background_color" => [
            'field' => "background_color",
            'display_name' => "Background Color",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#fff',
        ],
		
       
        "button_text" => [
            'field' => "button_text",
            'display_name' => "Button Text",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Learn More',
        ],
		
		 "button_class" => [
            'field' => "button_class",
            'display_name' => "Button Class",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Button Class',
        ],
		
        "link_2" => [
            'field' => "link",
            'display_name' => "Link",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#',
        ],
         
        //'spaces' => $spacesField,
        
    ]
];

$blocks['inner_pages_banner'] = [ 
    'name' => 'Inner Pages Banner',
    'template' => 'blocks.inner_pages_banner',
    'fields'=>
        ["html_content"=> [
            'field' => "html_content",
            'display_name' => "Column content",
            'partial' => 'voyager::formfields.rich_text_box',
            'required' => 0,
            'placeholder' => 'Enter Your Story here...',
            ],
        "image" => [
            'field' => "image",
            'display_name' => "Image",
            'partial' => 'voyager::formfields.image',
            'required' => 0,
            ],
		"image_position" => [
            'field' => "image_position",
            'display_name' => "Position of Image",
            'partial' => 'voyager::formfields.select_dropdown',
            'required' => 0,
            'options' => [
                'Left',
                'Right',
            ],
            'placeholder' => 0,
            ],	
        "title" => [
            'field' => "title",
            'display_name' => "Title",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Changing the World!',
        ],
		
		"background_color" => [
            'field' => "background_color",
            'display_name' => "Background Color",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#fff',
        ],
		
       
        "button_text" => [
            'field' => "button_text",
            'display_name' => "Button Text",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Learn More',
        ],
        "link_2" => [
            'field' => "link",
            'display_name' => "Link",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#',
        ],
         
        //'spaces' => $spacesField,
        
    ] 
];


$blocks['how-it-works'] = [ 
    'name' => 'How It works content',
    'template' => 'blocks.how-it-works-content',
    'fields'=> 
        ["html_content"=> [
            'field' => "html_content",
            'display_name' => "Column content",
            'partial' => 'voyager::formfields.rich_text_box',
            'required' => 0,
            'placeholder' => 'Enter Your Story here...',
            ]
			
         
        //'spaces' => $spacesField,
        
    ] 
];

$blocks['page-content'] = [ 
    'name' => 'Content Box',
    'template' => 'blocks.page-content',
    'fields'=> 
        ["html_content"=> [
            'field' => "html_content",
            'display_name' => "Column content",
            'partial' => 'voyager::formfields.rich_text_box',
            'required' => 0,
            'placeholder' => 'Enter Your Story here...', 
            ]      
         
        //'spaces' => $spacesField,
        
    ] 
];

$blocks['year-loom'] = [ 
    'name' => 'How it works Year Loom',
    'template' => 'blocks.year-loom', 
    'fields'=> 
        ["html_content"=> [
            'field' => "title",
            'display_name' => "Title",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => 'Heading', 
            ],
			"background_color" => [
            'field' => "background_color",
            'display_name' => "Background Color",
            'partial' => 'voyager::formfields.text',
            'required' => 0,
            'placeholder' => '#fff',
			],       
         
        //'spaces' => $spacesField,
        
    ] 
];




$blocks['testimonial_no_background_image'] = [ 
    'name' => 'Testimonial with no Background',
    'template' => 'blocks.testimonial_no_background_image',
    'fields'=>array()
];

$blocks['testimonial_with_background_image'] = [ 
    'name' => 'Testimonial with Background',
    'template' => 'blocks.testimonial_with_background_image',
    'fields'=>array()
];




return $blocks;
