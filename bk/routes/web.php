<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('pages.home');
});
*/

Auth::routes(); 

Route::group(['middleware' => 'auth'], function () {
   
    Route::get('/dashboard', 'DashboardController@dashboard');  
	Route::get('/tree', 'DashboardController@userTree');  
	Route::any('/my-account', 'UserController@myAccount');  
	Route::any('/create-story', 'DashboardController@createStory');  
	Route::get('/my-story', 'DashboardController@myStory');   
	Route::any('/photos', 'DashboardController@addPhotos');   
	Route::any('/videos', 'DashboardController@addVideos');   
	Route::any('/group', 'DashboardController@showGroup');   
	Route::any('/create-group', 'DashboardController@createGroup');   
	Route::post('/create-add-member', 'DashboardController@addMember');
	Route::get('/forum', 'DashboardController@forum');
	Route::any('/create-forum', 'DashboardController@createForum');
	Route::post('/add-forum-comment', 'DashboardController@addForumComment');
	Route::get('/get-member-list', 'DashboardController@getMemberList');
	Route::get('/get-member-email', 'DashboardController@getMemberEmail');
	Route::post('/invite-member', 'DashboardController@inviteMember');
	Route::any('/questions', 'DashboardController@questions');
	Route::get('/forum-detail/{slug}', 'DashboardController@forumDetail');
	Route::get('/add-group-story', 'DashboardController@createStory');
	Route::get('/edit-story/{slug}', 'DashboardController@editStory');
	Route::any('/group-detail/{slug}', 'DashboardController@groupDetail');   
	Route::get('/story/{slug}', 'UserController@storydetail')->name('story_id');


});


//User Ajax

Route::any('/logout', 'HomeController@logout'); 
Route::post('/showuserregisterform', 'UserController@ShowUserRegisterForm'); 
Route::any('/registeruser', 'UserController@registerUser');
Route::post('/my_account_ajax', 'UserAjaxContoller@MyAccount'); 
Route::post('/create_story_ajax', 'UserAjaxContoller@CreateStoryAjax'); 
Route::post('/mystory_load_more', 'UserAjaxContoller@MyStoryPagination');   
Route::post('/dashboard_story_load_more', 'UserAjaxContoller@DashboardStoryPagination');   
Route::post('/change_user_image_ajax', 'UserAjaxContoller@ChangeUserImage');    
Route::post('/change_user_cover_image_ajax', 'UserAjaxContoller@ChangeUserCoverImage');    
Route::post('/add-member', 'UserAjaxContoller@ShowAddMemberForm');    
    
 

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

 
Route::get('crop-image', 'ImageController@index');
Route::post('crop-image', ['as'=>'upload.image','uses'=>'ImageController@uploadImage']);
 
 


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
 
Route::get('/{slug?}', 'PagesContoller@getPage')->name('home');

 
